# CATIA_prod project :

## hardware : Kicad files to produce the CATIA test board.<br>
- schematics using power from FEAD board (3.6V) and power control with LTC2945 with dedicated I2C bus through ERNI connector and I2C level translator (PCA9306).<br>
This reduce drastically the number of connexions <br>

- pcb : 6 layers

- fab files

## software : Code to access the FEAD board and run CATIA tests 
- You need to install the cactus project libraries
  - Have a look at https://ipbus.web.cern.ch
  - You have to install :
    - boost and boost-devel
    - libxml2 libxml2-devel
    - pugixml pugixml-devel
    - erlang
- GUI version of the test program based on GTK. The menu is developped with Glade.
  - CATIA_test.cxx : main program
  - gdaq.menu.cc : definition of GTK menus
  - config.cc : manage xml files and program CATIAs and LiTE-DTUs accordingly
  - init_gdaq.cc : Init IPbus connection with FEAD/VICEPP board
  - CATIA_calibration.cc : Complete procedure to calibrate CATIAs
  - CRC_calc.cc : Compute CRC from incoming data and compare with LiTE-DTU one
  - get_temp.cc : Read temperature in CATIAs and FPGA
  - calib_ADC.cc : Manage ADC calibration setting CATIA outputs to correct values
  - get_event.cc : Get events according to trigger setings
  - I2C_RW.cc : Manage I2C protocol
  - send_ReSync_command.cc : Send command through ReSync line to LiTE-DTU
  - switch_power.cc : Manage test board power

- Playing with line drivers strength and signal pre-amphasis, it is possible to introduce flat cables (tested up to 30 cm) between test board and DAQ board.
The reference of cables is 4060427 on Farnell catalog. You will need also a small adaptator PCB since extension cables are female-female.<br>
Please have a look in hardware/fead_extend folder.

At the end, the executable is in "bin" directory. <br>

Nothing is perfect, so any feedback is welcome.<br>

M.D.

Initial creation : 2023/02/08 <br>
Last Modification : 2024/03/13
