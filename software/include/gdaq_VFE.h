#ifndef EXTERN
  #define EXTERN
#endif

#include "gtk/gtk.h"
#include "uhal/uhal.hpp"
#include <signal.h>
#include "limits.h"

#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TStyle.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>

#define I2C_SHIFT_DEV_NUMBER   3

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)
#define GENE_G10_PULSE         (1<<5)
#define GENE_G1_PULSE          (1<<6)
#define GENE_RESYNC            (1<<7)
#define GENE_AWG               (1<<8)
#define CRC_RESET              (1<<31)

#define RESET                  (1<<0)
#define FIFO_MODE              (1<<1)
#define SELF_TRIGGER_LOOP      (1<<2)
#define SELF_TRIGGER           (1<<3)
#define I2C_LOW                (1<<4)
#define CLOCK_SELECT           (1<<5)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define CALIB_PULSE_ENABLED    (1<<27)
#define CALIB_MUX_ENABLED      (1<<28)
#define BOARD_SN               (1<<28)
#define SELF_TRIGGER_MODE      (1<<31)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define RESYNC_PHASE           (1<<16)

#define I2C_TOGGLE_SDA         (1<<31)

#define PWUP_RESETB            (1<<31)
#define BULKY_I2C_DTU          (1<<28)
#define I2C_LPGBT_MODE         (1<<27)
#define N_RETRY_I2C_DTU        (1<<14)
#define PED_MUX                (1<<13)
#define BUGGY_I2C_DTU          (1<<4)
#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START          1
#define CAPTURE_STOP           2
//#define SW_DAQ_DELAY           0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY           (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY           (1<<0)   // Laser with external trigger
#define NSAMPLE_MAX            26624
#define MAX_PAYLOAD            1380
#define MAX_VFE                10
#define MAX_STEP               4096

#define DRP_WRb                (1<<31)
#define DELAY_TAP_DIR          (1<<5)     // Increase (1) or decrease (0) tap delay in ISERDES
#define DELAY_RESET            (1<<6)     // Reset ISERDES delay
#define IO_RESET               (1<<7)     // Reset ISERDES IO
#define BIT_SLIP               (1<<16)    // Slip serial bits by 1 unit (to align bits in ISERDES) (to multiply with 5 bits channel map)
#define BYTE_SLIP              (1<<21)    // Slip serial bytes by 1 unit (to align bytes in 40 MHz clock) (to multiply with 5 bits channel map)
#define DELAY_AUTO_TUNE        (1<<29)    // Launch automatic IDELAY tuning to get the eye center of each stream on calibartion streams
#define START_IDELAY_SYNC      (1<<27)    // Force IDELAY tuning to get the eye center of each stream when idle patterns are ON (LiTE-DTU-V2.0)

#define DTU_CALIB_MODE         (1<<0)
#define DTU_TEST_MODE          (1<<1)
#define DTU_MEM_MODE           (1<<5)

#define RESYNC_IDLE_PATTERN    (1<<0)
#define DTU_PLL_LOCK           8
#define TE_POS                 (1<<13)
#define TE_COMMAND             (1<<27)
#define TE_ENABLE              (1<<31)

#define eLINK_ACTIVE           (1<<8)
#define STATIC_RESET           (1<<31)

#define I2C_LVR_TYPE           0
#define I2C_CATIA_TYPE         1
#define I2C_LiTEDTU_TYPE       2

#define NSAMPLE_PED            5
#define N_ADC_REG              76

using namespace uhal;

typedef struct
{
  GtkWidget *main_window;
  GtkBuilder *builder;
  gpointer user_data;
} SGlobalData;
EXTERN SGlobalData gtk_data;

typedef struct
{
  Int_t CATIA_version[5], CATIA_Vref_val[5], CATIA_ped_G10[5], CATIA_ped_G1[5], CATIA_DAC1_val[5], CATIA_DAC2_val[5];
  Int_t CATIA_gain[5], CATIA_LPF35[5], CATIA_Rconv[5], CATIA_temp_out[5], CATIA_temp_X5[5], CATIA_Vref_out[5], CATIA_DAC_buf_off[5];
  Int_t CATIA_reg_def[5][7], CATIA_reg_loc[5][7];
  Int_t DTU_version[5], DTU_BL_G10[5], DTU_BL_G1[5], DTU_PLL_conf[5], DTU_PLL_conf1[5], DTU_PLL_conf2[5];
  Int_t DTU_CRC_errors[5], DTU_n_orbit_samples[5], DTU_driver_current[5],DTU_PE_width[5], DTU_PE_strength[5];
  Double_t CATIA_Temp_offset[5], DTU_consumption_ref;
  bool DTU_dither[5], DTU_nsample_calib_x4[5], DTU_global_test[5], DTU_override_Vc_bit[5], DTU_force_PLL[5];
  bool DTU_VCO_rail_mode[5], DTU_bias_ctrl_override[5];
  bool DTU_force_G1[5], DTU_force_G10[5], DTU_invert_clock[5], DTU_G1_window16[5], DTU_clk_out[5], DTU_eLink[5];
  bool CATIA_DAC1_status[5], CATIA_DAC2_status[5], CATIA_SEU_corr[5];
  Int_t CATIA_number[5], DTU_number[5];
} AsicSetting_s;
EXTERN AsicSetting_s asic;

typedef struct
{
  Int_t FEAD_number, VFE_number, firmware_version, nsample, nevent, numrun, cur_evt, I2C_low, clock_select;
  Int_t n_active_channel, eLink_active, channel_number[5], daq_initialized;
  Int_t I2C_LiTEDTU_type, I2C_CATIA_type, I2C_shift_dev_number, I2C_lpGBT_mode;
  Int_t delay_auto_tune, odd_sample_shift;
  Int_t old_read_address, shift_oddH_samples, shift_oddL_samples;
  Int_t I2C_DTU_nreg, buggy_DTU, DTU_test_mode;
  Int_t trigger_type, self_trigger, self_trigger_mode, self_trigger_threshold, self_trigger_mask, self_trigger_loop;
  Int_t fifo_mode;         // Use memory in FIFO mode or not
  Int_t sw_DAQ_delay, hw_DAQ_delay, resync_idle_pattern, resync_phase;
  Int_t TP_step, n_TP_step, n_TP_event, TP_width, TP_delay, TP_level;
  Int_t TE_pos, TE_command;
  bool TE_enable, is_VICEPP;
  Int_t debug_DAQ, debug_I2C, debug_GTK, debug_draw, debug_ana, zoom_draw;
  Int_t LVRB_autoscan, error, calib_pulse_enabled, calib_mux_enabled;
  UInt_t ReSync_data;
  Long_t timestamp;
  Short_t *event[6], *gain[6];
  Double_t *fevent[6], ped_G1[6], ped_G10[6];
  Double_t Rshunt_V2P5, Rshunt_V1P2, Tsensor_divider, Vref_target;
  Int_t all_sample[6], ns_g1[6];
  bool cli,CATIA_scan_Vref, ADC_use_ref_calib, CATIA_Vcal_out, half_mode;
  bool corgain, wait_for_ever, daq_running;
  bool min_setting, max_setting;
  bool test_power, test_temperature, test_bandgap, test_DAC1, test_DAC2, test_pedestal, test_noise, test_linearity, test_gain_internal, test_gain_AWG;
  bool power_valid, temperature_valid, bandgap_valid, DAC1_valid, DAC2_valid, pedestal_valid, noise_valid, linearity_valid, gain_internal_valid, gain_AWG_valid;
  Double_t V1P2_val, I1P2_val, I1P2_min, I1P2_max, V2P5_val, I2P5_val, I2P5_min, I2P5_max;
  Double_t temp_X1_val, temp_X1_min, temp_X1_max, temp_X5_val, temp_X5_min, temp_X5_max;
  Double_t bandgap_best_value, bandgap_value_min, bandgap_value_max;
  Int_t    bandgap_best_setting,bandgap_setting_min, bandgap_setting_max;
  Double_t DAC1_offset_val, DAC1_offset_min, DAC1_offset_max, DAC2_offset_val, DAC2_offset_min, DAC2_offset_max;
  Double_t DAC1_slope_val, DAC1_slope_min, DAC1_slope_max, DAC2_slope_val, DAC2_slope_min, DAC2_slope_max;
  Double_t ped_G10_offset_val, ped_G10_offset_min, ped_G10_offset_max, ped_G1_offset_val, ped_G1_offset_min, ped_G1_offset_max;
  Double_t ped_G10_slope_val, ped_G10_slope_min, ped_G10_slope_max, ped_G1_slope_val, ped_G1_slope_min, ped_G1_slope_max;
  Int_t ped_G10_best_val, ped_G10_best_min, ped_G10_best_max, ped_G1_best_val, ped_G1_best_min, ped_G1_best_max;
  Double_t noise_G1_val, noise_G1_min, noise_G1_max;
  Double_t noise_R320_LPF50_val, noise_R320_LPF50_min, noise_R320_LPF50_max;
  Double_t noise_R320_LPF35_val, noise_R320_LPF35_min, noise_R320_LPF35_max;
  Double_t noise_R380_LPF50_val, noise_R380_LPF50_min, noise_R380_LPF50_max;
  Double_t noise_R380_LPF35_val, noise_R380_LPF35_min, noise_R380_LPF35_max;
  Double_t noise_R470_LPF50_val, noise_R470_LPF50_min, noise_R470_LPF50_max;
  Double_t noise_R470_LPF35_val, noise_R470_LPF35_min, noise_R470_LPF35_max;
  Double_t linearity_G10_val, linearity_G10_min, linearity_G10_max;
  Double_t linearity_G1_val, linearity_G1_min, linearity_G1_max;
  Double_t gain_int_G10_val, gain_int_G10_min, gain_int_G10_max;
  Double_t gain_int_R320_val, gain_int_R320_min, gain_int_R320_max;
  Double_t gain_int_R380_val, gain_int_R380_min, gain_int_R380_max;
  Double_t gain_int_R470_val, gain_int_R470_min, gain_int_R470_max;
  Double_t gain_int_R2471_val, gain_int_R2471_min, gain_int_R2471_max;
  Double_t gain_int_R272_val, gain_int_R272_min, gain_int_R272_max;
  Double_t gain_AWG_R320_val, gain_AWG_R320_min, gain_AWG_R320_max;
  Double_t gain_AWG_R380_val, gain_AWG_R380_min, gain_AWG_R380_max;
  Double_t gain_AWG_R470_val, gain_AWG_R470_min, gain_AWG_R470_max;
  Double_t gain_AWG_R2471_val, gain_AWG_R2471_min, gain_AWG_R2471_max;
  Double_t gain_AWG_R272_val, gain_AWG_R272_min, gain_AWG_R272_max;
  Int_t CATIA_test_number;
  ValVector< uint32_t > mem;
  TGraph *tg[6], *tg_g1[6];
  TH1D *hmean[6], *hrms[6], *hdensity[6];
  TProfile *pshape[MAX_STEP][6];
  TCanvas *c1, *c2, *c3;
  TTree *tdata;
  TFile *fd;
  char xml_filename[PATH_MAX], comment[1024];
  char JSONdb[80];
} DAQSetting_s;
EXTERN DAQSetting_s daq;

using namespace uhal;

EXTERN struct timeval tv;
EXTERN void read_conf_from_xml_file(char*);
EXTERN void write_conf_to_xml_file(char*);
EXTERN std::vector<uhal::HwInterface> devices;
EXTERN void init_gDAQ();
EXTERN void send_ReSync_command(UInt_t command);
EXTERN UInt_t update_CATIA_reg(Int_t, Int_t);
EXTERN UInt_t update_LiTEDTU_reg(Int_t, Int_t);
EXTERN void take_run(void);
EXTERN void get_event(Int_t, Int_t);
EXTERN void get_single_event(Int_t);
EXTERN void calib_ADC(void);
EXTERN int  CATIA_calibration(int);
EXTERN void end_of_run(void);
EXTERN void update_trigger(void);
EXTERN void get_temp(void);
EXTERN void switch_power(bool);
EXTERN void get_consumption(void);
EXTERN void allow_limit_tuning(bool,bool);

extern "C" G_MODULE_EXPORT void delete_event(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void callback_about(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void my_exit(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void button_clicked(GtkWidget*, gpointer);
extern "C" G_MODULE_EXPORT void switch_toggled(GtkWidget*, gpointer);
extern "C" G_MODULE_EXPORT void Entry_modified(GtkWidget*, gpointer);
extern "C" G_MODULE_EXPORT void read_config(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void save_config(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void save_config_as(GtkMenuItem*, gpointer);
