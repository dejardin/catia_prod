#define EXTERN extern
#include "gdaq_VFE.h"
#include <TVirtualFFT.h>
#include <TFitResultPtr.h>
#include <fstream>
#include <sys/stat.h>
#include "nlohmann/json.hpp"
using json = nlohmann::json;


int CATIA_calibration(int redo)
{
  TFitResultPtr tfr[2],tfr1,tfr2;
  uhal::HwInterface hw=devices.front();
  FILE *fcal;
  Double_t NSPS=160.e6;
  Int_t command, fead_ctrl;
  Int_t ret_code=0;
  ValWord<uint32_t> address;
  ValVector< uint32_t > mem;
  double dv=1200./4096.; // 12 bits on 1.2V
  GtkWidget *widget;
  char widget_name[80], widget_text[80], fit_opt[32];
  gettimeofday(&tv,NULL);
  Double_t start_time=tv.tv_sec+tv.tv_usec*1.e-6;
  printf("Start CATIA calibratin @ %.6lf\n",start_time);

  // Check that data directory exists, otherwise create it 
  struct stat sb;
  if(!(stat("data", &sb)==0)) {
    printf("creating data dir");
    const int dir_err = mkdir("data", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (-1 == dir_err){
      printf("Error creating directory!");
    }
  }

  GtkTextBuffer *buffer;
  GtkTextIter start, end;
  char cdum;
  char output_file[256], hname[80];
  daq.LVRB_autoscan=0;
  Int_t draw=0;
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_comment");
    g_assert (widget);
    buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
    g_assert (buffer);
    gtk_text_buffer_get_start_iter(buffer, &start);
    gtk_text_buffer_get_end_iter(buffer, &end);
    gchar *comment= gtk_text_buffer_get_text (buffer, &start, &end,FALSE);
    strcpy(daq.comment,comment);
    printf("Comment seen : %s\n",daq.comment);

    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_debug_draw");
    g_assert (widget);
    //gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  sprintf(fit_opt,"N");
  if(daq.debug_draw) sprintf(fit_opt,"");

  daq.nevent=1000;
  daq.nsample=100;
  daq.n_TP_step=1;
  daq.TP_step=128;
  daq.TP_level=0;
  daq.TP_delay=50;
  daq.TP_width=150;
  Int_t TP_width_recovery=5;
  UInt_t iret, I2C_data;
  command=((daq.TP_delay&0xffff)<<16) | (daq.TP_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width,daq.TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);

  Int_t ADC_reg_val[2][N_ADC_REG];
  Double_t ped[2], mean[6], mean_prev[6]={0.}, rms[2];
  Double_t val_gain[2]={10.,1.};
  Double_t RTIA[3]={470.,380.,320.};
  Double_t Rshunt_V1P2=0.10;
  Double_t Rshunt_V2P5=0.10;
  Double_t Vref_ref=daq.Vref_target;

  bool chip_is_valid = TRUE;
  daq.power_valid=TRUE;
  daq.temperature_valid=TRUE;
  daq.bandgap_valid=TRUE;
  daq.DAC1_valid=TRUE;
  daq.DAC2_valid=TRUE;
  daq.pedestal_valid=TRUE;
  daq.noise_valid=TRUE;
  daq.linearity_valid=TRUE;
  daq.gain_internal_valid=TRUE;
  daq.gain_AWG_valid=TRUE;

  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_power_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"?");
    gtk_widget_set_name(widget,"");
    //widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_valid");
    //g_assert (widget);
    //gtk_label_set_label((GtkLabel*)widget,"?");
    //gtk_widget_set_name(widget,"");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_CATIA_valid");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,"CATIA valid\n          ?");
    gtk_widget_set_name(widget,"");
  }

  // Setting up JSON dump here
  json j;
  std::ifstream f_json(daq.JSONdb);
  if(!f_json) {
    printf("Creating new json DB: %s \n", daq.JSONdb);
   
    // as working from blank json, fill up "metadata" at this point
    j["Refs"] = {
      { "I2P5_min",             daq.I2P5_min             },
      { "I2P5_max",             daq.I2P5_max             },

      { "Bandgap_min",          daq.bandgap_value_min    },
      { "Bandgap_max",          daq.bandgap_value_max    },

      { "Iinj_DAC1_slope_min",  daq.DAC1_slope_min       },  
      { "Iinj_DAC1_slope_max",  daq.DAC1_slope_max       },  
      { "Iinj_DAC1_offset_min", daq.DAC1_offset_max      },
      { "Iinj_DAC1_offset_max", daq.DAC1_offset_max      },
      { "Iinj_DAC2_slope_min",  daq.DAC2_slope_min       },
      { "Iinj_DAC2_slope_max",  daq.DAC2_slope_max       },
      { "Iinj_DAC2_offset_min", daq.DAC2_offset_max      },
      { "Iinj_DAC2_offset_max", daq.DAC2_offset_max      },

      { "PED_G1_offset_min",	daq.ped_G1_offset_min    },	 
      { "PED_G1_offset_max",	daq.ped_G1_offset_max    }, 
      { "PED_G1_slope_min",	daq.ped_G1_slope_min     }, 
      { "PED_G1_slope_max",	daq.ped_G1_slope_max     }, 
      { "PED_G1_best_min",	daq.ped_G1_best_min      }, 
      { "PED_G1_best_max",	daq.ped_G1_best_max      }, 
      { "PED_G10_offset_min",	daq.ped_G10_offset_min   },	 
      { "PED_G10_offset_max",	daq.ped_G10_offset_max   }, 
      { "PED_G10_slope_min",	daq.ped_G10_slope_min    }, 
      { "PED_G10_slope_max",	daq.ped_G10_slope_max    }, 
      { "PED_G10_best_min",	daq.ped_G10_best_min     }, 
      { "PED_G10_best_max",	daq.ped_G10_best_max     }, 

      { "Noise_G1_min",		daq.noise_G1_min         },		 
      { "Noise_G1_max",		daq.noise_G1_max         },		 
      { "Noise_R470_LPF50_min",	daq.noise_R470_LPF50_min },	 
      { "Noise_R470_LPF50_max",	daq.noise_R470_LPF50_max },	 
      { "Noise_R470_LPF35_min",	daq.noise_R470_LPF35_min },	 
      { "Noise_R470_LPF35_max",	daq.noise_R470_LPF35_max },	 
      { "Noise_R380_LPF50_min",	daq.noise_R380_LPF50_min },	 
      { "Noise_R380_LPF50_max",	daq.noise_R380_LPF50_max },	 
      { "Noise_R380_LPF35_min",	daq.noise_R380_LPF35_min },	 
      { "Noise_R380_LPF35_max",	daq.noise_R380_LPF35_max },	 
      { "Noise_R320_LPF50_min",	daq.noise_R320_LPF50_min },	 
      { "Noise_R320_LPF50_max",	daq.noise_R320_LPF50_max },	 
      { "Noise_R320_LPF35_min",	daq.noise_R320_LPF35_min },	 
      { "Noise_R320_LPF35_max",	daq.noise_R320_LPF35_max },	 

      { "Linearity_G1_min", 	daq.linearity_G1_min     },
      { "Linearity_G1_max", 	daq.linearity_G1_max     },	 
      { "Linearity_G10_min", 	daq.linearity_G10_min    }, 	 
      { "Linearity_G10_max", 	daq.linearity_G10_max    } 	 
    };
    
    j["Summary"] = {
      { "Ntested"    , 0  },
      { "Ngood"      , 0  },
      { "Nbad"       , 0  },
      { "good_chips" , {} },
      { "bad_chips"  , {} }
    };

  } else {
    printf("Saving test results to DB: %s \n", daq.JSONdb);
    f_json >> j;
  }

// Make calibration of CATIA on test board (single channel)
  Int_t ich=daq.channel_number[0];
  daq.TP_width/=4.; // With LiTE-DTU >2.0, width is a multiple of 40 MHz clock

// CATIA settings if requested
  Int_t CATIA_number   = asic.CATIA_version[ich]*1000000+999000;
  Int_t CATIA_revision  = 1;
  Int_t ref_number[10], ref_revision[10], n_ref_number=0;
  FILE *fnumber=NULL;
  fnumber=fopen(".last_CATIA_number","r");
  if(fnumber != NULL)
  {
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;

    Int_t loc_version;
    while ((nread = getline(&line, &len, fnumber)) != -1)
    {
      if(line[0]=='#') continue;
      sscanf(line,"%2d",&loc_version);
      sscanf(line,"%d %d",&ref_number[n_ref_number],&ref_revision[n_ref_number]);
      printf("Number found : %d version : %d revision : %d\n",ref_number[n_ref_number],loc_version,ref_revision[n_ref_number]);
      if(loc_version==asic.CATIA_version[ich])
      {
        if(redo==0)
        {
          ref_number[n_ref_number]++;
          ref_revision[n_ref_number]=1;
        }
        else
          ref_revision[n_ref_number]++;
        CATIA_number=ref_number[n_ref_number];
        CATIA_revision=ref_revision[n_ref_number];
      }
      n_ref_number++;
    }
    free(line);
    fclose(fnumber);
  }
  else
  {
    ref_number[n_ref_number]=CATIA_number;
    ref_revision[n_ref_number]=CATIA_revision;
    n_ref_number++;
  }
  fnumber=fopen(".last_CATIA_number","w+");
  if(fnumber != NULL)
  {
    for(Int_t i=0; i<n_ref_number; i++)
    {
      fprintf(fnumber,"%d %d\n",ref_number[i],ref_revision[i]);
    }
    fclose(fnumber);
  }

  printf("Next CATIA number/revision : %d.%2.2d\n",CATIA_number,CATIA_revision);
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_CATIA_number");
    g_assert (widget);
    sprintf(widget_text,"%8.8d",CATIA_number);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_CATIA_test_revision");
    g_assert (widget);
    sprintf(widget_text,"%d",CATIA_revision);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
  }

//DTU settings if requested

  Int_t average        = 128;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vref, XADC_V2P5, XADC_V1P2;
  XADC_Vdac=0x10; // Buffered DAC signal for CATIA-V1-rev3 connected to APD_temp_in
  XADC_Temp=0x11; // Catia temperature connected to CATIA_temp_in
  if(daq.is_VICEPP)XADC_Temp=0x18;
  XADC_Vref=0x12; // Vref_reg connected to APD_temp_out (flying wire) (V1.4)
  XADC_Vref=XADC_Temp; // Vref_reg connected to CATIA_temp_in (V2.0)

  Int_t n_Vref=15, n_Vdac=33, n_PED_dac=64;

  TF1 *f1, *fDAC[2];
  sprintf(output_file,"");

  Int_t wait=0;
  UInt_t VFE_control;

  unsigned int ireg,device_number,val;
  ValWord<uint32_t> free_mem, trig_reg, delays, reg, current, fault;

// Switch ON and Initialize current monitors, send pwup_reset and measure currents :
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else
  {
    switch_power(TRUE);
  }
  Double_t I1P2_old=0;
  Double_t I2P5_old=0;
  Double_t V1P2_old=0;
  Double_t V2P5_old=0;
  Int_t n_wait=0;
  while((fabs(daq.I1P2_val-I1P2_old)>2 || fabs(daq.I2P5_val-I2P5_old)>2) && n_wait<10)
  {
    usleep(100000);
    get_consumption();
    I1P2_old=daq.I1P2_val;
    I2P5_old=daq.I2P5_val;
    n_wait++;
  }

  daq.I1P2_val-=asic.DTU_consumption_ref;
  I1P2_old=daq.I1P2_val;
  I2P5_old=daq.I2P5_val;
  V1P2_old=daq.V1P2_val;
  V2P5_old=daq.V2P5_val;
  printf("Found %f V and %f V\n",V1P2_old,V2P5_old);
  printf("%f %f %f %f %f %f\n", daq.I1P2_val, daq.I1P2_min, daq.I1P2_max,daq.I2P5_val, daq.I2P5_min, daq.I2P5_max);
  if(daq.I1P2_val<daq.I1P2_min || daq.I1P2_val>daq.I1P2_max)daq.power_valid=FALSE;
  if(daq.I2P5_val<daq.I2P5_min || daq.I2P5_val>daq.I2P5_max)daq.power_valid=FALSE;
  if(daq.test_power)
  {
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_value");
      g_assert (widget);
      sprintf(widget_text,"%.0f mA",daq.I1P2_val);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_value");
      g_assert (widget);
      sprintf(widget_text,"%.0f mA",daq.I2P5_val);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_power_valid");
      g_assert (widget);
      if(daq.power_valid)
      {
        gtk_widget_set_name(widget,"green_button");
        gtk_label_set_label((GtkLabel*)widget,"Passed");
      }
      else
      {
        gtk_widget_set_name(widget,"red_button");
        gtk_label_set_label((GtkLabel*)widget,"Failed");
        ret_code=-1;
      }
    }
    else
    {
      if(daq.power_valid)
      {
        printf("Power test : Passed\n");
      }
      else
      {
        printf("Power test : Failed\n");
        ret_code=-1;
      }
    }
  }
  if(!daq.power_valid)
  {
    printf("Power test failed, stop here !\n");
// Switch OFF the board
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_CATIA_valid");
      g_assert (widget);
      gtk_widget_set_name(widget,"red_button");
      sprintf(widget_text,"Failed(%d)",ret_code);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      switch_power(FALSE);
    }

    // Only increment Ntested if chip tested for the first time
    bool chip_in_goodlist = std::find(j["Summary"]["good_chips"].begin(), j["Summary"]["good_chips"].end(), CATIA_number) != std::end(j["Summary"]["good_chips"]);
    bool chip_in_badlist  = std::find(j["Summary"]["bad_chips"].begin(), j["Summary"]["bad_chips"].end(), CATIA_number) != std::end(j["Summary"]["bad_chips"]);

    if (!chip_in_goodlist && !chip_in_badlist) {
      j["Summary"]["Ntested"] = j["Summary"]["Ntested"].get<int>()+1;
      j["Summary"]["Nbad"] = j["Summary"]["Nbad"].get<int>()+1;
      j["Summary"]["bad_chips"]+=CATIA_number;  
    }
    
    std::string sCATIA_number = std::to_string(CATIA_number);
    j["Chip"][sCATIA_number]["test"]["power"] = daq.power_valid;

    // write prettified JSON to another file
    std::ofstream o(daq.JSONdb);
    o << std::setw(4) << j << std::endl;

    return ret_code;
  }
  chip_is_valid &= daq.power_valid;

  daq.I2C_low=0;
  daq.clock_select=0;
  update_trigger();

// DTU Resync init sequence:
// For LiTEDTU version >=2.0, put outputs in sync mode with 0xaaaaaaaa
  printf("Reset PLL/DTU/DTU_I2C/DTUTestUnit\n");
  send_ReSync_command((LiTEDTU_PLL_reset<<12) | (LiTEDTU_ADCTestUnit_reset<<8) | (LiTEDTU_DTU_reset<<4) | LiTEDTU_I2C_reset);
  printf("Reset ADCH/ADCL\n");
  send_ReSync_command((LiTEDTU_NORMAL_MODE<<8) | (LiTEDTU_ADCL_reset<<4) | LiTEDTU_ADCH_reset);

// Program LiTE-DTU (ADC 1 & 2 ON and 4 output lines
  Int_t num=asic.DTU_number[ich];
  device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU address

  update_LiTEDTU_reg(ich,0);
  update_LiTEDTU_reg(ich,1);

// G10 and G1 baseline subtraction :
  update_LiTEDTU_reg(ich,5);
  update_LiTEDTU_reg(ich,6);

  iret=I2C_RW(hw, device_number,  2, asic.DTU_driver_current[ich], 0, 1, 0);
  if(asic.DTU_PE_strength[ich]>=0)
    iret=I2C_RW(hw, device_number,  3, 0x80 | (asic.DTU_PE_width[ich]<<3) | (asic.DTU_PE_strength[ich]), 0, 1, 0);
  else
    iret=I2C_RW(hw, device_number,  3, 0x00, 0, 1, 0);
  iret=I2C_RW(hw, device_number, 18, 0x88, 0, 1, 0);
  iret=I2C_RW(hw, device_number, 19, 0x88, 0, 1, 0);
  iret=I2C_RW(hw, device_number, 20, 0x00, 0, 1, 0);
  update_LiTEDTU_reg(ich,17);

// PLL setting, usefull if we force PLL:
  update_LiTEDTU_reg(ich,16);
  update_LiTEDTU_reg(ich,15);
  usleep(10000);

// TP duration
  update_LiTEDTU_reg(ich,25);

  I2C_data=0xfff-asic.DTU_BL_G10[ich]; // switch gain at 4095 - substrated baseline
  printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,I2C_data);
  iret=I2C_RW(hw, device_number, 7, I2C_data&0xff, 0, 3, daq.debug_I2C);
  iret=I2C_RW(hw, device_number, 8, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);

// Force test mode for synchronization :
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan | eLINK_ACTIVE*daq.eLink_active |
               DTU_TEST_MODE*1 | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  usleep(1000);

  reg = hw.getNode("RESYNC_IDLE").read();
  hw.dispatch();
  UInt_t PLL_lock=(reg.value()>>DTU_PLL_LOCK)&0x1f;
  printf("PLL lock : %d",PLL_lock&1);
  printf("\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t jch=daq.channel_number[i];
    Int_t num=asic.DTU_number[jch];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    if(asic.DTU_version[jch]>=20)
    {
      iret=I2C_RW(hw, device_number, 9, 0, 0, 2, daq.debug_I2C);
      printf("PLL status registers 0x09 : 0x%2.2x, ",iret&0xff);
      iret=I2C_RW(hw, device_number, 10, 0, 0, 2, daq.debug_I2C);
      printf("0x0a : 0x%2.2x\n",iret&0xff);
    }
  }

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
  hw.dispatch();
  usleep(1000);

  hw.getNode("DTU_SYNC_PATTERN").write(0xeaaaaaaa); // Default synchro pattern during calibration with LiTEDTU
  reg = hw.getNode("DTU_SYNC_PATTERN").read();
  hw.dispatch();
  printf("Expected Sync patterns %d : 0x%8.8x\n",daq.DTU_test_mode,reg.value());
  hw.getNode("DELAY_CTRL").write((unsigned) DELAY_TAP_DIR*1 | (unsigned) START_IDELAY_SYNC*0x1f);
  hw.dispatch();
  usleep(1000);
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1);
  hw.dispatch();
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Get sync status : 0x%8.8x, VFE  sync %d, FE sync %d, ch1 0x%2.2x, ch2 0x%2.2x, ch3 0x%2.2x, ch4 0x%2.2x, ch5 0x%2.2x\n",
          reg.value(), (reg.value()>>31)&1, (reg.value()>>30)&1,
         (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);

// Read voltages measured by controler
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    double Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%s measured power supply voltage %7.3f V\n",alim,Vmeas);
    val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%s measured sense voltage %7.3f V\n",alim,Vmeas);
  }

// Set CATIA outputs with calibration levels
  asic.CATIA_Vref_out[ich]=0;
  daq.CATIA_Vcal_out=TRUE;
  iret=update_CATIA_reg(ich,5);
  printf("Set CATIA outputs with calibration levels\n");
  usleep(100000); // Let some time for calibration voltages to stabilize

  //hw.getNode("DELAY_CTRL").write(DELAY_RESET*1);
  //hw.dispatch();
  send_ReSync_command(LiTEDTU_ADCH_reset<<4 | LiTEDTU_ADCL_reset);
  usleep(10000);
  printf("Launch ADC calibration !\n");
  send_ReSync_command(LiTEDTU_ADCH_calib<<4 | LiTEDTU_ADCL_calib);
  usleep(10000);

// Reset Test unit to get samples in right order :
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);

// Restore CAL mux in normal position after calibration
  daq.CATIA_Vcal_out=FALSE;
  iret=update_CATIA_reg(ich,5);
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan | eLINK_ACTIVE*daq.eLink_active |
               DTU_TEST_MODE*1 | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  printf("Set CATIA outputs with physics levels : 0x%4.4x\n",iret&0xFFFF);
  usleep(100000); // Let some time for DC voltages to stabilize

// Read Temperature and others analog voltages with FPGA ADC
// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on CATIA :
  asic.CATIA_temp_out[ich]=0;
  asic.CATIA_temp_X5[ich]=0;
  iret=update_CATIA_reg(ich,1);

//===========================================================================================================
// 0 : Get Vdac vs DAC transfer function for current injection 
// Now, make the temp measurement on all catias, sequentially end for both current gain
  TGraph *tg_Vref=new TGraph();
  tg_Vref->SetLineColor(kRed);
  tg_Vref->SetMarkerColor(kRed);
  tg_Vref->SetMarkerStyle(20);
  tg_Vref->SetMarkerSize(1.);
  tg_Vref->SetName("Vref_vs_reg");
  tg_Vref->SetTitle("Vref_vs_reg");
  TGraph *tg_Vdac[2], *tg_Vdac_resi[2];
  tg_Vdac[0] =new TGraph();
  tg_Vdac[0]->SetLineColor(kRed);
  tg_Vdac[0]->SetMarkerColor(kRed);
  tg_Vdac[0]->SetMarkerStyle(20);
  tg_Vdac[0]->SetMarkerSize(1.);
  tg_Vdac[0]->SetName("Vdac1_vs_reg");
  tg_Vdac[0]->SetTitle("Vdac1_vs_reg");
  tg_Vdac[1] =new TGraph();
  tg_Vdac[1]->SetLineColor(kBlue);
  tg_Vdac[1]->SetMarkerColor(kBlue);
  tg_Vdac[1]->SetMarkerStyle(20);
  tg_Vdac[1]->SetMarkerSize(1.);
  tg_Vdac[1]->SetName("Vdac2_vs_reg");
  tg_Vdac[1]->SetTitle("Vdac2_vs_reg");
  tg_Vdac_resi[0] =new TGraph();
  tg_Vdac_resi[0]->SetLineColor(kRed);
  tg_Vdac_resi[0]->SetMarkerColor(kRed);
  tg_Vdac_resi[0]->SetMarkerStyle(20);
  tg_Vdac_resi[0]->SetMarkerSize(1.);
  tg_Vdac_resi[0]->SetName("Vdac1_resi_vs_reg");
  tg_Vdac_resi[0]->SetTitle("Vdac1_resi_vs_reg");
  tg_Vdac_resi[1] =new TGraph();
  tg_Vdac_resi[1]->SetLineColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerStyle(20);
  tg_Vdac_resi[1]->SetMarkerSize(1.);
  tg_Vdac_resi[1]->SetName("Vdac2_resi_vs_reg");
  tg_Vdac_resi[1]->SetTitle("Vdac2_resi_vs_reg");
  Double_t DAC_val[33]={0.}, Vdac[2][33]={0.}, Vdac_offset[2]={0.}, Vdac_slope[2]={0.}, Vref[15]={0.}, Temp[3]={0.};
  Int_t ref_best=0;
  Double_t dref_best=99999., Vref_best=-1.;
  if(chip_is_valid)
  {
    for(int XADC_meas=0; XADC_meas<5; XADC_meas++)
    {
      XADC_reg=0;
      if(XADC_meas==1)XADC_reg=XADC_Temp; // Temperature measurement
      if(XADC_meas==2)XADC_reg=XADC_Vref; // CATIA Vref measurement (pin 22)
      if(XADC_meas==3)XADC_reg=XADC_Vdac; // CATIA Vdac1 measurement (pin 25)
      if(XADC_meas==4)XADC_reg=XADC_Vdac; // CATIA Vdac2 measurement (pin 25)

      Int_t nmeas=1;
      if(XADC_meas==1)nmeas=2;
      if(XADC_meas==2)nmeas=n_Vref;
      if(XADC_meas==3)nmeas=n_Vdac;
      if(XADC_meas==4)nmeas=n_Vdac;
      for(int imeas=0; imeas<nmeas; imeas++)
      {
        int loc_meas=imeas;
        if(XADC_meas==1) // CATIA Temp measurements
        {
          asic.CATIA_temp_out[ich]=1;
          asic.CATIA_temp_X5[ich]=imeas;
          asic.CATIA_Vref_out[ich]=0;
          iret=update_CATIA_reg(ich,1);
          iret=update_CATIA_reg(ich,5);
          usleep(10000);
        }
        if(XADC_meas==2) // Scan Vref_reg values
        {
          asic.CATIA_temp_out[ich]=0;
          asic.CATIA_temp_X5[ich]=0;
          asic.CATIA_Vref_out[ich]=1;
          iret=update_CATIA_reg(ich,1);
          iret=update_CATIA_reg(ich,5);
          if(imeas>0)loc_meas=imeas+1;
  // Do CATIA update through gtk menus
          asic.CATIA_Vref_val[ich]=loc_meas;
          iret=update_CATIA_reg(ich,6);
        }
        if(XADC_meas==3) // Scan DAC1 values
        {
          asic.CATIA_temp_out[ich]=0;
          asic.CATIA_temp_X5[ich]=0;
          asic.CATIA_Vref_out[ich]=0;
          iret=update_CATIA_reg(ich,1);
          if(imeas==0)
          {
            asic.CATIA_Vref_val[ich]=ref_best;
            iret=update_CATIA_reg(ich,6);
          }
          loc_meas=imeas*128;
          if(loc_meas>4095)loc_meas=4095;
          asic.CATIA_DAC1_status[ich]=TRUE;
          asic.CATIA_DAC1_val[ich]=loc_meas;
          asic.CATIA_DAC2_status[ich]=FALSE;
          asic.CATIA_DAC2_val[ich]=loc_meas;
          iret=update_CATIA_reg(ich,4);
          iret=update_CATIA_reg(ich,5);
          if(imeas==0)usleep(100000);
          usleep(1000);
        }
        if(XADC_meas==4) // Scan Vdac values
        {
          loc_meas=imeas*128;
          if(loc_meas>4095)loc_meas=4095;
          asic.CATIA_DAC1_status[ich]=FALSE;
          asic.CATIA_DAC1_val[ich]=loc_meas;
          asic.CATIA_DAC2_status[ich]=TRUE;
          asic.CATIA_DAC2_val[ich]=loc_meas;
          iret=update_CATIA_reg(ich,4);
          iret=update_CATIA_reg(ich,5);
          if(imeas==0)usleep(100000);
          usleep(1000);
        }
        if(!daq.cli)
        {
          while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
        }

        double ave_val=0.;
        ValWord<uint32_t> temp;
        for(int iave=0; iave<average; iave++)
        {
          command=DRP_WRb*0 | (XADC_reg<<16);
          hw.getNode("DRP_XADC").write(command);
          temp  = hw.getNode("DRP_XADC").read();
          hw.dispatch();
          double loc_val=double((temp.value()&0xffff)>>4)/4096.;
          ave_val+=loc_val;
        }
        ave_val/=average;

        if(XADC_meas==0)
        {
          Temp[0]=ave_val*4096*0.123-273.;
          printf("FPGA temperature, meas %d : %.2f deg\n",imeas, Temp[0]);
        }
        else if(XADC_meas==1)
        {
          ave_val/=daq.Tsensor_divider;
          Temp[imeas+1]=ave_val*1000.;
          printf("CATIA 3, X5 %d, temperature : %.0f mV\n", imeas,Temp[imeas+1]);
          if(daq.test_temperature)
          {
            if(imeas==0 && (Temp[imeas+1]<daq.temp_X1_min || Temp[imeas+1]>daq.temp_X1_max))daq.temperature_valid=FALSE;
            if(imeas==1 && (Temp[imeas+1]<daq.temp_X5_min || Temp[imeas+1]>daq.temp_X5_max))daq.temperature_valid=FALSE;
            if(!daq.cli)
            {
              if(imeas==0) widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_value");
              else         widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_value");
              g_assert (widget);
              sprintf(widget_text,"%.0f mV",Temp[imeas+1]);
              gtk_label_set_label((GtkLabel*)widget,widget_text);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_valid");
              g_assert (widget);
              if(daq.temperature_valid)
              {
                gtk_widget_set_name(widget,"green_button");
                gtk_label_set_label((GtkLabel*)widget,"Passed");
              }
              else
              {
                gtk_widget_set_name(widget,"red_button");
                gtk_label_set_label((GtkLabel*)widget,"Failed");
                ret_code=-2;
              }
            }
            else
            {
              if(daq.temperature_valid)
              {
                printf("Temperature test : Passed\n");
              }
              else
              {
                printf("Temperature test : Failed\n");
                ret_code=-2;
              }
            }
          }
        }
        else if(XADC_meas==2)
        {
          ave_val/=daq.Tsensor_divider; // 4.02k-1k Resistor bridge on PCB
          printf("CATIA 0, Vref DAC %d, Vref : %.4f V\n", loc_meas,ave_val);
          Vref[imeas]=ave_val;
          tg_Vref->SetPoint(imeas,(Double_t)loc_meas,Vref[imeas]);
          if(fabs(Vref[imeas]-Vref_ref)<dref_best)
          {
            dref_best=fabs(Vref[imeas]-Vref_ref);
            ref_best=loc_meas;
            Vref_best=Vref[imeas];
            printf("%d %f %d\n",loc_meas,dref_best,ref_best);
          }
          if(CATIA_number<20000000)ref_best=3;
          if(daq.test_bandgap && imeas==nmeas-1)
          {
            if(Vref_best<daq.bandgap_value_min || Vref_best>daq.bandgap_value_max)daq.bandgap_valid=FALSE;
            if(ref_best<daq.bandgap_setting_min || ref_best>daq.bandgap_setting_max)daq.bandgap_valid=FALSE;
            if(!daq.cli)
            {
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_best_value");
              g_assert (widget);
              sprintf(widget_text,"%.3f V",Vref_best);
              gtk_label_set_label((GtkLabel*)widget,widget_text);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_best_setting");
              g_assert (widget);
              sprintf(widget_text,"%d",ref_best);
              gtk_label_set_label((GtkLabel*)widget,widget_text);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_valid");
              g_assert (widget);
              if(daq.bandgap_valid)
              {
                gtk_widget_set_name(widget,"green_button");
                gtk_label_set_label((GtkLabel*)widget,"Passed");
              }
              else
              {
                gtk_widget_set_name(widget,"red_button");
                gtk_label_set_label((GtkLabel*)widget,"Failed");
                ret_code=-3;
              }
            }
            else
            {
              if(daq.bandgap_valid)
              {
                printf("Bandgap test : Passed\n");
              }
              else
              {
                printf("Bandgap test : Failed\n");
                ret_code=-3;
              }
            }
          }
        }
        else if(XADC_meas==3)
        {
          //printf("CATIA 0, TP DAC1 %d, Vdac : %.4f V\n", loc_meas, ave_val);
          ave_val*=2.;
          tg_Vdac[0]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
          DAC_val[imeas]=loc_meas;
          Vdac[0][imeas]=ave_val;
          //printf("DAC %d : %f\n",loc_meas,ave_val);
        }
        else if(XADC_meas==4)
        {
          //printf("CATIA 0, TP DAC2 %d, Vdac : %.4f V\n", loc_meas, ave_val);
          ave_val*=2.;
          tg_Vdac[1]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
          Vdac[1][imeas]=ave_val;
        }
      }
      if(!daq.cli)
      {
        while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
      }
    }

    if(daq.debug_draw && !daq.cli)daq.c1->cd();
    for(int iDAC=0; iDAC<2; iDAC++)
    {
      tfr[iDAC]=tg_Vdac[iDAC]->Fit("pol1","WQ",fit_opt,0.,3500.);
      fDAC[iDAC]=tg_Vdac[iDAC]->GetFunction("pol1");
      Int_t n=tg_Vdac[iDAC]->GetN();
      for(Int_t i=0; i<n; i++)
      {
        Double_t x,y;
        tg_Vdac[iDAC]->GetPoint(i,x,y);
        if(fDAC[iDAC]!=NULL)
          tg_Vdac_resi[iDAC]->SetPoint(i,x,y-fDAC[iDAC]->Eval(x));
        else
          tg_Vdac_resi[iDAC]->SetPoint(i,x,0.);
      }
      Vdac_offset[iDAC]=0.;
      Vdac_slope[iDAC]=0.;
      if(fDAC[iDAC]!=NULL)
      {
        Vdac_offset[iDAC]=fDAC[iDAC]->GetParameter(0)*1000.;
        Vdac_slope[iDAC]=fDAC[iDAC]->GetParameter(1)*1000.;
      }
      printf("Vdac%d offset : %e mV, slope : %e mV/lsb\n",iDAC,Vdac_offset[iDAC],Vdac_slope[iDAC]); 
    }

    if(daq.test_DAC1)
    {
      if(Vdac_offset[0]<daq.DAC1_offset_min || Vdac_offset[0]>daq.DAC1_offset_max)daq.DAC1_valid=FALSE;
      if(Vdac_slope[0]<daq.DAC1_slope_min || Vdac_slope[0]>daq.DAC1_slope_max)daq.DAC1_valid=FALSE;
      if(!daq.cli)
      {
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_value");
        g_assert (widget);
        sprintf(widget_text,"%.1f mV",Vdac_offset[0]);
        gtk_label_set_label((GtkLabel*)widget,widget_text);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_value");
        g_assert (widget);
        sprintf(widget_text,"%.3f mV/lsb",Vdac_slope[0]);
        gtk_label_set_label((GtkLabel*)widget,widget_text);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_valid");
        g_assert (widget);
        if(daq.DAC1_valid)
        {
          gtk_widget_set_name(widget,"green_button");
          gtk_label_set_label((GtkLabel*)widget,"Passed");
        }
        else
        {
          gtk_widget_set_name(widget,"red_button");
          gtk_label_set_label((GtkLabel*)widget,"Failed");
          ret_code=-4;
        }
      }
      else
      {
        if(daq.DAC1_valid)
        {
          printf("DAC1 test : Passed\n");
        }
        else
        {
          printf("DAC1 test : Failed\n");
          ret_code=-4;
        }
      }
    }
    if(daq.test_DAC2)
    {
      if(Vdac_offset[1]<daq.DAC2_offset_min || Vdac_offset[1]>daq.DAC2_offset_max)daq.DAC2_valid=FALSE;
      if(Vdac_slope[1]<daq.DAC2_slope_min || Vdac_slope[1]>daq.DAC2_slope_max)daq.DAC2_valid=FALSE;
      if(!daq.cli)
      {
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_value");
        g_assert (widget);
        sprintf(widget_text,"%.1f mV",Vdac_offset[1]);
        gtk_label_set_label((GtkLabel*)widget,widget_text);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_value");
        g_assert (widget);
        sprintf(widget_text,"%.3f mV/lsb",Vdac_slope[1]);
        gtk_label_set_label((GtkLabel*)widget,widget_text);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_valid");
        g_assert (widget);
        if(daq.DAC2_valid)
        {
          gtk_widget_set_name(widget,"green_button");
          gtk_label_set_label((GtkLabel*)widget,"Passed");
        }
        else
        {
          gtk_widget_set_name(widget,"red_button");
          gtk_label_set_label((GtkLabel*)widget,"Failed");
          ret_code=-5;
        }
      }
      else
      {
        if(daq.DAC2_valid)
        {
          printf("DAC2 test : Passed\n");
        }
        else
        {
          printf("DAC2 test : Failed\n");
          ret_code=-5;
        }
      }
    }
    if(!daq.cli)
    {
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
    }
  }
  chip_is_valid &= daq.temperature_valid;
  chip_is_valid &= daq.bandgap_valid;
  chip_is_valid &= daq.DAC1_valid;
  chip_is_valid &= daq.DAC2_valid;

// Read current values for control :
  printf("Consumption with DAC1 OFF and DAC2 ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  if(wait==2)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

// Prepare CATIA for calibration :
// Switch OFF DACs and re-measure power consumption :
  asic.CATIA_DAC1_status[ich]=FALSE;
  asic.CATIA_DAC1_val[ich]=600;
  asic.CATIA_DAC2_status[ich]=FALSE;
  asic.CATIA_DAC2_val[ich]=600;
  asic.CATIA_Rconv[ich]=0;
  iret=update_CATIA_reg(ich,4);
  iret=update_CATIA_reg(ich,5);
  iret=update_CATIA_reg(ich,6);
  usleep(200000);

// Read current values for control :
  printf("Consumption with DAC1 OFF and DAC2 OFF, Vref ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  if(wait==2)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

//===========================================================================================================
// 0.9 : Redo ADC calibration after Vref_reg setting :
// Stop LVRB auto scan, not to generate noise in CATIA during calibration

  calib_ADC();

  if(wait==2)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  if(daq.ADC_use_ref_calib)
  {
// Read ADC calibration (full list of registers :
    sprintf(output_file,"data/ADC_ref_calib.dat");
    printf("Read reference ADC calibration from file %s\n",output_file);
    fcal=fopen(output_file,"r");
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      //printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xFF, ADC_reg_val[1][ireg]&0xFF);
      fscanf(fcal,"%d %d",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
    }
    fclose(fcal);
    for(int iADC=0; iADC<2; iADC++)
    {
      Int_t num=asic.DTU_number[daq.channel_number[0]];
      device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;      // ADC sub-addresses
      for(int ireg=0; ireg<N_ADC_REG; ireg++)
      {
        ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, 0);
      }
    }
  }

// Reset Test unit to get samples in right order :
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);

// Read current values for control :
  printf("After LiTE-DTU calibration :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);

//===========================================================================================================
// 1 : Pedestal value vs PED_DAC value.
// Read 1 event of 1000 samples
  printf("Start Pedestal study\n");
  TGraph *tg_ped_DAC[2];
  tg_ped_DAC[0]=new TGraph();
  tg_ped_DAC[0]->SetLineColor(kRed);
  tg_ped_DAC[0]->SetMarkerColor(kRed);
  tg_ped_DAC[0]->SetMarkerSize(kRed);
  tg_ped_DAC[0]->SetName("Ped_vs_DAC_G1");
  tg_ped_DAC[0]->SetTitle("Ped_vs_DAC_G1");
  tg_ped_DAC[1]=new TGraph();
  tg_ped_DAC[1]->SetLineColor(kBlue);
  tg_ped_DAC[1]->SetMarkerColor(kBlue);
  tg_ped_DAC[1]->SetMarkerSize(kBlue);
  tg_ped_DAC[1]->SetName("Ped_vs_DAC_G10");
  tg_ped_DAC[1]->SetTitle("Ped_vs_DAC_G10");
  Double_t ped_DAC_par[2][2]={0.};
  daq.trigger_type = 0;
  daq.nevent       = 1;
  daq.nsample      = 250;

// Switch to triggered mode + internal trigger, reduce noise by disabling CALIB_PULSE  :
  daq.calib_pulse_enabled    =0;
  daq.calib_mux_enabled      =0;
  daq.clock_select           =0;
  daq.I2C_low                =0;
  daq.self_trigger_mode      =0;
  daq.self_trigger_mask      =0;
  daq.self_trigger_threshold =0;
  daq.self_trigger           =0;
  daq.self_trigger_loop      =0;
  daq.fifo_mode              =1;
  update_trigger();

  //fead_ctrl=  CALIB_PULSE_ENABLED   *0 |
  //            SELF_TRIGGER_MASK     *0 |
  //            SELF_TRIGGER_THRESHOLD*0 |
  //            SELF_TRIGGER          *0 |
  //            SELF_TRIGGER_LOOP     *0 |
  //            FIFO_MODE             *1 |
  //            RESET                 *0;
  //hw.getNode("FEAD_CTRL").write(fead_ctrl);
  //hw.dispatch();

  //Int_t num=asic.CATIA_number[daq.channel_number[0]];
  //device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;      // CATIA address
  //if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+((ich+1)<<daq.I2C_shift_dev_number)+0xb;
  //val=I2C_RW(hw, device_number, 1, 2,0, 3, daq.debug_I2C); // Switch off temp sensor
  //val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, daq.debug_I2C); // Switch off TP stuff except Vref 
  //val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, daq.debug_I2C);
  //val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0B, 0, 3, daq.debug_I2C);
  //usleep(100000);


  asic.CATIA_gain[ich]=3;
  asic.CATIA_LPF35[ich]=1;
  iret=update_CATIA_reg(ich,3);

  Int_t ped_ref[3][2]={-1,-1,-1,-1,-1,-1};
  if(chip_is_valid && daq.test_pedestal)
  {
    for(int iRTIA=0; iRTIA<3; iRTIA++)
    {
      Int_t jRTIA=iRTIA;
      if(iRTIA==2)jRTIA=3;

      for(int idac=0; idac<n_PED_dac; idac++)
      {
        mean[0]=0.;
        mean[1]=0.;
        mean[2]=0.;
        mean[3]=0.;
        mean[4]=0.;
        mean[5]=0.;
        //printf("VCM DAC : %d\n",idac);
        asic.CATIA_ped_G10[ich]=idac;
        asic.CATIA_ped_G1[ich]=idac;
        asic.CATIA_gain[ich] =jRTIA;
        iret=update_CATIA_reg(ich,3);
        usleep(100000);
        if(daq.debug_draw)draw=(idac%8);
        for(int ievt=0; ievt<daq.nevent; ievt++)
        {
          get_event(1, draw);
          draw=0;
          for(Int_t is=0; is<daq.all_sample[0]; is++)
          {
            mean[0]+=daq.fevent[0][is]*dv;
            mean[1]+=daq.fevent[1][is]*dv;
            mean[2]+=daq.fevent[2][is]*dv;
            mean[3]+=daq.fevent[3][is]*dv;
          }
          for(Int_t is=0; is<daq.all_sample[4]; is++)
          {
            mean[4]+=daq.fevent[4][is]*dv;
            mean[5]+=daq.fevent[5][is]*dv;
          }
        }
        mean[0]/=(daq.all_sample[0]*daq.nevent);
        mean[1]/=(daq.all_sample[0]*daq.nevent);
        mean[2]/=(daq.all_sample[0]*daq.nevent);
        mean[3]/=(daq.all_sample[0]*daq.nevent);
        mean[4]/=(daq.all_sample[4]*daq.nevent);
        mean[5]/=(daq.all_sample[4]*daq.nevent);
        tg_ped_DAC[0]->SetPoint(idac,(Double_t)idac,mean[4]);
        tg_ped_DAC[1]->SetPoint(idac,(Double_t)idac,mean[5]);
  // keep tuning which gives pedestal below 30 lsb or the first-1 which makes the pedestal value saturate.
        //if(fabs(mean[4]/dv-mean_prev[4]/dv)<5. && ped_ref[iRTIA][0]<0.)ped_ref[iRTIA][0]=idac-1;
        //if(fabs(mean[5]/dv-mean_prev[5]/dv)<5. && ped_ref[iRTIA][1]<0.)ped_ref[iRTIA][1]=idac-1;
        if(mean[4]/dv<30. && ped_ref[iRTIA][0]<0.)ped_ref[iRTIA][0]=idac;
        if(mean[5]/dv<30. && ped_ref[iRTIA][1]<0.)ped_ref[iRTIA][1]=idac;
        for(Int_t i=0; i<6; i++)mean_prev[i]=mean[i];
        printf("%d %f (%f %f) %d, %f (%f %f) %d\n",idac,mean[4]/dv,mean[0]/dv,mean[1]/dv,ped_ref[iRTIA][0],mean[5]/dv,mean[2]/dv,mean[3]/dv,ped_ref[iRTIA][1]);
        if(wait==2)
        {
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q')exit(-1);
        }
        if(mean[4]<0.01 && mean[5]<0.01) break;
      }
      ped_ref[iRTIA][0]&=0x3F;
      ped_ref[iRTIA][1]&=0x3F;
      if(daq.debug_draw)daq.c1->cd();
      tfr1=tg_ped_DAC[0]->Fit("pol1","WQ",fit_opt,0.,ped_ref[iRTIA][0]);
      f1=tg_ped_DAC[0]->GetFunction("pol1");
      ped_DAC_par[0][0]=0.;
      ped_DAC_par[0][1]=0.;
      if(f1!=NULL)
      {
        ped_DAC_par[0][0]=f1->GetParameter(0);
        ped_DAC_par[0][1]=f1->GetParameter(1);
        f1->~TF1();
      }
      tfr1.~TFitResultPtr();
      printf("G10  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[0][0],ped_DAC_par[0][1]);
      tfr1=tg_ped_DAC[1]->Fit("pol1","WQ",fit_opt,0.,ped_ref[iRTIA][1]);
      f1=tg_ped_DAC[1]->GetFunction("pol1");
      ped_DAC_par[1][0]=0.;
      ped_DAC_par[1][1]=0.;
      if(f1!=NULL)
      {
        ped_DAC_par[1][0]=f1->GetParameter(0);
        ped_DAC_par[1][1]=f1->GetParameter(1);
        f1->~TF1();
      }
      tfr1.~TFitResultPtr();
      printf("G1  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[1][0],ped_DAC_par[1][1]);
    }

    sprintf(widget_name,"3_pedestal_G10_offset_value");
    sprintf(widget_text,"%.1f mV",ped_DAC_par[0][0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name, widget_text);
    }
    sprintf(widget_name,"3_pedestal_G10_slope_value");
    sprintf(widget_text,"%.1f mV/lsb",ped_DAC_par[0][1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name, widget_text);
    }
    sprintf(widget_name,"3_pedestal_G10_best_value");
    sprintf(widget_text,"%d",ped_ref[2][0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name, widget_text);
    }
    sprintf(widget_name,"3_pedestal_G1_offset_value");
    sprintf(widget_text,"%.1f mV",ped_DAC_par[1][0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name, widget_text);
    }
    sprintf(widget_name,"3_pedestal_G1_slope_value");
    sprintf(widget_text,"%.1f mV/lsb",ped_DAC_par[1][1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name, widget_text);
    }
    sprintf(widget_name,"3_pedestal_G1_best_value");
    sprintf(widget_text,"%d",ped_ref[2][1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name, widget_text);
    }
    if(ped_DAC_par[0][0]<daq.ped_G10_offset_min || ped_DAC_par[0][0]>daq.ped_G10_offset_max)daq.pedestal_valid=FALSE;
    if(ped_DAC_par[0][1]<daq.ped_G10_slope_min || ped_DAC_par[0][1]>daq.ped_G10_slope_max)daq.pedestal_valid=FALSE;
    if(ped_ref[2][0]<daq.ped_G10_best_min || ped_ref[2][0]>daq.ped_G10_best_max)daq.pedestal_valid=FALSE;
    if(ped_DAC_par[1][0]<daq.ped_G1_offset_min || ped_DAC_par[1][0]>daq.ped_G1_offset_max)daq.pedestal_valid=FALSE;
    if(ped_DAC_par[1][1]<daq.ped_G1_slope_min || ped_DAC_par[1][1]>daq.ped_G1_slope_max)daq.pedestal_valid=FALSE;
    if(ped_ref[2][1]<daq.ped_G1_best_min || ped_ref[2][1]>daq.ped_G1_best_max)daq.pedestal_valid=FALSE;
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_valid");
      g_assert (widget);
      if(daq.pedestal_valid)
      {
        gtk_widget_set_name(widget,"green_button");
        gtk_label_set_label((GtkLabel*)widget,"Passed");
      }
      else
      {
        gtk_widget_set_name(widget,"red_button");
        gtk_label_set_label((GtkLabel*)widget,"Failed");
        ret_code=-6;
      }
    }
    else
    {
      if(daq.pedestal_valid)
      {
        printf("Pedestal test : Passed\n");
      }
      else
      {
        printf("Pedestal test : Failed\n");
        ret_code=-6;
      }
    }
  }
  if(!daq.cli)
  {
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
  }
  chip_is_valid &= daq.pedestal_valid;

//===========================================================================================================
// 2 : Noise for R=320, R=380 and R=470 and for LPF=50MHz and LPF=35 MHz
  Double_t G1_noise[3][2]={0.,0.,0.,0.,0.,0.};
  Double_t G10_noise[3][2]={0.,0.,0.,0.,0.,0.};
  daq.trigger_type=0;
  daq.nevent=1;
  daq.nsample=26622;

  Double_t *rex,*rey,*imx,*imy,*mod;
// In test mode, we have 2 samples/word on even lines and 2 sample/word in odd lines
// Thus 4 samples per event in merged lines 4 (G10) and 5 (G1)
  rex=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  imx=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  rey=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  imy=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  mod=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  Int_t loc_nsample=daq.nsample*4;
  TVirtualFFT *fft_f=TVirtualFFT::FFT(1,&loc_nsample,"C2CF K");
  Double_t dt=1./NSPS;
  Double_t tmax=loc_nsample*dt;
  Double_t fmax=1./dt;
  Double_t df=fmax/(loc_nsample);
  TProfile *pmod[2];
  pmod[0]=new TProfile("NDS_G10","NDS_G10",loc_nsample,0,fmax);
  pmod[1]=new TProfile("NDS_G1","NDS_G1",loc_nsample,0,fmax);
  TH1D *hmod[2][6];
  hmod[0][0]=new TH1D("NDS_G10_RTIA_470_LPF_50","NDS_G10_RTIA_470_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[0][1]=new TH1D("NDS_G10_RTIA_470_LPF_35","NDS_G10_RTIA_470_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[0][2]=new TH1D("NDS_G10_RTIA_380_LPF_50","NDS_G10_RTIA_380_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[0][3]=new TH1D("NDS_G10_RTIA_380_LPF_35","NDS_G10_RTIA_380_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[0][4]=new TH1D("NDS_G10_RTIA_320_LPF_50","NDS_G10_RTIA_320_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[0][5]=new TH1D("NDS_G10_RTIA_320_LPF_35","NDS_G10_RTIA_320_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[1][0]=new TH1D("NDS_G1_RTIA_470_LPF_50","NDS_G1_RTIA_470_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[1][1]=new TH1D("NDS_G1_RTIA_470_LPF_35","NDS_G1_RTIA_470_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[1][2]=new TH1D("NDS_G1_RTIA_380_LPF_50","NDS_G1_RTIA_380_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[1][3]=new TH1D("NDS_G1_RTIA_380_LPF_35","NDS_G1_RTIA_380_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[1][4]=new TH1D("NDS_G1_RTIA_320_LPF_50","NDS_G1_RTIA_320_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[1][5]=new TH1D("NDS_G1_RTIA_320_LPF_35","NDS_G1_RTIA_320_LPF_35",loc_nsample/2,0.,fmax/2.);
  TGraph *tg_pulse[2];
  tg_pulse[0]=new TGraph();
  tg_pulse[0]->SetLineColor(kRed);
  tg_pulse[0]->SetMarkerColor(kRed);
  tg_pulse[0]->SetMarkerSize(0.1);
  tg_pulse[0]->SetTitle("G10_R320_LPF35_pedestal_frame");
  tg_pulse[0]->SetName("G10_R320_LPF35_pedestal_frame");
  tg_pulse[1]=new TGraph();
  tg_pulse[1]->SetLineColor(kRed);
  tg_pulse[1]->SetMarkerColor(kRed);
  tg_pulse[1]->SetMarkerSize(0.1);
  tg_pulse[1]->SetTitle("G1_R320_LPF35_pedestal_frame");
  tg_pulse[1]->SetName("G1_R320_LPF35_pedestal_frame");

  if(chip_is_valid && daq.test_noise)
  {
    for(int iRTIA=2; iRTIA>=0; iRTIA--)
    {
      Int_t jRTIA=iRTIA;
      if(iRTIA==2)jRTIA=3;

      for(int iLPF=0; iLPF<2; iLPF++)
      {
        pmod[0]->Reset();
        pmod[1]->Reset();

        asic.CATIA_ped_G10[ich]=ped_ref[iRTIA][0];
        asic.CATIA_ped_G1[ich] =ped_ref[iRTIA][1];
        asic.CATIA_LPF35[ich] =iLPF;
        asic.CATIA_gain[ich] =jRTIA;
        iret=update_CATIA_reg(ich,3);
        usleep(100000);
        if(daq.debug_draw)draw=1;
        for(int ievt=0; ievt<daq.nevent; ievt++)
        {
          ped[0]=0.,rms[0]=0.;
          ped[1]=0.,rms[1]=0.;
          get_event(1,draw);
          draw=0;

          Double_t loc_rms=0., loc_ped=0.;
          Int_t ngood=0;
          for(Int_t is=0; is<daq.all_sample[4]; is++)
          {
            loc_ped+=daq.fevent[4][is]*dv;
            loc_rms+=daq.fevent[4][is]*dv*daq.fevent[4][is]*dv;
            Double_t t=dt*is;
            if(iRTIA==1 && iLPF==1 && ievt==0) tg_pulse[0]->SetPoint(is,t,daq.fevent[4][is]*dv);
            rex[is]=daq.fevent[4][is]*dv;
            imx[is]=0.;
          }
          loc_ped/=daq.all_sample[4];
          loc_rms/=daq.all_sample[4];
          loc_rms=sqrt(loc_rms-loc_ped*loc_ped);
          for(Int_t is=0; is<daq.all_sample[4]; is++)
          {
            if(fabs(daq.fevent[4][is]*dv-loc_ped)<loc_rms*5.)
            {
              ped[0]+=daq.fevent[4][is]*dv;
              rms[0]+=daq.fevent[4][is]*dv*daq.fevent[4][is]*dv;
              ngood++;
            }
          }
          ped[0]/=ngood;
          rms[0]/=ngood;
          rms[0]=rms[0]-ped[0]*ped[0];

          G10_noise[iRTIA][iLPF]+=rms[0];
          fft_f->SetPointsComplex(rex,imx);
          fft_f->Transform();
          fft_f->GetPointsComplex(rey,imy);
          for(Int_t is=0; is<daq.all_sample[4]; is++)
          {
            Double_t f=df*(is+0.5);
            rey[is]/=daq.all_sample[4];
            imy[is]/=daq.all_sample[4];
            mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
            pmod[0]->Fill(f,mod[is]/df);
          }

          loc_ped=0.; loc_rms=0.; ngood=0;
          for(Int_t is=0; is<daq.all_sample[5]; is++)
          {
            loc_ped+=daq.fevent[5][is]*dv;
            loc_rms+=daq.fevent[5][is]*dv*daq.fevent[5][is]*dv;
            Double_t t=dt*is;
            if(iRTIA==1 && iLPF==1 && ievt==0)tg_pulse[1]->SetPoint(is,t,daq.fevent[5][is]*dv);
            rex[is]=daq.fevent[5][is]*dv;
            imx[is]=0.;
          }
          loc_ped/=daq.all_sample[5];
          loc_rms/=daq.all_sample[5];
          loc_rms=sqrt(loc_rms-loc_ped*loc_ped);
          for(Int_t is=0; is<daq.all_sample[5]; is++)
          {
            if(fabs(daq.fevent[5][is]*dv-loc_ped)<loc_rms*5.)
            {
              ped[1]+=daq.fevent[5][is]*dv;
              rms[1]+=daq.fevent[5][is]*dv*daq.fevent[5][is]*dv;
              ngood++;
            }
          }
          ped[1]/=ngood;
          rms[1]/=ngood;
          rms[1]=rms[1]-ped[1]*ped[1];
          G1_noise[iRTIA][iLPF]+=rms[1];
          fft_f->SetPointsComplex(rex,imx);
          fft_f->Transform();
          fft_f->GetPointsComplex(rey,imy);
          for(Int_t is=0; is<daq.all_sample[5]; is++)
          {
            Double_t f=df*(is+0.5);
            rey[is]/=daq.all_sample[5];
            imy[is]/=daq.all_sample[5];
            mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
            pmod[1]->Fill(f,mod[is]/df);
          }
          if(ievt==0)printf("G10 noise : %.0f uV, %d good, G1 noise : %.0f uV\n",sqrt(rms[0])*1000.,ngood,sqrt(rms[1])*1000.);
          if(wait==2)
          {
            system("stty raw");
            cdum=getchar();
            system("stty -raw");
            if(cdum=='q')exit(-1);
          }
        }
        G1_noise[iRTIA][iLPF]=sqrt(G1_noise[iRTIA][iLPF]/daq.nevent)*1000.;
        G10_noise[iRTIA][iLPF]=sqrt(G10_noise[iRTIA][iLPF]/daq.nevent)*1000.;
        hmod[0][iRTIA*2+iLPF]->SetBinContent(1,0.);
        hmod[1][iRTIA*2+iLPF]->SetBinContent(1,0.);
        for(Int_t is=1; is<daq.all_sample[4]/2; is++)
        {
          Double_t mod=0.;
          mod=pmod[0]->GetBinContent(is+1);
          hmod[0][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
          mod=pmod[1]->GetBinContent(is+1);
          hmod[1][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
        }
        if(iLPF==0 && iRTIA==0)
        {
          sprintf(widget_name,"3_noise_R470_LPF50_value");
          if(daq.test_noise && (G10_noise[iRTIA][iLPF]<daq.noise_R470_LPF50_min || G10_noise[iRTIA][iLPF]>daq.noise_R470_LPF50_max))daq.noise_valid=FALSE;
        }
        else if(iLPF==0 && iRTIA==1)
        {
          sprintf(widget_name,"3_noise_R380_LPF50_value");
          if(daq.test_noise && (G10_noise[iRTIA][iLPF]<daq.noise_R380_LPF50_min || G10_noise[iRTIA][iLPF]>daq.noise_R380_LPF50_max))daq.noise_valid=FALSE;
        }
        else if(iLPF==0 && iRTIA==2)
        {
          sprintf(widget_name,"3_noise_R320_LPF50_value");
          if(daq.test_noise && (G10_noise[iRTIA][iLPF]<daq.noise_R320_LPF50_min || G10_noise[iRTIA][iLPF]>daq.noise_R320_LPF50_max))daq.noise_valid=FALSE;
        }
        else if(iLPF==1 && iRTIA==0)
        {
          sprintf(widget_name,"3_noise_R470_LPF35_value");
          if(daq.test_noise && (G10_noise[iRTIA][iLPF]<daq.noise_R470_LPF35_min || G10_noise[iRTIA][iLPF]>daq.noise_R470_LPF35_max))daq.noise_valid=FALSE;
        }
        else if(iLPF==1 && iRTIA==1)
        {
          sprintf(widget_name,"3_noise_R380_LPF35_value");
          if(daq.test_noise && (G10_noise[iRTIA][iLPF]<daq.noise_R380_LPF35_min || G10_noise[iRTIA][iLPF]>daq.noise_R380_LPF35_max))daq.noise_valid=FALSE;
        }
        else if(iLPF==1 && iRTIA==2)
        {
          sprintf(widget_name,"3_noise_R320_LPF35_value");
          if(daq.test_noise && (G10_noise[iRTIA][iLPF]<daq.noise_R320_LPF35_min || G10_noise[iRTIA][iLPF]>daq.noise_R320_LPF35_max))daq.noise_valid=FALSE;
        }
        sprintf(widget_text,"%.0f uV",G10_noise[iRTIA][iLPF]);
        if(!daq.cli)
        {
          widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          g_assert (widget);
          gtk_label_set_label((GtkLabel*)widget,widget_text);
        }
        else
        {
          printf("%s : %s\n",widget_name,widget_text);
        }
      }
    }
    daq.noise_G1_val=0.;
    for(int iRTIA=2; iRTIA>=0; iRTIA--)
    {
      for(int iLPF=0; iLPF<2; iLPF++)
      {
        daq.noise_G1_val+=G1_noise[iRTIA][iLPF]*G1_noise[iRTIA][iLPF];
      }
    }
    daq.noise_G1_val=sqrt(daq.noise_G1_val/6.);
    if(daq.noise_G1_val<daq.noise_G1_min || daq.noise_G1_val>daq.noise_G1_max)daq.noise_valid=FALSE;
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_value");
      g_assert (widget);
      sprintf(widget_text,"%.0f uV",daq.noise_G1_val);
      gtk_label_set_label((GtkLabel*)widget,widget_text);

      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_valid");
      g_assert (widget);
      if(daq.noise_valid)
      {
        gtk_widget_set_name(widget,"green_button");
        gtk_label_set_label((GtkLabel*)widget,"Passed");
      }
      else
      {
        gtk_widget_set_name(widget,"red_button");
        gtk_label_set_label((GtkLabel*)widget,"Failed");
        ret_code=-7;
      }
    }
    else
    {
      if(daq.noise_valid)
      {
        printf("Noise test : Passed\n");
      }
      else
      {
        printf("Noise test : Failed\n");
        ret_code=-7;
      }
    }
  }

  if(!daq.cli)
  {
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus
  }
  chip_is_valid &= daq.noise_valid;

  fft_f->~TVirtualFFT();
  free(rex);
  free(imx);
  free(rey);
  free(imy);
  free(mod);


//===========================================================================================================
// 3 : Send internal TP triggers for linearity measurements : G10 and G1
// calibration trigger setting :
  daq.trigger_type=1;
  daq.nevent=daq.n_TP_event;
  daq.nsample=100;
  daq.n_TP_step=60;
  daq.TP_step=64;
  daq.TP_level=0.;

  Double_t V_Iinj_slope[2], INL_min[2]={9999.,9999.}, INL_max[2]={-9999.,-9999.}, INL_min_fit[2]={9999.,9999.}, INL_max_fit[2]={-9999.,-9999.};
  TProfile *tp_inj1[2][daq.n_TP_step];
  TH1D     *hp_inj1[2][daq.n_TP_step];
  sprintf(output_file,"data/CATIA_%8.8d_%2.2d_calib.root",CATIA_number,CATIA_revision);
  TFile *fd=new TFile(output_file,"recreate");
  for(int iinj=0; iinj<daq.n_TP_step; iinj++)
  {
    sprintf(hname,"tpulse_G10_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    tp_inj1[0][iinj]=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    sprintf(hname,"tpulse_G1_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    tp_inj1[1][iinj]=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    sprintf(hname,"hpulse_G10_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    hp_inj1[0][iinj]=new TH1D(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    //hp_inj1[0][iinj]=new TH1D(hname,hname,daq.nsample*2,0.,dt*daq.nsample*4);
    sprintf(hname,"hpulse_G1_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    hp_inj1[1][iinj]=new TH1D(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    //hp_inj1[1][iinj]=new TH1D(hname,hname,daq.nsample*2,0.,dt*daq.nsample*4);
  }
  sprintf(hname,"pulse_G10_DAC_Rcal");
  TProfile *tp_inj_cal=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
  TGraph *tg_inj1[2], *tg_inj2[2], *tg_inj3[2];
  tg_inj1[0]=new TGraph();
  tg_inj1[0]->SetTitle("Inj_vs_iDAC_G10");
  tg_inj1[0]->SetName("Inj_vs_iDAC_G10");
  tg_inj1[0]->SetMarkerStyle(20);
  tg_inj1[0]->SetMarkerSize(1.0);
  tg_inj1[0]->SetMarkerColor(kRed);
  tg_inj1[0]->SetLineColor(kRed);
  tg_inj1[0]->SetLineWidth(2);
  tg_inj1[1]=new TGraph();
  tg_inj1[1]->SetTitle("Inj_vs_iDAC_G1");
  tg_inj1[1]->SetName("Inj_vs_iDAC_G1");
  tg_inj1[1]->SetMarkerStyle(20);
  tg_inj1[1]->SetMarkerSize(1.0);
  tg_inj1[1]->SetMarkerColor(kBlue);
  tg_inj1[1]->SetLineColor(kBlue);
  tg_inj1[1]->SetLineWidth(2);
  tg_inj2[0]=new TGraph();
  tg_inj2[0]->SetTitle("Inj_vs_vDAC_G10");
  tg_inj2[0]->SetName("Inj_vs_vDAC_G10");
  tg_inj2[0]->SetMarkerStyle(20);
  tg_inj2[0]->SetMarkerSize(1.0);
  tg_inj2[0]->SetMarkerColor(kRed);
  tg_inj2[0]->SetLineColor(kRed);
  tg_inj2[0]->SetLineWidth(2);
  tg_inj2[1]=new TGraph();
  tg_inj2[1]->SetTitle("Inj_vs_vDAC_G1");
  tg_inj2[1]->SetName("Inj_vs_vDAC_G1");
  tg_inj2[1]->SetMarkerStyle(20);
  tg_inj2[1]->SetMarkerSize(1.0);
  tg_inj2[1]->SetMarkerColor(kBlue);
  tg_inj2[1]->SetLineColor(kBlue);
  tg_inj2[1]->SetLineWidth(2);
  tg_inj3[0]=new TGraph();
  tg_inj3[0]->SetTitle("INL_vs_amplitude_G10");
  tg_inj3[0]->SetName("INL_vs_amplitude_G10");
  tg_inj3[0]->SetMarkerStyle(20);
  tg_inj3[0]->SetMarkerSize(1.0);
  tg_inj3[0]->SetMarkerColor(kRed);
  tg_inj3[0]->SetLineColor(kRed);
  tg_inj3[0]->SetLineWidth(2);
  tg_inj3[0]->SetMaximum(0.003);
  tg_inj3[0]->SetMinimum(-.003);
  tg_inj3[1]=new TGraph();
  tg_inj3[1]->SetTitle("INL_vs_amplitude_G1");
  tg_inj3[1]->SetName("INL_vs_amplitude_G1");
  tg_inj3[1]->SetMarkerStyle(20);
  tg_inj3[1]->SetMarkerSize(1.0);
  tg_inj3[1]->SetMarkerColor(kBlue);
  tg_inj3[1]->SetLineColor(kBlue);
  tg_inj3[1]->SetLineWidth(2);
  tg_inj3[1]->SetMaximum(0.003);
  tg_inj3[1]->SetMinimum(-.003);

  if(chip_is_valid && daq.test_linearity)
  {
    asic.CATIA_ped_G10[ich]=ped_ref[2][0];
    asic.CATIA_ped_G1[ich] =ped_ref[2][1];
    asic.CATIA_LPF35[ich]=1;
    asic.CATIA_gain[ich] =3;
    iret=update_CATIA_reg(ich,3);
    usleep(100000);

// First round to compute the correction to apply to G10 TP due to an un-understood bug in CATIA-V2.0
    Double_t y_low=-9999., y_up=9999., ped_cal=0.;
    daq.TP_level=450;
    asic.CATIA_DAC1_status[ich]=TRUE;
    asic.CATIA_DAC1_val[ich]=daq.TP_level;
    asic.CATIA_Rconv[ich]=0;
    iret=update_CATIA_reg(ich,4);
    iret=update_CATIA_reg(ich,6);

    if(wait==1)
    {
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);
    }

    asic.CATIA_DAC1_status[ich]=TRUE;

    while(y_low<y_up-0.5)
    {
      daq.TP_level++;
      asic.CATIA_DAC1_val[ich]=daq.TP_level;
      iret=update_CATIA_reg(ich,4);
      usleep(100);

      tp_inj_cal->Reset();
      for(Int_t ievt=0; ievt<10; ievt++)
      {
        draw=0;
        get_event(1,draw);
        if(daq.error>0) printf("DAQ error on event %d : %d\n",ievt, daq.error);
        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t y=daq.fevent[4][is]*dv;
          if(y<1.e-6)y=1.e-6;
          tp_inj_cal->Fill(dt*(is+0.5),y);
        }
      }
      tfr1=tp_inj_cal->Fit("pol0","wq",fit_opt,1475.e-9,1500.e-9);
      f1=tp_inj_cal->GetFunction("pol0");
      y_low=0.;
      if(f1!=NULL){y_low=f1->GetParameter(0); f1->~TF1();}
      tfr1.~TFitResultPtr();
      tfr1=tp_inj_cal->Fit("pol0","wq",fit_opt,1600.e-9,1625.e-9);
      f1=tp_inj_cal->GetFunction("pol0");
      y_up=0.;
      if(f1!=NULL){y_up=f1->GetParameter(0); f1->~TF1();}
      tfr1.~TFitResultPtr();
      tfr1=tp_inj_cal->Fit("pol0","wq",fit_opt,500.e-9,750.e-9);
      f1=tp_inj_cal->GetFunction("pol0");
      ped_cal=0.;
      if(f1!=NULL){ped_cal=f1->GetParameter(0); f1->~TF1();}
      tfr1.~TFitResultPtr();
    }
    printf("level %d : low %.2f, up %.2f\n",daq.TP_level,y_low,y_up);
    Int_t TP_base=daq.TP_level-10;

    for(int igain=0; igain<2; igain++)
    {
      daq.TP_level=TP_base;
      daq.TP_level=0;
      //asic.CATIA_Rconv[ich]=1-igain;
      asic.CATIA_Rconv[ich]=igain; // Bug corrected 2023/06/29, M.D.
      iret=update_CATIA_reg(ich,6);

      Double_t fit_min=-1, fit_max=-1;
      Int_t loc_step=0;
      for(int istep=0; istep<daq.n_TP_step; istep++)
      {
  // Program DAC for this step
        asic.CATIA_DAC1_status[ich]=TRUE;
        asic.CATIA_DAC1_val[ich]=daq.TP_level;
        iret=update_CATIA_reg(ich,4);

// Wait for Vdac to stabilize :
        if(istep==0)
          usleep(10000);
        else
          usleep(100);

        if(daq.debug_draw)draw=(istep%6);
        for(Int_t ievt=0; ievt<daq.nevent; ievt++)
        {
          get_event(1,draw);
          draw=0;
          if(daq.error>0) printf("DAQ error on event %d : %d\n",ievt, daq.error);

          for(int is=0; is<daq.all_sample[4]; is++)
          {
            Double_t y=daq.fevent[4+igain][is]*dv;
            if(y<1.e-6)y=1.e-6;
            tp_inj1[igain][istep]->Fill(dt*(is+0.5),y);
          }
          //for(int is=0; is<daq.all_sample[1]; is++)
          //{
          //  Double_t y=daq.fevent[1+igain*2][is]*dv;
          //  if(y<1.e-6)y=1.e-6;
          //  tp_inj1[igain][istep]->Fill(2.*dt*(is+0.5),y);
          //}
        }
        for(int is=0; is<daq.all_sample[4]; is++)
        //for(int is=0; is<daq.all_sample[1]; is++)
        {
          Double_t y_raw, ey_raw, cal_cor;
          y_raw=tp_inj1[igain][istep]->GetBinContent(is+1);
          ey_raw=tp_inj1[igain][istep]->GetBinError(is+1);
          //cal_cor=tp_inj_cal->GetBinContent(is+1);
          //if(CATIA_number>=20000 && igain==0)
          //  hp_inj1[igain][istep]->SetBinContent(is+1,y_raw-cal_cor+ped_cal);
          //else
            hp_inj1[igain][istep]->SetBinContent(is+1,y_raw);
          hp_inj1[igain][istep]->SetBinError(is+1,ey_raw);
        }

        if(wait==1)
        {
          printf("step %d/%d\n",istep,daq.n_TP_step);
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q')exit(-1);
        }

        tfr1= hp_inj1[igain][istep]->Fit("pol0","wq",fit_opt,300.e-9,700.e-9);
        f1=hp_inj1[igain][istep]->GetFunction("pol0");
        mean[igain]=0.;
        if(f1!=NULL){ mean[igain]=f1->GetParameter(0); f1->~TF1();}
        tfr1.~TFitResultPtr();
        if(daq.TP_level==512)printf("TP_ped : %.2f\n",mean[igain]);
        //hp_inj1[igain][istep]->Fit("pol1","wq",fit_opt,9000.e-9,1600.e-9);
        //f1=hp_inj1[igain][istep]->GetFunction("pol1");
        //Double_t val0=0.;
        //if(f1!=NULL){val0=f1->GetParameter(0)+f1->GetParameter(1)*800.e-9-mean[igain]; f1->~TF1();}
        tfr1=hp_inj1[igain][istep]->Fit("pol0","wq",fit_opt,900.e-9,1600.e-9);
        f1=hp_inj1[igain][istep]->GetFunction("pol0");
        Double_t val0=0.;
        //if(f1!=NULL){val0=f1->GetParameter(0)+f1->GetParameter(1)*800.e-9-mean[igain];f1->~TF1();}
        if(f1!=NULL)
        {
          val0=f1->GetParameter(0)-mean[igain];
          if(daq.TP_level==512)printf("TP val : %.2f %.2f\n",f1->GetParameter(0),val0);
          f1->~TF1();
        }
        tfr1.~TFitResultPtr();

        
        if(val0<-2.){daq.TP_level+=daq.TP_step; continue;}
        if(val0>1100. || (igain==1 && istep>75))
        {
          //if(igain==1)cdebug->Write();
          break;
        }

        tg_inj1[igain]->SetPoint(loc_step,daq.TP_level,val0);
        if(fDAC[0]!=NULL)
          tg_inj2[igain]->SetPoint(loc_step,fDAC[0]->Eval(daq.TP_level)*1000.,val0);
        else
          tg_inj2[igain]->SetPoint(loc_step,daq.TP_level,val0);
        if(fit_min<0 && val0>0.)fit_min=daq.TP_level-daq.TP_step/2.;
        if(val0<1190.)fit_max=daq.TP_level+daq.TP_step/2.;
        loc_step++;
        daq.TP_level+=daq.TP_step;
      }
      tfr1=tg_inj1[igain]->Fit("pol1","WQ",fit_opt,fit_min,fit_max);
      V_Iinj_slope[igain]=0.;
      if(fDAC[0]!=NULL)
      {
        tfr2=tg_inj2[igain]->Fit("pol1","WQ",fit_opt,fDAC[0]->Eval(fit_min)*1000.,fDAC[0]->Eval(fit_max)*1000.);
        f1=tg_inj2[igain]->GetFunction("pol1");
        if(f1!=NULL) { V_Iinj_slope[igain]=f1->GetParameter(1); f1->~TF1(); }
        tfr2.~TFitResultPtr();
      }
      printf("Gain %.0f : V_Iinj slope = %.3f\n",val_gain[igain],V_Iinj_slope[igain]);
      f1=tg_inj1[igain]->GetFunction("pol1");
  // Compute residuals :
      Int_t n=tg_inj1[igain]->GetN();
      for(Int_t is=0; is<n; is++)
      {
        Double_t x,y;
        tg_inj1[igain]->GetPoint(is,x,y);
        if(f1!=NULL)
        {
          tg_inj3[igain]->SetPoint(is,y,(y-f1->Eval(x))/1200.);
          if(y>0. && y<1200. && (y-f1->Eval(x))/1200.>INL_max[igain])INL_max[igain]=(y-f1->Eval(x))/1200.;
          if(y>0. && y<1200. && (y-f1->Eval(x))/1200.<INL_min[igain])INL_min[igain]=(y-f1->Eval(x))/1200.;
        }
      }
      if(f1!=NULL) f1->~TF1();
      tfr1.~TFitResultPtr();
      INL_min[igain]*=1000.;
      INL_max[igain]*=1000.;
      printf("Gain %.0f INL min %.2f max %.2f\n",val_gain[igain],INL_min[igain], INL_max[igain]);
  // Perform a pol2 fit on residual curve to reduce statistical fluctuations
      f1=NULL;
      if(igain==1)
      {
        tfr1=tg_inj3[igain]->Fit("pol3","WQ","",100.,1100.);
        f1=tg_inj3[igain]->GetFunction("pol3");
      }
      else
      {
        tfr1=tg_inj3[igain]->Fit("pol2","WQ","",5.,1100.);
        f1=tg_inj3[igain]->GetFunction("pol2");
      }
      if(f1!=NULL)
      {
        INL_max_fit[igain]=f1->GetMaximum(50.,1150.)*1000.;
        INL_min_fit[igain]=f1->GetMinimum(50.,1150.)*1000.;
        f1->~TF1();
      }
      tfr1.~TFitResultPtr();
      printf("Gain %.0f INL min fit %.2f max fit %.2f\n",val_gain[igain],INL_min_fit[igain], INL_max_fit[igain]);

      if(igain==0)
      {
        sprintf(widget_name,"3_linearity_G10_value");
        if(daq.test_linearity && (INL_max[igain]-INL_min[igain])/2.>daq.linearity_G10_max)daq.linearity_valid=FALSE;
      }
      else
      {
        sprintf(widget_name,"3_linearity_G1_value");
        if(daq.test_linearity && (INL_max[igain]-INL_min[igain])/2.>daq.linearity_G1_max)daq.linearity_valid=FALSE;
      }
      sprintf(widget_text,"%.1f/1000",(INL_max[igain]-INL_min[igain])/2.);
      if(!daq.cli)
      {
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_label_set_label((GtkLabel*)widget,widget_text);
      }
      else
      {
        printf("%s : %s\n",widget_name,widget_text);
      }
    }
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_valid");
      g_assert (widget);
      if(daq.linearity_valid)
      {
        gtk_widget_set_name(widget,"green_button");
        gtk_label_set_label((GtkLabel*)widget,"Passed");
      }
      else
      {
        gtk_widget_set_name(widget,"red_button");
        gtk_label_set_label((GtkLabel*)widget,"Failed");
        ret_code=-8;
      }
    }
    else
    {
      if(daq.linearity_valid)
      {
        printf("Linearity test : Passed\n");
      }
      else
      {
        printf("Linearity test : Failed\n");
        ret_code=-8;
      }
    }
  }
  if(fDAC[0]!=NULL)fDAC[0]->~TF1();
  if(fDAC[1]!=NULL)fDAC[1]->~TF1();
  tfr[0].~TFitResultPtr();
  tfr[1].~TFitResultPtr();

  if(!daq.cli)
  {
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus
  }
  chip_is_valid &= daq.linearity_valid;

//===========================================================================================================
// 4 : Send G10 and G1 calibration pulses with R=470 and R=380
// 4.1 : Using FPGA LVCMOS pulse :
// Enabe CALIB_PULSE  :
  TGraph *tg_calib[2];
  tg_calib[0]=new TGraph();
  tg_calib[0]->SetLineColor(kRed);
  tg_calib[0]->SetMarkerColor(kRed);
  tg_calib[0]->SetMarkerSize(0.1);
  tg_calib[1]=new TGraph();
  tg_calib[1]->SetLineColor(kRed);
  tg_calib[1]->SetMarkerColor(kRed);
  tg_calib[1]->SetMarkerSize(0.1);
  if(asic.CATIA_version[ich]<14)
  {
    tg_calib[0]->SetTitle("G10_R400_LPF35_calib_pulse");
    tg_calib[0]->SetName("G10_R400_LPF35_calib_pulse");
    tg_calib[1]->SetTitle("G1_R400_LPF35_calib_pulse");
    tg_calib[1]->SetName("G1_R400_LPF35_calib_pulse");
  }
  else if(asic.CATIA_version[ich]<21)
  {
    tg_calib[0]->SetTitle("G10_R340_LPF35_calib_pulse");
    tg_calib[0]->SetName("G10_R340_LPF35_calib_pulse");
    tg_calib[1]->SetTitle("G1_R340_LPF35_calib_pulse");
    tg_calib[1]->SetName("G1_R340_LPF35_calib_pulse");
  }
  else
  {
    tg_calib[0]->SetTitle("G10_R320_LPF35_calib_pulse");
    tg_calib[0]->SetName("G10_R320_LPF35_calib_pulse");
    tg_calib[1]->SetTitle("G1_R320_LPF35_calib_pulse");
    tg_calib[1]->SetName("G1_R320_LPF35_calib_pulse");
  }
  Double_t Rcalib[2]={10000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv[2];
  Double_t CMOS_amp=1.20;
  Double_t CATIA_gain[2][3], TIA_gain[3], gain_stage;
// Enable G10 and G1 calibration pulse from FPGA :

  daq.calib_pulse_enabled    =1;
  daq.calib_mux_enabled      =0;
  daq.clock_select           =0;
  daq.I2C_low                =0;
  daq.self_trigger_mode      =0;
  daq.self_trigger_mask      =0;
  daq.self_trigger_threshold =0;
  daq.self_trigger           =0;
  daq.self_trigger_loop      =1;
  daq.fifo_mode              =1;
  update_trigger();

  //fead_ctrl=  CALIB_PULSE_ENABLED   *1 |
  //            SELF_TRIGGER_MASK     *0 |
  //            SELF_TRIGGER_THRESHOLD*0 |
  //            SELF_TRIGGER          *0 |
  //            SELF_TRIGGER_LOOP     *1 |
  //            FIFO_MODE             *1 | // Always DAQ on trigger
  //            RESET                 *0;
  //hw.getNode("FEAD_CTRL").write(fead_ctrl);
  //hw.dispatch();
  //reg=hw.getNode("FEAD_CTRL").read();
  //hw.dispatch();
  //printf("Calib_pulse_enable : 0x%8.8x -> %d\n",reg.value(),(reg.value()>>27)&1);

  daq.trigger_type=3;
  daq.nevent=100;
  daq.nsample=100;
  daq.TP_delay=0;
  daq.TP_width=100;
  command=((daq.TP_delay&0xffff)<<16) | (daq.TP_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width,daq.TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);

  asic.CATIA_Rconv[ich]=0;
  asic.CATIA_DAC1_status[ich]=FALSE;
  asic.CATIA_DAC1_val[ich]   =0;
  asic.CATIA_DAC2_status[ich]=FALSE;
  asic.CATIA_DAC2_val[ich]   =0;
  daq.CATIA_Vcal_out         =FALSE;
  iret=update_CATIA_reg(ich,4);
  iret=update_CATIA_reg(ich,5);
  iret=update_CATIA_reg(ich,6);

  TProfile *tp_calib[2][3];
  tmax=daq.nsample*4*dt;
  tp_calib[0][0]=new TProfile("G10_R470_calib_pulse_response","G10_R470_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[0][1]=new TProfile("G10_R380_calib_pulse_response","G10_R380_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[0][2]=new TProfile("G10_R320_calib_pulse_response","G10_R320_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[1][0]=new TProfile("G1_R470_calib_pulse_response","G1_R470_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[1][1]=new TProfile("G1_R380_calib_pulse_response","G1_R380_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[1][2]=new TProfile("G1_R320_calib_pulse_response","G1_R320_calib_pulse_response",daq.nsample*4,0.,tmax);
  TF1 *fcalib=new TF1("fcalib","[0]+[1]*(1.+tanh((x-[2])/[3]))/2.",0.,2.e3);
  fcalib->SetParameter(0,0.);
  fcalib->SetParameter(1,2000.);
  fcalib->SetParameter(2,0.90e-6);
  fcalib->SetParameter(3,9.e-9);
  TH1D *hcalib_t0=new TH1D("t0_calib","t0_calib",1000,0.80e-6,1.00e-6);
  TH1D *hcalib_rt=new TH1D("RT_calib","RT_calib",1000,7.e-9,12.e-9);

  gain_stage=10.;
  TIA_gain[0]=470; // G1-R470
  TIA_gain[1]=380; // G1-R380
  TIA_gain[2]=320; // G1-R320
  Rconv[0]=2471; // Iinj conversion resistor for G1
  Rconv[1]=272;  // Iinj conversion resistor for G10

  if(chip_is_valid && daq.test_gain_internal)
  {
    for(int iRTIA=2; iRTIA>=0; iRTIA--)
    {
      Int_t jRTIA=iRTIA;
      if(iRTIA==2)jRTIA=3;
      asic.CATIA_gain[ich]=jRTIA;
      asic.CATIA_LPF35[ich]=1;
      iret=update_CATIA_reg(ich,3);
      usleep(10000);

      if(daq.debug_draw)draw=1;
      for(int ievt=0; ievt<daq.nevent; ievt++)
      {
        get_event(1,draw);
        draw=0;
        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t t=dt*(is+0.5);
          tp_calib[0][iRTIA]->Fill(t,daq.fevent[4][is]*(dv/1000.));
          tp_calib[1][iRTIA]->Fill(t,daq.fevent[5][is]*(dv/1000.));
          if(iRTIA==2)tg_calib[0]->SetPoint(is,dt*is,daq.fevent[4][is]);

        }
        if(iRTIA==2)
        {
          tfr1=tg_calib[0]->Fit(fcalib,"WQ",fit_opt,0.8e-6,1.00e-6);
          hcalib_t0->Fill(fcalib->GetParameter(2));
          hcalib_rt->Fill(fcalib->GetParameter(3));
          tfr1.~TFitResultPtr();
        }
      }
      tfr1=tp_calib[0][iRTIA]->Fit("pol0","WQ",fit_opt,0.,.8e-6);
      f1=tp_calib[0][iRTIA]->GetFunction("pol0");
      mean[0]=0.;
      if(f1!=NULL){mean[0]=f1->GetParameter(0); f1->~TF1();}
      tfr1.~TFitResultPtr();
      tfr1=tp_calib[0][iRTIA]->Fit("pol1","WQ",fit_opt,1.1e-6,1.4e-6);
      f1=tp_calib[0][iRTIA]->GetFunction("pol1");
      CATIA_gain[0][iRTIA]=-999.;
      if(f1!=NULL)
      {
        CATIA_gain[0][iRTIA]=(f1->Eval(0.9e-6)-mean[0])/(CMOS_amp/Rcalib[0]);
        //CATIA_gain[0][iRTIA]=(f1->Eval(0.35e-6)-mean[0])/(CMOS_amp/Rcalib[0]);
        printf("AWG : ped  %.0f, top %.0f\n",mean[0],f1->Eval(0.9e-6));
        f1->~TF1();
      }
      tfr1.~TFitResultPtr();
      printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[0],RTIA[iRTIA],CATIA_gain[0][iRTIA]);

      tfr1=tp_calib[1][iRTIA]->Fit("pol0","WQ",fit_opt,0.,.8e-6);
      f1=tp_calib[1][iRTIA]->GetFunction("pol0");
      mean[1]=0.;
      if(f1!=NULL){mean[1]=f1->GetParameter(0);f1->~TF1();}
      tfr1.~TFitResultPtr();
      tfr1=tp_calib[1][iRTIA]->Fit("pol1","WQ",fit_opt,1.1e-6,1.5e-6);
      f1=tp_calib[1][iRTIA]->GetFunction("pol1");
      CATIA_gain[1][iRTIA]=-999.;
      if(f1!=NULL)
      {
        CATIA_gain[1][iRTIA]=(f1->Eval(0.9e-6)-mean[1])/(CMOS_amp/Rcalib[1]);
        //CATIA_gain[1][iRTIA]=(f1->Eval(0.35e-6)-mean[1])/(CMOS_amp/Rcalib[1]);
        printf("AWG : ped  %.0f, top %.0f\n",mean[1],f1->Eval(0.9e-6));
        f1->~TF1();
      }
      tfr1.~TFitResultPtr();
      printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[1],RTIA[iRTIA],CATIA_gain[1][iRTIA]);
    }
    // (G10-R470/G1-R470 + G10-R380/G1-R380 +G10-R320/G1-R320)/3
    gain_stage=(CATIA_gain[0][0]/CATIA_gain[1][0]+CATIA_gain[0][1]/CATIA_gain[1][1]+CATIA_gain[0][2]/CATIA_gain[1][2])/3.;
    TIA_gain[0]=CATIA_gain[1][0]; // G1-R470
    TIA_gain[1]=CATIA_gain[1][1]; // G1-R380
    TIA_gain[2]=CATIA_gain[1][2]; // G1-R320
    printf("G10 with R470 : %.2f, with R380 : %.2f, with R320 : %2f, mean : %.2f\n",
           CATIA_gain[0][0]/CATIA_gain[1][0],CATIA_gain[0][1]/CATIA_gain[1][1],CATIA_gain[0][2]/CATIA_gain[1][2],gain_stage);
    printf("R470 = %.0f Ohm\n",TIA_gain[0]);
    printf("R380 = %.0f Ohm\n",TIA_gain[1]);
    printf("R320 = %.0f Ohm\n",TIA_gain[2]);
    Rconv[0]=CATIA_gain[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
    Rconv[1]=CATIA_gain[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
    printf("Rconv Iinj G10 (2471 Ohm) = %.4f Ohm\n",Rconv[0]);
    printf("Rconv Iinj G1  (272 Ohm)  = %.4f Ohm\n",Rconv[1]);
    if(gain_stage<daq.gain_int_G10_min || gain_stage>daq.gain_int_G10_max)daq.gain_internal_valid=FALSE;
    if(TIA_gain[0]<daq.gain_int_R470_min || TIA_gain[0]>daq.gain_int_R470_max)daq.gain_internal_valid=FALSE;
    if(TIA_gain[1]<daq.gain_int_R380_min || TIA_gain[1]>daq.gain_int_R380_max)daq.gain_internal_valid=FALSE;
    if(TIA_gain[2]<daq.gain_int_R320_min || TIA_gain[2]>daq.gain_int_R320_max)daq.gain_internal_valid=FALSE;
    if(Rconv[0]<daq.gain_int_R2471_min || Rconv[0]>daq.gain_int_R2471_max)daq.gain_internal_valid=FALSE;
    if(Rconv[1]<daq.gain_int_R272_min || Rconv[1]>daq.gain_int_R272_max)daq.gain_internal_valid=FALSE;
    sprintf(widget_name,"3_gain_internal_G10_value");
    sprintf(widget_text,"%.2f",gain_stage);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_internal_R470_value");
    sprintf(widget_text,"%.0f",TIA_gain[0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_internal_R380_value");
    sprintf(widget_text,"%.0f",TIA_gain[1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_internal_R320_value");
    sprintf(widget_text,"%.0f",TIA_gain[2]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_internal_R2471_value");
    sprintf(widget_text,"%.0f",Rconv[0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_internal_R272_value");
    sprintf(widget_text,"%.0f",Rconv[1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }

    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_valid");
      g_assert (widget);
      if(daq.gain_internal_valid)
      {
        gtk_widget_set_name(widget,"green_button");
        gtk_label_set_label((GtkLabel*)widget,"Passed");
      }
      else
      {
        gtk_widget_set_name(widget,"red_button");
        gtk_label_set_label((GtkLabel*)widget,"Failed");
        ret_code=-9;
      }
    }
    else
    {
      if(daq.gain_internal_valid)
      {
        printf("Gain test (internal) : Passed\n");
      }
      else
      {
        printf("Gain test (internal) : Failed\n");
        ret_code=-9;
      }
    }
  }

  if(!daq.cli)
  {
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus
  }
  chip_is_valid &= daq.gain_internal_valid;

// 4.2 : Using AWG pulse :
  TGraph *tg_calib_AWG[2];
  tg_calib_AWG[0]=new TGraph();
  tg_calib_AWG[0]->SetLineColor(kRed);
  tg_calib_AWG[0]->SetMarkerColor(kRed);
  tg_calib_AWG[0]->SetMarkerSize(0.1);
  tg_calib_AWG[0]->SetTitle("G10_R320_LPF35_AWG_calib_pulse");
  tg_calib_AWG[0]->SetName("G10_R320_LPF35_AWG_calib_pulse");
  tg_calib_AWG[1]=new TGraph();
  tg_calib_AWG[1]->SetLineColor(kRed);
  tg_calib_AWG[1]->SetMarkerColor(kRed);
  tg_calib_AWG[1]->SetMarkerSize(0.1);
  tg_calib_AWG[1]->SetTitle("G1_R320_LPF35_AWG_calib_pulse");
  tg_calib_AWG[1]->SetName("G1_R320_LPF35_AWG_calib_pulse");
  Double_t Rcalib_AWG[2]={10000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv_AWG[2];
  Double_t CATIA_gain_AWG[2][3], TIA_gain_AWG[3], gain_stage_AWG;
  TProfile *tp_calib_AWG[2][3];
  tp_calib_AWG[0][0]=new TProfile("G10_R470_AWG_calib_pulse_response","G10_R470_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[0][1]=new TProfile("G10_R380_AWG_calib_pulse_response","G10_R380_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[0][2]=new TProfile("G10_R320_AWG_calib_pulse_response","G10_R320_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[1][0]=new TProfile("G1_R470_AWG_calib_pulse_response","G1_R470_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[1][1]=new TProfile("G1_R380_AWG_calib_pulse_response","G1_R380_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[1][2]=new TProfile("G1_R320_AWG_calib_pulse_response","G1_R320_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  TH1D *hcalib_AWG_t0=new TH1D("t0_AWG_calib","t0_AWG_calib",1000,0.80e-6,1.00e-6);
  TH1D *hcalib_AWG_rt=new TH1D("RT_AWG_calib","RT_AWG_calib",1000,7.e-9,12.e-9);

  gain_stage_AWG=10.;
  TIA_gain_AWG[0]=470; // G1-R470
  TIA_gain_AWG[1]=380; // G1-R380
  TIA_gain_AWG[2]=320; // G1-R320
  Rconv_AWG[0]=2471;   // Iinj conversion resistor for G1
  Rconv_AWG[1]=272;    // Iinj conversion resistor for G10

  if(chip_is_valid && daq.test_gain_AWG)
  {
// Disaable G10 and G1 calibration pulse from FPGA :

    daq.calib_pulse_enabled    =0;
    daq.calib_mux_enabled      =0;
    daq.clock_select           =0;
    daq.I2C_low                =0;
    daq.self_trigger_mode      =0;
    daq.self_trigger_mask      =0xf;
    daq.self_trigger_threshold =1000;
    daq.self_trigger           =1;
    daq.self_trigger_loop      =1;
    daq.fifo_mode              =1;
    update_trigger();

    //fead_ctrl=  CALIB_PULSE_ENABLED   *0 |
    //            SELF_TRIGGER_MASK     *0xf |
    //            SELF_TRIGGER_THRESHOLD*1000 |
    //            SELF_TRIGGER          *1 |
    //            SELF_TRIGGER_LOOP     *1 |
    //            FIFO_MODE             *1 | // Always DAQ on trigger
    //            SELF_TRIGGER_MODE     *0 | // trig on absolute level
    //            RESET                 *0;
    //hw.getNode("FEAD_CTRL").write(fead_ctrl);
    //hw.dispatch();
    //reg=hw.getNode("FEAD_CTRL").read();
    //hw.dispatch();
    //printf("Calib_pulse_enable : 0x%8.8x -> %d\n",reg.value(),(reg.value()>>27)&1);

    daq.trigger_type=2;
    daq.nevent=100;
    daq.nsample=100;

    printf("Please switch ON the pulse generator with 1.2V pulse, 500 ns width, 600 ns delay\n");
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);

    fcalib->SetParameter(0,0.);
    fcalib->SetParameter(1,2000.);
    fcalib->SetParameter(2,0.35e-6);
    fcalib->SetParameter(3,9.e-9);

    for(int iRTIA=2; iRTIA>=0; iRTIA--)
    {
      Int_t jRTIA=iRTIA;
      if(iRTIA==2)jRTIA=3;
      asic.CATIA_gain[ich] =jRTIA;
      asic.CATIA_LPF35[ich]=1;
      iret=update_CATIA_reg(ich,3);
      usleep(10000);

      if(daq.debug_draw)draw=1;
      for(int ievt=0; ievt<daq.nevent; ievt++)
      {
        get_event(1,draw);
        draw=0;
        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t t=dt*(is+0.5);
          tp_calib_AWG[0][iRTIA]->Fill(t,daq.fevent[4][is]*(dv/1000.));
          tp_calib_AWG[1][iRTIA]->Fill(t,daq.fevent[5][is]*(dv/1000.));
          if(iRTIA==2)tg_calib_AWG[0]->SetPoint(is,dt*is,daq.fevent[4][is]);
        }
        if(iRTIA==2)
        {
          tfr1=tg_calib_AWG[0]->Fit(fcalib,"WQ",fit_opt,0.2e-6,0.4e-6);
          hcalib_AWG_t0->Fill(fcalib->GetParameter(2));
          hcalib_AWG_rt->Fill(fcalib->GetParameter(3));
          tfr1.~TFitResultPtr();
        }
      }
      tfr1=tp_calib_AWG[0][iRTIA]->Fit("pol0","WQ",fit_opt,0.,.2e-6);
      f1=tp_calib_AWG[0][iRTIA]->GetFunction("pol0");
      mean[0]=0.;
      if(f1!=NULL){mean[0]=f1->GetParameter(0); f1->~TF1();}
      tfr1.~TFitResultPtr();
      tfr1=tp_calib_AWG[0][iRTIA]->Fit("pol1","WQ",fit_opt,0.4e-6,0.9e-6);
      f1=tp_calib_AWG[0][iRTIA]->GetFunction("pol1");
      CATIA_gain_AWG[0][iRTIA]=-999.;
      if(f1!=NULL){CATIA_gain_AWG[0][iRTIA]=(f1->Eval(0.3e-6)-mean[0])/(1.2/Rcalib_AWG[0]); f1-> ~TF1();}
      tfr1.~TFitResultPtr();
      printf("AWG : ped  %.0f, top %.0f\n",mean[0],f1->Eval(0.9e-6));
      printf("AWG : Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[0],RTIA[iRTIA],CATIA_gain_AWG[0][iRTIA]);

      tfr1=tp_calib_AWG[1][iRTIA]->Fit("pol0","WQ",fit_opt,0.,.2e-6);
      f1=tp_calib_AWG[1][iRTIA]->GetFunction("pol0");
      mean[1]=0.;
      if(f1!=NULL){mean[1]=f1->GetParameter(0); f1->~TF1();}
      tfr1.~TFitResultPtr();
      tfr1=tp_calib_AWG[1][iRTIA]->Fit("pol1","WQ",fit_opt,0.4e-6,0.9e-6);
      f1=tp_calib_AWG[1][iRTIA]->GetFunction("pol1");
      CATIA_gain_AWG[1][iRTIA]=-999.;
      if(f1!=NULL)
      {
        CATIA_gain_AWG[1][iRTIA]=(f1->Eval(0.3e-6)-mean[1])/(1.2/Rcalib_AWG[1]);
        printf("AWG : ped  %.0f, top %.0f\n",mean[1],f1->Eval(0.9e-6));
        f1->~TF1();
      }
      tfr1.~TFitResultPtr();
      printf("AWG : Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[1],RTIA[iRTIA],CATIA_gain_AWG[1][iRTIA]);

    }
    gain_stage_AWG=(CATIA_gain_AWG[0][0]/CATIA_gain_AWG[1][0]+CATIA_gain_AWG[0][1]/CATIA_gain_AWG[1][1]+CATIA_gain_AWG[0][2]/CATIA_gain_AWG[1][2])/3.;
    TIA_gain_AWG[0]=CATIA_gain_AWG[1][0]; // G1-R470
    TIA_gain_AWG[1]=CATIA_gain_AWG[1][1]; // G1-R380
    TIA_gain_AWG[2]=CATIA_gain_AWG[1][2]; // G1-R320
    printf("G10 with R470 : %.2f, with R380 : %.2f, with R320 : %2f, mean : %.2f\n",
           CATIA_gain_AWG[0][0]/CATIA_gain_AWG[1][0],CATIA_gain_AWG[0][1]/CATIA_gain_AWG[1][1],CATIA_gain_AWG[0][2]/CATIA_gain_AWG[1][2],gain_stage_AWG);
    printf("R470 = %.0f Ohm\n",TIA_gain_AWG[0]);
    printf("R380 = %.0f Ohm\n",TIA_gain_AWG[1]);
    printf("R320 = %.0f Ohm\n",TIA_gain_AWG[2]);
    Rconv_AWG[0]=CATIA_gain_AWG[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
    Rconv_AWG[1]=CATIA_gain_AWG[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
    printf("Rconv Iinj G10 (2471 Ohm) = %.0f Ohm\n",Rconv_AWG[0]);
    printf("Rconv Iinj G1  (272 Ohm)  = %.0f Ohm\n",Rconv_AWG[1]);

    if(TIA_gain_AWG[0]<daq.gain_AWG_R470_min || TIA_gain_AWG[0]>daq.gain_AWG_R470_max)daq.gain_AWG_valid=FALSE;
    if(TIA_gain_AWG[1]<daq.gain_AWG_R380_min || TIA_gain_AWG[1]>daq.gain_AWG_R380_max)daq.gain_AWG_valid=FALSE;
    if(TIA_gain_AWG[2]<daq.gain_AWG_R320_min || TIA_gain_AWG[2]>daq.gain_AWG_R320_max)daq.gain_AWG_valid=FALSE;
    if(Rconv_AWG[1]<daq.gain_AWG_R2471_min || Rconv_AWG[1]>daq.gain_AWG_R2471_max)daq.gain_AWG_valid=FALSE;
    if(Rconv_AWG[0]<daq.gain_AWG_R272_min || Rconv_AWG[0]>daq.gain_AWG_R272_max)daq.gain_AWG_valid=FALSE;
    sprintf(widget_name,"3_gain_AWG_R470_value");
    sprintf(widget_text,"%.0f",TIA_gain_AWG[0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_AWG_R380_value");
    sprintf(widget_text,"%.0f",TIA_gain_AWG[1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_AWG_R320_value");
    sprintf(widget_text,"%.0f",TIA_gain_AWG[2]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_AWG_R2471_value");
    sprintf(widget_text,"%.0f",Rconv_AWG[1]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    sprintf(widget_name,"3_gain_AWG_RR272_value");
    sprintf(widget_text,"%.0f",Rconv_AWG[0]);
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
    else
    {
      printf("%s : %s\n",widget_name,widget_text);
    }
    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_valid");
      g_assert (widget);
      if(daq.gain_AWG_valid)
      {
        gtk_widget_set_name(widget,"green_button");
        gtk_label_set_label((GtkLabel*)widget,"Passed");
      }
      else
      {
        gtk_widget_set_name(widget,"red_button");
        gtk_label_set_label((GtkLabel*)widget,"Failed");
        ret_code=-10;
      }
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus
    }
    else
    {
      if(daq.gain_AWG_valid)
      {
        printf("Gain test (AWG) : Passed\n");
      }
      else
      {
        printf("Gain test (AWG) : Failed\n");
        ret_code=-10;
      }
    }
  }
  chip_is_valid &= daq.gain_AWG_valid;

  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_CATIA_valid");
    g_assert (widget);
    if(chip_is_valid)
    {
      gtk_widget_set_name(widget,"green_button");
      gtk_label_set_label((GtkLabel*)widget,"Passed");
    }
    else
    {
      gtk_widget_set_name(widget,"red_button");
      sprintf(widget_text,"Failed(%d)",ret_code);
      gtk_label_set_label((GtkLabel*)widget,widget_text);
    }
  }
  else
  {
    if(chip_is_valid)
    {
      printf("Chip %d valid\n",CATIA_number);
    }
    else
    {
      printf("Chip %d not valid. Failure code : \n",CATIA_number,ret_code);
    }
  }

  Double_t calib_pos=0.;
  Double_t calib_jitter=0.;
  Double_t RT=0.;
  Double_t eRT=0.;
  if(chip_is_valid)
  {
    tfr1=hcalib_t0->Fit("gaus","LQ",fit_opt);
    f1=hcalib_t0->GetFunction("gaus");
    if(f1!=NULL)
    {
      calib_pos=f1->GetParameter(1);
      calib_jitter=f1->GetParameter(2);
      f1->~TF1();
    }
    tfr1.~TFitResultPtr();
    tfr1=hcalib_rt->Fit("gaus","LQ",fit_opt);
    f1=hcalib_rt->GetFunction("gaus");
    if(f1!=NULL)
    {
      RT=f1->GetParameter(1);
      eRT=f1->GetParameter(2);
      f1->~TF1();
    }
    tfr1.~TFitResultPtr();
    printf("Pulse jitter : %.1f ps, rise time %.2f ns\n",calib_jitter*1.e12,RT*1.e9);

// Save ADC calibration (full list of registers :
    sprintf(output_file,"data/CATIA_%8.8d_%2.2d_ADC_calib.dat",CATIA_number,CATIA_revision);
    fcal=fopen(output_file,"w+");
    for(int iADC=0; iADC<2; iADC++)
    {
      Int_t num=asic.DTU_number[ich];
      device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;      // ADC sub-addresses
      for(int ireg=0; ireg<N_ADC_REG; ireg++)
      {
        ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, 0);
        //printf("%d %d %x\n",iADC, ireg,ADC_reg_val[iADC][ireg]);
      }
    }
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      //printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xFF, ADC_reg_val[1][ireg]&0xFF);
      fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
    }
    fclose(fcal);
  }

// Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();

// Switch OFF the board
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  }
  else
  {
    switch_power(FALSE);
  }

  pmod[0]->~TProfile();
  delete pmod[0];
  pmod[1]->~TProfile();
  delete pmod[1];
// Now compute Rconv for current injection should be around 272 and 2472 Ohms
  tg_Vref->Write();
  tg_Vref->~TGraph();
  delete tg_Vref;
  tg_Vdac[0]->Write();
  tg_Vdac[0]->~TGraph();
  delete tg_Vdac[0];
  tg_Vdac[1]->Write();
  tg_Vdac[1]->~TGraph();
  delete tg_Vdac[1];
  tg_Vdac_resi[0]->Write();
  tg_Vdac_resi[0]->~TGraph();
  delete tg_Vdac_resi[0];
  tg_Vdac_resi[1]->Write();
  tg_Vdac_resi[1]->~TGraph();
  delete tg_Vdac_resi[1];
  tg_ped_DAC[0]->Write();
  tg_ped_DAC[1]->Write();
  tg_pulse[0]->Write();
  tg_pulse[0]->~TGraph();
  delete tg_pulse[0];
  tg_pulse[1]->Write();
  tg_pulse[1]->~TGraph();
  delete tg_pulse[1];
  hmod[0][0]->Write();
  hmod[0][0]->~TH1D();
  delete hmod[0][0];
  hmod[0][1]->Write();
  hmod[0][1]->~TH1D();
  delete hmod[0][1];
  hmod[0][2]->Write();
  hmod[0][2]->~TH1D();
  delete hmod[0][2];
  hmod[0][3]->Write();
  hmod[0][3]->~TH1D();
  delete hmod[0][3];
  hmod[0][4]->Write();
  hmod[0][4]->~TH1D();
  delete hmod[0][4];
  hmod[0][5]->Write();
  hmod[0][5]->~TH1D();
  delete hmod[0][5];
  hmod[1][0]->Write();
  hmod[1][0]->~TH1D();
  delete hmod[1][0];
  hmod[1][1]->Write();
  hmod[1][1]->~TH1D();
  delete hmod[1][1];
  hmod[1][2]->Write();
  hmod[1][2]->~TH1D();
  delete hmod[1][2];
  hmod[1][3]->Write();
  hmod[1][3]->~TH1D();
  delete hmod[1][3];
  hmod[1][4]->Write();
  hmod[1][4]->~TH1D();
  delete hmod[1][4];
  hmod[1][5]->Write();
  hmod[1][5]->~TH1D();
  delete hmod[1][5];
  tp_inj_cal->Write();
  tp_inj_cal->~TProfile();
  delete tp_inj_cal;
  for(int iinj=0; iinj<daq.n_TP_step; iinj++)
  {
    tp_inj1[0][iinj]->Write();
    tp_inj1[0][iinj]->~TProfile();
    delete tp_inj1[0][iinj];
    tp_inj1[1][iinj]->Write();
    tp_inj1[1][iinj]->~TProfile();
    delete tp_inj1[1][iinj];
    hp_inj1[0][iinj]->Write();
    hp_inj1[0][iinj]->~TH1D();
    delete hp_inj1[0][iinj];
    hp_inj1[1][iinj]->Write();
    hp_inj1[1][iinj]->~TH1D();
    delete hp_inj1[1][iinj];
  }
  tg_inj1[0]->Write();
  tg_inj1[0]->~TGraph();
  delete tg_inj1[0];
  tg_inj1[1]->Write();
  tg_inj1[1]->~TGraph();
  delete tg_inj1[1];
  tg_inj2[0]->Write();
  tg_inj2[0]->~TGraph();
  delete tg_inj2[0];
  tg_inj2[1]->Write();
  tg_inj2[1]->~TGraph();
  delete tg_inj2[1];
  tg_inj3[0]->Write();
  tg_inj3[0]->~TGraph();
  delete tg_inj3[0];
  tg_inj3[1]->Write();
  tg_inj3[1]->~TGraph();
  delete tg_inj3[1];
  tp_calib[0][0]->Write();
  tp_calib[0][0]->~TProfile();
  delete tp_calib[0][0];
  tp_calib[0][1]->Write();
  tp_calib[0][1]->~TProfile();
  delete tp_calib[0][1];
  tp_calib[0][2]->Write();
  tp_calib[0][2]->~TProfile();
  delete tp_calib[0][2];
  tp_calib[1][0]->Write();
  tp_calib[1][0]->~TProfile();
  delete tp_calib[1][0];
  tp_calib[1][1]->Write();
  tp_calib[1][1]->~TProfile();
  delete tp_calib[1][1];
  tp_calib[1][2]->Write();
  tp_calib[1][2]->~TProfile();
  delete tp_calib[1][2];
  tg_calib[0]->Write();
  tg_calib[0]->~TGraph();
  delete tg_calib[0];
  tg_calib[1]->Write();
  tg_calib[1]->~TGraph();
  delete tg_calib[1];
  hcalib_t0->Write();
  hcalib_t0->~TH1D();
  delete hcalib_t0;
  hcalib_rt->Write();
  hcalib_rt->~TH1D();
  delete hcalib_rt;
  tg_calib_AWG[0]->~TGraph();
  delete tg_calib_AWG[0];
  tg_calib_AWG[1]->~TGraph();
  delete tg_calib_AWG[1];
  tp_calib_AWG[0][0]->~TProfile();
  delete tp_calib_AWG[0][0];
  tp_calib_AWG[0][1]->~TProfile();
  delete tp_calib_AWG[0][1];
  tp_calib_AWG[0][2]->~TProfile();
  delete tp_calib_AWG[0][2];
  tp_calib_AWG[1][0]->~TProfile();
  delete tp_calib_AWG[1][0];
  tp_calib_AWG[1][1]->~TProfile();
  delete tp_calib_AWG[1][1];
  tp_calib_AWG[1][2]->~TProfile();
  delete tp_calib_AWG[1][2];
  hcalib_AWG_t0->~TH1D();
  delete hcalib_AWG_t0;
  hcalib_AWG_rt->~TH1D();
  delete hcalib_AWG_rt;
  fcalib->~TF1();
  delete fcalib;
  fd->Close();
  fd->~TFile();
  delete fd;
  
  // Only increment Ntested if chip tested for the first time
  bool chip_in_goodlist = std::find(j["Summary"]["good_chips"].begin(), j["Summary"]["good_chips"].end(), CATIA_number) != std::end(j["Summary"]["good_chips"]);
  bool chip_in_badlist  = std::find(j["Summary"]["bad_chips"].begin(), j["Summary"]["bad_chips"].end(), CATIA_number) != std::end(j["Summary"]["bad_chips"]);

  if (!chip_in_goodlist && !chip_in_badlist) {
    j["Summary"]["Ntested"] = j["Summary"]["Ntested"].get<int>()+1;
  }

  // Fill good_chips/bad_chips entries depending on test results
  if(chip_is_valid && !chip_in_goodlist) {
    j["Summary"]["Ngood"] = j["Summary"]["Ngood"].get<int>()+1;
    j["Summary"]["good_chips"]+=CATIA_number;  
    // remove duplicates
    if(chip_in_badlist){
      auto pos_badchip = std::find(j["Summary"]["bad_chips"].begin(), j["Summary"]["bad_chips"].end(), CATIA_number);
      j["Summary"]["bad_chips"].erase(pos_badchip);
    } 
  } else if (!chip_in_badlist) {
    j["Summary"]["Nbad"] = j["Summary"]["Nbad"].get<int>()+1;
    j["Summary"]["bad_chips"]+=CATIA_number;  
  }
    
  std::string sCATIA_number = std::to_string(CATIA_number);
  j["Chip"][sCATIA_number]["Revision"] = CATIA_revision;

  j["Chip"][sCATIA_number]["test"]["power"] = daq.power_valid;

  sprintf(output_file,"data/CATIA_%8.8d_%2.2d_calib.root",CATIA_number,CATIA_revision);
  j["Chip"][sCATIA_number]["ROOT_output"] = output_file;

  sprintf(output_file,"data/CATIA_%8.8d_%2.2d_calib.txt",CATIA_number,CATIA_revision);
  FILE *fout=fopen(output_file,"w+");
  fprintf(fout,"# Version 3\n");
  fprintf(fout,"# %s\n",daq.comment);
  fprintf(fout,"# Return code :\n%d\n",ret_code);
  fprintf(fout,"# timestamp\n");
  gettimeofday(&tv,NULL);
  fprintf(fout,"%ld.%6.6ld\n",tv.tv_sec,tv.tv_usec);
 
  fprintf(fout,"# Voltages [V] and currents [mA] for 1.2V and 2.5V\n");
  fprintf(fout,"%.3f %.0f %.3f %.0f\n",V1P2_old,I1P2_old,V2P5_old,I2P5_old);
  fprintf(fout,"%.0f %.0f %d\n",daq.I2P5_min,daq.I2P5_max,daq.power_valid);
  j["Chip"][sCATIA_number]["V1p2"] = V1P2_old;
  j["Chip"][sCATIA_number]["V2p5"] = V2P5_old;
  j["Chip"][sCATIA_number]["I1p2"] = I1P2_old;
  j["Chip"][sCATIA_number]["I2p5"] = I2P5_old;

  fprintf(fout,"# Temperature FPGA CATIA_X1 CATIA_X5\n");
  fprintf(fout,"%e %e %e\n",Temp[0],Temp[1],Temp[2]);
  j["Chip"][sCATIA_number]["Temp_FPGA"] = Temp[0];
  j["Chip"][sCATIA_number]["Temp_CATIA_X1"] = Temp[1];
  j["Chip"][sCATIA_number]["Temp_CATIA_X5"] = Temp[2];

  fprintf(fout,"# Vref DAC values\n");
  fprintf(fout,"%d\n",n_Vref);
  for(Int_t i=0; i<n_Vref; i++) fprintf(fout,"%e ",Vref[i]);
  fprintf(fout,"\n");

  fprintf(fout,"# Vref best value and limits\n");
  fprintf(fout,"%.3f %d\n",Vref_best,             ref_best);
  fprintf(fout,"%.3f %d\n",daq.bandgap_value_min, daq.bandgap_setting_min);
  fprintf(fout,"%.3f %d\n",daq.bandgap_value_max, daq.bandgap_setting_max);
  fprintf(fout,"%d\n", daq.bandgap_valid);
  j["Chip"][sCATIA_number]["Bandgap_value"] = Vref_best;
  j["Chip"][sCATIA_number]["Bandgap_setting"] = ref_best;
  j["Chip"][sCATIA_number]["test"]["bandgap"] = daq.bandgap_valid;
  
  fprintf(fout,"# Iinj DAC values : in, DAC1, DAC2\n");
  fprintf(fout,"%d\n",n_Vdac);
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",DAC_val[i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[0][i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[1][i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC1 : slope, offset. Value, min and max\n");
  fprintf(fout,"%e %e\n",Vdac_slope[0],Vdac_offset[0]);
  fprintf(fout,"%e %e\n",daq.DAC1_slope_min, daq.DAC1_offset_min);
  fprintf(fout,"%e %e\n",daq.DAC1_slope_max, daq.DAC1_offset_max);
  fprintf(fout,"%d\n",daq.DAC1_valid);
  fprintf(fout,"# Iinj DAC2 : slope, offset. Value, min and max\n");
  fprintf(fout,"%e %e\n",Vdac_slope[1],      Vdac_offset[1]);
  fprintf(fout,"%e %e\n",daq.DAC2_slope_min, daq.DAC2_offset_min);
  fprintf(fout,"%e %e\n",daq.DAC2_slope_max, daq.DAC2_offset_max);
  fprintf(fout,"%d\n",daq.DAC2_valid);
  j["Chip"][sCATIA_number]["DAC1_slope"] = Vdac_slope[0];
  j["Chip"][sCATIA_number]["DAC1_offset"] = Vdac_offset[0];
  j["Chip"][sCATIA_number]["DAC2_slope"] = Vdac_slope[1];
  j["Chip"][sCATIA_number]["DAC2_offset"] = Vdac_offset[1];
  j["Chip"][sCATIA_number]["test"]["DAC1"] = daq.DAC1_valid;
  j["Chip"][sCATIA_number]["test"]["DAC2"] = daq.DAC2_valid;

  fprintf(fout,"# PED DAC values : G1, G10\n");
  fprintf(fout,"%d\n",n_PED_dac);
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[0]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  tg_ped_DAC[0]->~TGraph();
  delete tg_ped_DAC[0];
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[1]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  tg_ped_DAC[1]->~TGraph();
  delete tg_ped_DAC[1];
  fprintf(fout,"\n");
  fprintf(fout,"# PED DAC parameters : G1_ped_offset, G1_ped_slope, G1_best. Value, min and max\n");
  fprintf(fout,"%e %e %d\n", ped_DAC_par[1][0],     ped_DAC_par[1][1],    ped_ref[2][1]);
  fprintf(fout,"%e %e %d\n", daq.ped_G1_offset_min, daq.ped_G1_slope_min, daq.ped_G1_best_min);
  fprintf(fout,"%e %e %d\n", daq.ped_G1_offset_max, daq.ped_G1_slope_max, daq.ped_G1_best_max);
  j["Chip"][sCATIA_number]["Ped_G1_offset"] = ped_DAC_par[1][0]; //*
  j["Chip"][sCATIA_number]["Ped_G1_slope"] = ped_DAC_par[1][1];  //*
  j["Chip"][sCATIA_number]["Ped_G1_best"] = ped_ref[2][1]; //*
  fprintf(fout,"# PED DAC parameters : G10_ped_offset, G10_ped_slope, G10_best. Value, min and max\n");
  fprintf(fout,"%e %e %d\n", ped_DAC_par[0][0],      ped_DAC_par[0][1],     ped_ref[2][0]);
  fprintf(fout,"%e %e %d\n", daq.ped_G10_offset_min, daq.ped_G10_slope_min, daq.ped_G10_best_min);
  fprintf(fout,"%e %e %d\n", daq.ped_G10_offset_max, daq.ped_G10_slope_max, daq.ped_G10_best_max);
  j["Chip"][sCATIA_number]["Ped_G10_offset"] = ped_DAC_par[0][0]; //*
  j["Chip"][sCATIA_number]["Ped_G10_slope"] = ped_DAC_par[0][1]; //*
  j["Chip"][sCATIA_number]["Ped_G10_best"] = ped_ref[2][0]; //*
  fprintf(fout,"%d\n",daq.pedestal_valid);
  j["Chip"][sCATIA_number]["test"]["pedestal"] = daq.pedestal_valid;

  fprintf(fout,"# Noise : G1-average, G1-R470-LPF50, G1-R470-LPF35 G1-R380-LPF50 G1-R380-LPF35 G1-R320-LPF50 G1-R320-LPF35. Value, min and max\n");
  fprintf(fout,"%e %e %e %e %e %e %e\n",daq.noise_G1_val, G1_noise[0][0], G1_noise[0][1], G1_noise[1][0], G1_noise[1][1], G1_noise[2][0], G1_noise[2][1]);
  fprintf(fout,"%e\n",daq.noise_G1_min);
  fprintf(fout,"%e\n",daq.noise_G1_max);
  j["Chip"][sCATIA_number]["Noise_G1_avg"] = daq.noise_G1_val;
  j["Chip"][sCATIA_number]["Noise_G1_R470-LPF50"] = G1_noise[0][0];
  j["Chip"][sCATIA_number]["Noise_G1_R470-LPF35"] = G1_noise[0][1];
  j["Chip"][sCATIA_number]["Noise_G1_R380-LPF50"] = G1_noise[1][0];
  j["Chip"][sCATIA_number]["Noise_G1_R380-LPF35"] = G1_noise[1][1];
  j["Chip"][sCATIA_number]["Noise_G1_R320-LPF50"] = G1_noise[2][0];
  j["Chip"][sCATIA_number]["Noise_G1_R320-LPF35"] = G1_noise[2][1];
  fprintf(fout,"# Noise : G10-R470-LPF50, G10-R470-LPF35 G10-R380-LPF50 G10-R380-LPF35 G10-R320-LPF50 G10-R320-LPF35. Values, min and max\n");
  fprintf(fout,"%e %e %e %e %e %e\n", G10_noise[0][0],G10_noise[0][1],G10_noise[1][0],G10_noise[1][1], G10_noise[2][0], G10_noise[2][1]);
  fprintf(fout,"%e %e %e %e %e %e\n",daq.noise_R470_LPF50_min,daq.noise_R470_LPF35_min,daq.noise_R380_LPF50_min,daq.noise_R380_LPF35_min,daq.noise_R320_LPF50_min,daq.noise_R320_LPF35_min);
  fprintf(fout,"%e %e %e %e %e %e\n",daq.noise_R470_LPF50_max,daq.noise_R470_LPF35_max,daq.noise_R380_LPF50_max,daq.noise_R380_LPF35_max,daq.noise_R320_LPF50_max,daq.noise_R320_LPF35_max);
  j["Chip"][sCATIA_number]["Noise_G10-LPF50"] = G10_noise[0][0];
  j["Chip"][sCATIA_number]["Noise_G10-LPF35"] = G10_noise[0][1];
  j["Chip"][sCATIA_number]["Noise_G10-LPF50"] = G10_noise[1][0];
  j["Chip"][sCATIA_number]["Noise_G10-LPF35"] = G10_noise[1][1];
  j["Chip"][sCATIA_number]["Noise_G10-LPF50"] = G10_noise[2][0];
  j["Chip"][sCATIA_number]["Noise_G10-LPF35"] = G10_noise[2][1];
  fprintf(fout,"%d\n",daq.noise_valid);
  j["Chip"][sCATIA_number]["test"]["noise"] = daq.noise_valid;

  fprintf(fout,"# INL : G10-low G10-up, (up-low)/2, G1-low G1-up, (up-low)/2. points then fit method. Values, min and max\n");
  fprintf(fout,"%e %e %e %e %e %e %e %e %e %e %e %e\n",
               INL_min[0],    INL_max[0],    (INL_max[0]-INL_min[0])/2.,        INL_min[1],    INL_max[1],    (INL_max[1]-INL_min[1])/2.,
               INL_min_fit[0],INL_max_fit[0],(INL_max_fit[0]-INL_min_fit[0])/2.,INL_min_fit[1],INL_max_fit[1],(INL_max_fit[1]-INL_min_fit[1])/2.);
  fprintf(fout,"%e %e\n",daq.linearity_G1_min,daq.linearity_G10_min);
  fprintf(fout,"%e %e\n",daq.linearity_G1_max,daq.linearity_G10_max);
  j["Chip"][sCATIA_number]["INL_G1"] = (INL_min[1]-INL_min[1])/2.;
  j["Chip"][sCATIA_number]["INL_G10"] = (INL_min[0]-INL_min[0])/2.;
  fprintf(fout,"%d\n",daq.linearity_valid);
  j["Chip"][sCATIA_number]["test"]["linearity"] = daq.linearity_valid;

  fprintf(fout,"# CATIA gains : G1-R470 G1-R380 G1-R320 G10-R470 G10-R380 G10-R320 (LPF35)\n");
  fprintf(fout,"%e %e %e %e %e %e\n",CATIA_gain[1][0],CATIA_gain[1][1],CATIA_gain[1][2],CATIA_gain[0][0],CATIA_gain[0][1],CATIA_gain[0][2]);
  fprintf(fout,"# CATIA parameters : G10 R470 R380 R320 Rconv_G1 Rconv_G10. Values, min and max\n");
  fprintf(fout,"%e %e %e %e %e %e\n",gain_stage,TIA_gain[0],TIA_gain[1],TIA_gain[2],Rconv[1],Rconv[0]);
  fprintf(fout,"%e %e %e %e %e %e\n",daq.gain_int_G10_min, daq.gain_int_R470_min, daq.gain_int_R380_min, daq.gain_int_R320_min, daq.gain_int_R2471_min, daq.gain_int_R272_min);
  fprintf(fout,"%e %e %e %e %e %e\n",daq.gain_int_G10_max, daq.gain_int_R470_max, daq.gain_int_R380_max, daq.gain_int_R320_max, daq.gain_int_R2471_max, daq.gain_int_R272_max);
  j["Chip"][sCATIA_number]["Gains_G10"] = gain_stage;
  j["Chip"][sCATIA_number]["Gains_R470"] = TIA_gain[0];
  j["Chip"][sCATIA_number]["Gains_R380"] = TIA_gain[1];
  j["Chip"][sCATIA_number]["Gains_R320"] = TIA_gain[2];
  j["Chip"][sCATIA_number]["Gains_R2471"] = Rconv[1];
  j["Chip"][sCATIA_number]["Gains_R272"] = Rconv[0];
  fprintf(fout,"%d\n",daq.gain_internal_valid);
  j["Chip"][sCATIA_number]["test"]["gain_internal"] = daq.gain_internal_valid;

  fprintf(fout,"# Timing with calib pulses : pos, jitter, rising_time, rising_time RMS\n");
  fprintf(fout,"%e %e %e %e\n",calib_pos,calib_jitter,RT,eRT);
  j["Chip"][sCATIA_number]["timing_pos"] = calib_pos;
  j["Chip"][sCATIA_number]["timing_jitter"] = calib_jitter;
  j["Chip"][sCATIA_number]["timing_rising_time"] = RT;
  j["Chip"][sCATIA_number]["timing_rising_time_RMS"] = eRT;

  fclose(fout);
  gettimeofday(&tv,NULL);
  Double_t end_time=tv.tv_sec+tv.tv_usec*1.e-6;

  // write prettified JSON to another file
  std::ofstream o(daq.JSONdb);
  o << std::setw(4) << j << std::endl;

  printf("End of CATIA calibration @ %.6lf\n",end_time);

  sprintf(widget_text,"%.0f s",end_time-start_time);
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_duration");
    g_assert (widget);
    gtk_label_set_label((GtkLabel*)widget,widget_text);
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
  }
  else
  {
    printf("Test duration : %s\n",widget_text);
  }
  if(!widget) delete widget;
  if(!buffer) delete buffer;
  return ret_code;
}
