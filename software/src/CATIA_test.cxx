#include <gtk/gtk.h>
#include "gdaq_VFE.h"

void myCSS(void)
{
  GtkCssProvider *provider;
  GdkDisplay *display;
  GdkScreen *screen;

  provider = gtk_css_provider_new ();
  display = gdk_display_get_default ();
  screen = gdk_display_get_default_screen (display);
  gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  const gchar *myCssFile = "mystyle.css";
  GError *error = 0;

  gtk_css_provider_load_from_file(provider, g_file_new_for_path(myCssFile), &error);
  g_object_unref (provider);
}

void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);

  if(daq.fd!=NULL)
  {
    if(daq.tdata!=NULL)daq.tdata->Write();
    if(daq.c1!=NULL)daq.c1->Write();
    if(daq.c2!=NULL)daq.c2->Write();
    if(daq.c3!=NULL)daq.c3->Write();
    daq.fd->Close();
  }
  if(!daq.cli)gtk_main_quit();
  exit(1);
}

void intHandler(int)
{
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}

void abortHandler(int)
{
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}

//// try to catch segmentation faults but it is useless
void segfaultHandler(int)
{
  printf("Segmentation fault accured !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  printf("We will try to restart from scratch\n");
  //end_of_run();
}

int main(int argc, char *argv [])
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);
  //signal(SIGSEGV, segfaultHandler);

  gtk_data.main_window = NULL;

  daq.tdata=NULL;
  daq.c1=NULL;
  daq.c2=NULL;
  daq.c3=NULL;
  daq.fd=NULL;
  gchar *filename = NULL;
  GError *error = NULL;
  daq.comment[0]='\0';
  daq.cli=FALSE;
  Int_t redo=0;
  for(Int_t iargc=1; iargc<argc; iargc++)
  {
    if(strcmp(argv[iargc],"--cli")==0)
    {
      daq.cli=TRUE;
    }
    if(strcmp(argv[iargc],"--redo")==0)
    {
      redo=1;
    }
  }
  printf("Start run with auto_run = %d\n",daq.cli);

  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);

  if(!daq.cli)
  {
/* Initialisation de la librairie Gtk. */

    gtk_disable_setlocale ();
    gtk_init(&argc, &argv);
    myCSS();

  /* Ouverture du fichier Glade de la fenêtre principale */
    gtk_data.builder = gtk_builder_new();

  /* Création du chemin complet pour accéder au fichier test.glade. */
  /* g_build_filename(); construit le chemin complet en fonction du système */
  /* d'exploitation. ( / pour Linux et \ pour Windows) */
  //filename =  g_build_filename ("test.glade", NULL);
    filename =  g_build_filename ("xml/prod_test.glade", NULL);

  /* Chargement du fichier test.glade. */
    gtk_builder_add_from_file (gtk_data.builder, filename, &error);
    g_free (filename);
    if (error)
    {
      gint code = error->code;
      g_printerr("%s\n", error->message);
      g_error_free (error);
      return code;
    }

    /* Affectation des signaux de l'interface aux différents CallBacks. */
    gtk_builder_connect_signals (gtk_data.builder, NULL);

    /* Récupération du pointeur de la fenêtre principale */
    gtk_data.main_window = GTK_WIDGET(gtk_builder_get_object (gtk_data.builder, "0_prod_test"));

    /* Affichage de la fenêtre principale. */
    gtk_widget_show_all(gtk_data.main_window);
  }

  daq.daq_initialized=-1;
// Read default values and
// Disable channels if default values are such :
  for(Int_t ich=0; ich<5; ich++)
  {
    asic.DTU_clk_out[ich]=FALSE;
    asic.DTU_VCO_rail_mode[ich]=TRUE;
    asic.DTU_bias_ctrl_override[ich]=FALSE;
    asic.DTU_eLink[ich]=TRUE;
  }
  daq.calib_mux_enabled=0;
  daq.n_active_channel=0;
  daq.eLink_active=0;
  daq.is_VICEPP=false;
  char fname[132],fname2[110];
  sprintf(daq.xml_filename,"xml/config_CATIA_test.xml");
  read_conf_from_xml_file(daq.xml_filename);
  strncpy(fname2,daq.xml_filename,110);
  sprintf(fname,"CATIA_test.exe : %s",fname2);
  if(!daq.cli)gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  printf("Start DAQ with %d active channels\n",daq.n_active_channel);

  daq.clock_select=0;
  daq.trigger_type=-1;
  daq.self_trigger=0;
  daq.self_trigger_mode=0;
  daq.self_trigger_threshold=100;
  daq.self_trigger_mask=0x1F;
  daq.debug_draw &= !daq.cli;
  daq.debug_I2C  &= !daq.cli;

  if(!daq.cli)
  {
    gtk_main();
  }
  else
  {
    init_gDAQ();
    Int_t CATIA_valid=CATIA_calibration(redo);
    exit(CATIA_valid);
  }

  exit(0);
}
