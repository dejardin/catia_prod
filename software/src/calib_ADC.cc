#define EXTERN extern
#include "gdaq_VFE.h"
void calib_ADC(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, iret;
  ValWord<uint32_t> reg;

// Put calibration voltages to ADC inputs :
// First if pedestal multiplexer is used :
  UInt_t VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan | eLINK_ACTIVE*daq.eLink_active | DTU_TEST_MODE*daq.DTU_test_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
// And for CATIA version >=1.4 :
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    if(num<=0)continue;
    device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(asic.CATIA_version[ich]>=14)
    {
// Set CATIA outputs with calibration levels
      iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
      printf("Set CATIA %d outputs with calibration levels : 0x%4.4x\n\n",ich+1,iret&0xFFFF);
    }
  }
  usleep(100000); // Let some time for calibration voltages to stabilize

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.DTU_number[ich];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number);
    if(asic.DTU_dither[ich])
    {   
      I2C_RW(hw, device_number+0, 3, 0x01, 0, 1, daq.debug_I2C);
      I2C_RW(hw, device_number+1, 3, 0x01, 0, 1, daq.debug_I2C);
    }   
    if(asic.DTU_nsample_calib_x4[ich])
    {   
      I2C_RW(hw, device_number+0, 1, 0xfe, 0, 1, daq.debug_I2C);
      I2C_RW(hw, device_number+1, 1, 0xfe, 0, 1, daq.debug_I2C);
    }   
    if(asic.DTU_global_test[ich])
    {
      I2C_RW(hw, device_number+0, 0, 0x01, 0, 1, daq.debug_I2C);
      I2C_RW(hw, device_number+1, 0, 0x01, 0, 1, daq.debug_I2C);
    }
  }

  if(daq.firmware_version<0x21090000)
  {
    send_ReSync_command(LiTEDTU_ADCL_reset);
    send_ReSync_command(LiTEDTU_ADCH_reset);
    usleep(100);
    printf("Launch ADC calibration old !\n");
    send_ReSync_command(LiTEDTU_ADCL_calib);
    send_ReSync_command(LiTEDTU_ADCH_calib);
    usleep(1000);
  }
  else
  {
    send_ReSync_command((LiTEDTU_ADCL_reset<<4) | LiTEDTU_ADCH_reset);
    usleep(1000);
    printf("Launch ADC calibration new !\n");
    send_ReSync_command((LiTEDTU_ADCL_calib<<4) | LiTEDTU_ADCH_calib);
    usleep(10000);
  }

// Restore CAL mux in normal position after calibration
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan | eLINK_ACTIVE*daq.eLink_active | DTU_TEST_MODE*daq.DTU_test_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

// Put back CATIA output voltages to what they were before calibration
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    UInt_t ich=daq.channel_number[i];
    update_CATIA_reg(ich,1);
    update_CATIA_reg(ich,3);
    update_CATIA_reg(ich,4);
    update_CATIA_reg(ich,5);
    update_CATIA_reg(ich,6);
  }
// And wait for voltage stabilization
  usleep(100000);
}
