#define EXTERN extern
#include <gtk/gtk.h>
#include "gdaq_VFE.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "limits.h"

void read_config(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Open File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_OPEN,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    strcpy(daq.xml_filename,filename);
    read_conf_from_xml_file (filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);
  }

  gtk_widget_destroy (dialog);
  return;
}
void save_config(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Save current config in %s\n",daq.xml_filename);
  write_conf_to_xml_file(daq.xml_filename);
  return;
}

void save_config_as(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Save File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_SAVE,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
  printf("Current file : %s\n",daq.xml_filename);
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), daq.xml_filename);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    printf("Select file : %s\n",filename);
    strcpy(daq.xml_filename,filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);
    write_conf_to_xml_file(daq.xml_filename);
  }

  gtk_widget_destroy (dialog);
  return;
}

void read_conf_from_xml_file(char *filename)
{
  xmlChar *object_name, *prop_name, *prop_value, *child_status, *child_id;
  xmlDocPtr doc;
  xmlNodePtr cur, child, child2;
  Int_t loc_ich;

  char fname[132], widget_name[80], entry_text[80];
  GtkWidget *widget;
  daq.FEAD_number=0;
  daq.VFE_number=0;
  daq.resync_phase=0;
  daq.daq_initialized=-1;
  for(Int_t ich=0; ich<5; ich++)
  {
    asic.DTU_clk_out[ich]=FALSE;
    asic.DTU_VCO_rail_mode[ich]=TRUE;
    asic.DTU_bias_ctrl_override[ich]=FALSE;
    asic.DTU_eLink[ich]=TRUE;
  }
  daq.calib_mux_enabled=0;
  daq.n_active_channel=0;
  daq.eLink_active=0;

  daq.trigger_type=-1;
  daq.self_trigger=0;
  daq.self_trigger_mode=0;
  daq.self_trigger_threshold=100;
  daq.self_trigger_mask=0x1F;
  daq.ADC_use_ref_calib=FALSE;

// disable channels by defaults :
  for(Int_t ich=0; ich<5; ich++)
  {
    asic.CATIA_number[ich]=-1;
    asic.DTU_number[ich]=-1;
    //enable_channel(ich+1,FALSE);
    asic.CATIA_reg_def[ich][0]=0x00;
    asic.CATIA_reg_def[ich][1]=0x02;
    asic.CATIA_reg_def[ich][2]=0x00;
    asic.CATIA_reg_def[ich][3]=0x1087;
    asic.CATIA_reg_def[ich][4]=0xFFFF;
    asic.CATIA_reg_def[ich][5]=0xFFFF;
    asic.CATIA_reg_def[ich][6]=0x0F;
  }
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,FALSE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_measure_voltages");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,FALSE);
    allow_limit_tuning(FALSE,FALSE);
  }


  daq.test_power = TRUE;
  daq.test_temperature=TRUE;
  daq.test_bandgap=TRUE;
  daq.test_DAC1=TRUE;
  daq.test_DAC2=TRUE;
  daq.test_pedestal=TRUE;
  daq.test_noise=TRUE;
  daq.test_linearity=TRUE;
  daq.test_gain_internal=TRUE;
  daq.test_gain_AWG=FALSE;
  daq.half_mode=FALSE; // 80 MHz sampling

  daq.n_active_channel=0;
  daq.eLink_active=0;
  if(filename==NULL)
    sprintf(fname,"xml/VFE_config_%2.2d.xml",daq.VFE_number);
  else
    strcpy(fname,filename);
  printf("Read conf from xml file %s\n",fname);

  doc = xmlParseFile(fname);
  if (doc == NULL )
  {
    fprintf(stderr,"Document not parsed successfully. \n");
    return;
  }

  cur = xmlDocGetRootElement(doc);
  if (cur == NULL)
  {
    fprintf(stderr,"empty document\n");
    xmlFreeDoc(doc);
    return;
  }

  printf("Start to parse document : %s\n",fname);
  if (xmlStrcmp(cur->name, (const xmlChar *) "vfe"))
  {
    fprintf(stderr,"document of the wrong type, root node != vfe");
    xmlFreeDoc(doc);
    return;
  }
  else
  {
    printf("Starting point : %s\n",cur->name);
  }

  cur = cur->xmlChildrenNode;
  while (cur != NULL)
  {
    if ((!xmlStrcmp(cur->name, (const xmlChar *)"object")))
    {
      printf("Start to parse object : %s\n",xmlGetProp(cur,(const xmlChar *)"name"));

// DAQ settings :
      object_name=xmlGetProp(cur,(const xmlChar *)"name");
      if ((!xmlStrcmp(object_name, (const xmlChar *)"DAQ")))
      {
        child = cur->xmlChildrenNode;
        while (child != NULL)
        {
          if ((!xmlStrcmp(child->name, (const xmlChar *)"property")))
          {
            prop_value = xmlNodeListGetString(doc, child->xmlChildrenNode, 1);
            prop_name=xmlGetProp(child,(const xmlChar *)"name");
            printf("property %s :  %s\n",prop_name, prop_value);
            if(!xmlStrcmp(prop_name, (const xmlChar *)"FEAD"))
            {
              sscanf((const char*)prop_value,"%d",&daq.FEAD_number);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_FEAD_number");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.FEAD_number);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
              daq.is_VICEPP=false;
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"VICEPP"))
            {
              sscanf((const char*)prop_value,"%d",&daq.FEAD_number);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_FEAD_number");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.FEAD_number);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
              daq.is_VICEPP=true;
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"JSONdb"))
            {
              prop_value=xmlGetProp(child,(const xmlChar *)"path");
              std::cout<<"prop_value = "<<prop_value<<std::endl;
              sscanf((const char*)prop_value,"%s",daq.JSONdb);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DB_name");
                g_assert (widget);
                sprintf(entry_text,"%s",daq.JSONdb);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"resync_phase"))
            {
              sscanf((const char*)prop_value,"%d",&daq.resync_phase);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_mode"))
            {
              sscanf((const char*)prop_value,"%d",&daq.DTU_test_mode);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I2C_shift_dev_number"))
            {
              sscanf((const char*)prop_value,"%d",&daq.I2C_shift_dev_number);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_ana"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_ana);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_DAQ"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_DAQ);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_debug_DAQ");
                g_assert (widget);
                if(daq.debug_DAQ==0)
                  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                else
                  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_I2C"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_I2C);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_draw"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_draw);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_debug_draw");
                g_assert (widget);
                if(daq.debug_draw==0)
                {
                  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                }
                else
                {
                  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                }
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"zoom_draw"))
            {
              sscanf((const char*)prop_value,"%d",&daq.zoom_draw);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"corgain"))
            {
              daq.corgain=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"wait_for_ever"))
            {
              daq.wait_for_ever=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I2C_lpGBT"))
            {
              daq.I2C_lpGBT_mode=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"HW_delay"))
            {
              sscanf((const char*)prop_value,"%d",&daq.hw_DAQ_delay);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"SW_delay"))
            {
              sscanf((const char*)prop_value,"%d",&daq.sw_DAQ_delay);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_delay"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_delay);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_width"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_width);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_level"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_level);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_step"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_step);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"n_TP_step"))
            {
              sscanf((const char*)prop_value,"%d",&daq.n_TP_step);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"n_TP_event"))
            {
              sscanf((const char*)prop_value,"%d",&daq.n_TP_event);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TE"))
            {
              daq.TE_enable=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              prop_value=xmlGetProp(child,(const xmlChar *)"pos");
              sscanf((const char*)prop_value,"%d",&daq.TE_pos);
              prop_value=xmlGetProp(child,(const xmlChar *)"command");
              sscanf((const char*)prop_value,"%x",&daq.TE_command);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"nevent"))
            {
              sscanf((const char*)prop_value,"%d",&daq.nevent);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"nsample"))
            {
              sscanf((const char*)prop_value,"%d",&daq.nsample);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg1_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][1]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][1]=asic.CATIA_reg_def[0][1]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg2_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][2]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][2]=asic.CATIA_reg_def[0][2]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg3_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][3]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][3]=asic.CATIA_reg_def[0][3]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg4_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][4]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][4]=asic.CATIA_reg_def[0][4]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg5_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][5]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][5]=asic.CATIA_reg_def[0][5]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg6_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][6]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][6]=asic.CATIA_reg_def[0][6]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"LVRB_autoscan"))
            {
              daq.LVRB_autoscan=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_scan_Vref"))
            {
              daq.CATIA_scan_Vref=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_Vref_target"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Vref_target);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"LiTEDTU_consumption_V1P2_ref"))
            {
              sscanf((const char*)prop_value,"%lf",&asic.DTU_consumption_ref);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Use_ref_ADC_calib"))
            {
              daq.ADC_use_ref_calib=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Rshunt_V1P2"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Rshunt_V1P2);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Rshunt_V2P5"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Rshunt_V2P5);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Tsensor_divider"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Tsensor_divider);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"PLL_override_Vc_bit"))
            {
              asic.DTU_override_Vc_bit[0]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              for(Int_t i=1; i<5; asic.DTU_override_Vc_bit[i++]=asic.DTU_override_Vc_bit[0]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"use_half_mode"))
            {
              daq.half_mode=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_power"))
            {
              daq.test_power=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_power");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_power);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_temperature"))
            {
              daq.test_temperature=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_temperature);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_bandgap"))
            {
              daq.test_bandgap=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_bandgap);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_DAC1"))
            {
              daq.test_DAC1=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_DAC1");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_DAC1);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_DAC2"))
            {
              daq.test_DAC2=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_DAC2");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_DAC2);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_pedestal"))
            {
              daq.test_pedestal=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_pedestal);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_noise"))
            {
              daq.test_noise=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_noise);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_linearity"))
            {
              daq.test_linearity=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_linearity);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_gain_internal"))
            {
              daq.test_gain_internal=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal");
                g_assert (widget);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_gain_internal);
                gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_CATIA_gain_AWG"))
            {
              daq.test_gain_AWG=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              if(!daq.cli)
              {
//              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG");
//              g_assert (widget);
//              gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.test_gain_AWG);
//              gtk_button_clicked((GtkButton*)widget);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I1P2_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.I1P2_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.I1P2_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I1P2_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.I1P2_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.I1P2_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I2P5_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.I2P5_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.I2P5_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I2P5_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.I2P5_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.I2P5_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"temp_X1_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.temp_X1_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_min");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.temp_X1_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"temp_X1_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.temp_X1_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_max");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.temp_X1_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"temp_X5_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.temp_X5_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_min");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.temp_X5_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"temp_X5_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.temp_X5_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_max");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.temp_X5_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"bandgap_value_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.bandgap_value_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_value_min");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.bandgap_value_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"bandgap_value_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.bandgap_value_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_value_max");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.bandgap_value_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"bandgap_setting_min"))
            {
              sscanf((const char*)prop_value,"%d",&daq.bandgap_setting_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_setting_min");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.bandgap_setting_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"bandgap_setting_max"))
            {
              sscanf((const char*)prop_value,"%d",&daq.bandgap_setting_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_setting_max");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.bandgap_setting_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC1_offset_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC1_offset_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_min");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.DAC1_offset_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC1_offset_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC1_offset_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_max");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.DAC1_offset_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC1_slope_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC1_slope_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_min");
                g_assert (widget);
                sprintf(entry_text,"%.1e",daq.DAC1_slope_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC1_slope_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC1_slope_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_max");
                g_assert (widget);
                sprintf(entry_text,"%.1e",daq.DAC1_slope_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC2_offset_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC2_offset_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_min");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.DAC2_offset_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC2_offset_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC2_offset_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_max");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.DAC2_offset_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC2_slope_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC2_slope_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_min");
                g_assert (widget);
                sprintf(entry_text,"%.1e",daq.DAC2_slope_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"DAC2_slope_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.DAC2_slope_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_max");
                g_assert (widget);
                sprintf(entry_text,"%.1e",daq.DAC2_slope_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G10_offset_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G10_offset_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_offset_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.ped_G10_offset_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G10_offset_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G10_offset_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_offset_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.ped_G10_offset_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G10_slope_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G10_slope_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_slope_min");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.ped_G10_slope_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G10_slope_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G10_slope_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_slope_max");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.ped_G10_slope_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G10_best_min"))
            {
              sscanf((const char*)prop_value,"%d",&daq.ped_G10_best_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_best_min");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.ped_G10_best_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G10_best_max"))
            {
              sscanf((const char*)prop_value,"%d",&daq.ped_G10_best_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_best_max");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.ped_G10_best_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G1_offset_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G1_offset_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_offset_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.ped_G1_offset_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G1_offset_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G1_offset_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_offset_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.ped_G1_offset_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G1_slope_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G1_slope_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_slope_min");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.ped_G1_slope_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G1_slope_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.ped_G1_slope_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_slope_max");
                g_assert (widget);
                sprintf(entry_text,"%.2f",daq.ped_G1_slope_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G1_best_min"))
            {
              sscanf((const char*)prop_value,"%d",&daq.ped_G1_best_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_best_min");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.ped_G1_best_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"ped_G1_best_max"))
            {
              sscanf((const char*)prop_value,"%d",&daq.ped_G1_best_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_best_max");
                g_assert (widget);
                sprintf(entry_text,"%d",daq.ped_G1_best_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R320_LPF50_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R320_LPF50_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF50_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R320_LPF50_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R320_LPF50_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R320_LPF50_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF50_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R320_LPF50_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R320_LPF35_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R320_LPF35_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF35_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R320_LPF35_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R320_LPF35_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R320_LPF35_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF35_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R320_LPF35_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R380_LPF50_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R380_LPF50_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF50_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R380_LPF50_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R380_LPF50_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R380_LPF50_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF50_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R380_LPF50_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R380_LPF35_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R380_LPF35_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF35_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R380_LPF35_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R380_LPF35_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R380_LPF35_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF35_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R380_LPF35_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R470_LPF50_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R470_LPF50_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF50_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R470_LPF50_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R470_LPF50_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R470_LPF50_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF50_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R470_LPF50_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R470_LPF35_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R470_LPF35_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF35_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R470_LPF35_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_R470_LPF35_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_R470_LPF35_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF35_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_R470_LPF35_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_G1_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_G1_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_G1_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"noise_G1_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.noise_G1_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.noise_G1_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"linearity_G10_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.linearity_G10_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G10_min");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.linearity_G10_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"linearity_G10_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.linearity_G10_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G10_max");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.linearity_G10_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"linearity_G1_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.linearity_G1_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G1_min");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.linearity_G1_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"linearity_G1_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.linearity_G1_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G1_max");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.linearity_G1_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_G10_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_G10_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_G10_min");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.gain_int_G10_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_G10_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_G10_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_G10_max");
                g_assert (widget);
                sprintf(entry_text,"%.1f",daq.gain_int_G10_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R320_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R320_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R320_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R320_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R320_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R320_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R320_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R320_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R380_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R380_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R380_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R380_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R380_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R380_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R380_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R380_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R470_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R470_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R470_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R470_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R470_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R470_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R470_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R470_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R2471_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R2471_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R2471_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R2471_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R2471_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R2471_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R2471_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R2471_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R272_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R272_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R272_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R272_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_int_R272_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_int_R272_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R272_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_int_R272_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
/*
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R320_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R320_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R320_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R320_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R320_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R320_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R320_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R320_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R380_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R380_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R380_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R380_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R380_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R380_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R380_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R380_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R470_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R470_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R470_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R470_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R470_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R470_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R470_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R470_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R2471_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R2471_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R2471_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R2471_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R2471_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R2471_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R2471_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R2471_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R272_min"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R272_min);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R272_min");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R272_min);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"gain_AWG_R272_max"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.gain_AWG_R272_max);
              if(!daq.cli)
              {
                widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R272_max");
                g_assert (widget);
                sprintf(entry_text,"%.0f",daq.gain_AWG_R272_max);
                gtk_entry_set_text((GtkEntry*)widget,entry_text);
              }
            }
*/
            xmlFree(prop_name);
            xmlFree(prop_value);
          }
          child = child->next;
        }
        printf("Will use clock from FEAD board, %d ",daq.FEAD_number);
        printf("with debug flags : DAQ %d, I2C %d, GTK %d, draw %d\n",daq.debug_DAQ,daq.debug_I2C,daq.debug_GTK,daq.debug_draw);
      }

// Channel settings :
      else if (!xmlStrcmp(object_name, (const xmlChar *)"channel"))
      {
        prop_value=xmlGetProp(cur,(const xmlChar *)"id");
        sscanf((const char*)prop_value,"%d",&loc_ich);
        xmlFree(prop_value);
        if(loc_ich<1 || loc_ich>5) continue;
        prop_value=xmlGetProp(cur,(const xmlChar *)"status");
        if(!xmlStrcasecmp(prop_value,(const xmlChar *)"ON"))
        {
          //enable_channel(loc_ich,TRUE);
          asic.CATIA_number[loc_ich-1]=loc_ich;
          asic.DTU_number[loc_ich-1]=loc_ich;
          daq.channel_number[daq.n_active_channel++]=loc_ich-1;
          asic.DTU_eLink[loc_ich]=TRUE;
          daq.eLink_active|=(1<<(loc_ich-1));
// n_active_channel updated in widget action :
          printf("n_active_channel : %d\n",daq.n_active_channel);
        }
        printf("Channel id %d, status %s\n",loc_ich,prop_value);
        xmlFree(prop_value);

        child = xmlFirstElementChild(cur);
        object_name=xmlGetProp(child,(const xmlChar *)"name");
        printf("First Child name : %s %s\n",child->name,object_name);
        do
        {
          if ((!xmlStrcmp(child->name, (const xmlChar *)"object")))
          {
            object_name=xmlGetProp(child,(const xmlChar *)"name");
            child_status=xmlGetProp(child,(const xmlChar *)"status");
            child_id=xmlGetProp(child,(const xmlChar *)"id");
            printf("Child name : %s %s %s, status %s\n",child->name,object_name,child_status,child_id);

            child2 = child->xmlChildrenNode;
            while (child2 != NULL)
            {
              if ((!xmlStrcmp(child2->name, (const xmlChar *)"property")))
              {
                prop_value=xmlNodeListGetString(doc, child2->xmlChildrenNode, 1);
                prop_name=xmlGetProp(child2,(const xmlChar *)"name");
                printf("property %s :  %s\n",prop_name, prop_value);
                if(!xmlStrcmp(object_name,(const xmlChar *)"CATIA"))
                {
                  sscanf((const char*)child_id,"%d",&asic.CATIA_number[loc_ich-1]);
                  if(!xmlStrcmp(prop_name,(const xmlChar *)"version"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_version[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<=14)daq.calib_mux_enabled=1;
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"ped_G10"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_ped_G10[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<14 && asic.CATIA_ped_G10[loc_ich-1]>31)asic.CATIA_ped_G10[loc_ich-1]=31;
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"ped_G1"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_ped_G1[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<14 && asic.CATIA_ped_G1[loc_ich-1]>31)asic.CATIA_ped_G1[loc_ich-1]=31;
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"Vref"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_Vref_val[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"gain"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_gain[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<14 && asic.CATIA_gain[loc_ich-1]>1)asic.CATIA_gain[loc_ich-1]=1;
                    if(asic.CATIA_gain[loc_ich-1]==2)asic.CATIA_gain[loc_ich-1]++;
                    if(asic.CATIA_version[loc_ich-1]<14)
                    {
                      if(asic.CATIA_gain[loc_ich-1]==0)
                        sprintf(entry_text,"500");
                      else
                        sprintf(entry_text,"400");
                    }
                    else if(asic.CATIA_version[loc_ich-1]<=20)
                    {
                      if(asic.CATIA_gain[loc_ich-1]==0)
                        sprintf(entry_text,"500");
                      else if(asic.CATIA_gain[loc_ich-1]==1)
                        sprintf(entry_text,"400");
                      else
                        sprintf(entry_text,"340");
                    }
                    else
                    {
                      if(asic.CATIA_gain[loc_ich-1]==0)
                        sprintf(entry_text,"470");
                      else if(asic.CATIA_gain[loc_ich-1]==1)
                        sprintf(entry_text,"380");
                      else
                        sprintf(entry_text,"320");
                    }
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"LPF"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_LPF35[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"DAC1"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_DAC1_val[loc_ich-1]);
                    prop_value=xmlGetProp(child2,(const xmlChar *)"status");
                    asic.CATIA_DAC1_status[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"DAC2"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_DAC2_val[loc_ich-1]);
                    prop_value=xmlGetProp(child2,(const xmlChar *)"status");
                    asic.CATIA_DAC2_status[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"SEU_corr"))
                  {
                    asic.CATIA_SEU_corr[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    if(!daq.cli)
                    {
                      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    }
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"Temp_offset"))
                  {
                    sscanf((const char*)prop_value,"%lf",&asic.CATIA_Temp_offset[loc_ich-1]);
                  }
                }
                else if(!xmlStrcmp(object_name,(const xmlChar *)"LiTEDTU"))
                {
                  sscanf((const char*)child_id,"%d",&asic.DTU_number[loc_ich-1]);
                  if(!xmlStrcmp(prop_name,(const xmlChar *)"version"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_version[loc_ich-1]);
                  }
                  if(!xmlStrcmp(prop_name,(const xmlChar *)"eLink"))
                  {
                    asic.DTU_force_G10[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"BL_G10"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_BL_G10[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"BL_G1"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_BL_G1[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"force_G10"))
                  {
                    asic.DTU_force_G10[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"force_G1"))
                  {
                    asic.DTU_force_G1[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"PLL"))
                  {
                    sscanf((const char*)prop_value,"%x",&asic.DTU_PLL_conf[loc_ich-1]);
                    asic.DTU_PLL_conf[loc_ich-1] &= 0x01ff;
                    asic.DTU_PLL_conf1[loc_ich-1]= asic.DTU_PLL_conf[loc_ich-1]&0x7;
                    asic.DTU_PLL_conf2[loc_ich-1]= asic.DTU_PLL_conf[loc_ich-1]>>3;
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"force_PLL"))
                  {
                    asic.DTU_force_PLL[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"PLL_clock_out"))
                  {
                    asic.DTU_clk_out[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"VCO_rail_mode"))
                  {
                    asic.DTU_VCO_rail_mode[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"bias_ctrl_override"))
                  {
                    asic.DTU_bias_ctrl_override[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"driver_current"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_driver_current[loc_ich-1]);
                    if(asic.DTU_driver_current[loc_ich-1]>7)asic.DTU_driver_current[loc_ich-1]=7;
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"PE_width"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_PE_width[loc_ich-1]);
                    if(asic.DTU_PE_width[loc_ich-1]>7)asic.DTU_PE_width[loc_ich-1]=7;
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"PE_strength"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_PE_strength[loc_ich-1]);
                    if(asic.DTU_PE_strength[loc_ich-1]>7)asic.DTU_PE_strength[loc_ich-1]=7;
                  }
                }

                xmlFree(prop_value);
                xmlFree(prop_name);
              }
              child2 = child2->next;
            }
          }
          child=child->next;
        }while(child!=NULL);

        printf("Load config for channel %d : CATIA version : %.1f, LiTE-DTU version %.1f, bs10 0x%3.3x, bs1 0x%3.3x, pll 0x%3.3x=0x%2.2x 0x%1.1x, Vref %d, CATIA gain %d, CATIA LPF %d, force DTU PLL %d\n",
               loc_ich,asic.CATIA_version[loc_ich-1]/10.,asic.DTU_version[loc_ich-1]/10.,
               asic.DTU_BL_G10[loc_ich-1],   asic.DTU_BL_G1[loc_ich-1],
               asic.DTU_PLL_conf[loc_ich-1],  asic.DTU_PLL_conf1[loc_ich-1],asic.DTU_PLL_conf2[loc_ich-1],
               asic.CATIA_Vref_val[loc_ich-1],asic.CATIA_gain[loc_ich-1],   asic.CATIA_LPF35[loc_ich-1], asic.DTU_force_PLL[loc_ich-1]);
      }
    }

    cur = cur->next;
  }
  xmlFreeDoc(doc);

// Update gtk menu with new parameters :
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_start");
    g_assert (widget);
    gtk_widget_set_name(widget,"blue_button");
  }

  if(daq.n_active_channel==1 && daq.channel_number[0]==2 && daq.DTU_test_mode==1)
  {
    daq.eLink_active=0xF;
// Activate the CATIA_calib button only for test board (1 channel, 4 outputs)
    if(!daq.cli) gtk_widget_set_sensitive(widget,TRUE);
    printf("Switch to test board setting !\n");
  }
  else
  {
    if(!daq.cli) gtk_widget_set_sensitive(widget,FALSE);
  }
  printf("Will use I2C_shift_dev_number %d\n",daq.I2C_shift_dev_number);
  update_trigger();

// Update CATIA_number entry :
  Int_t ich=daq.channel_number[0];
  Int_t CATIA_number   = asic.CATIA_version[ich]*1000000+999999;
  Int_t ref_number;
  FILE *fnumber=NULL;
  fnumber=fopen(".last_CATIA_number","r");
  if(fnumber != NULL)
  {
    Int_t eof=0, loc_version;
    char snumber[80];
    while(eof!=EOF)
    {
      eof=fscanf(fnumber,"%s",snumber);
      if(eof!=EOF)
      {
        sscanf(snumber,"%2d",&loc_version);
        sscanf(snumber,"%d",&ref_number);
        printf("Number found : %d version : %d\n",ref_number,loc_version);
        if(loc_version==asic.CATIA_version[ich]) CATIA_number=ref_number;
      }
    }
    fclose(fnumber);
  }
  printf("Last CATIA number : %d\n",CATIA_number);
  printf("JSON db : %s\n",daq.JSONdb);
  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_CATIA_number");
    g_assert (widget);
    sprintf(entry_text,"%8.8d",CATIA_number);
    gtk_entry_set_text((GtkEntry*)widget,entry_text);
  }
}

void write_conf_to_xml_file(char *filename)
{
  char status[80];

  FILE*fd_out=fopen(filename,"w+");
  fprintf(fd_out,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(fd_out,"<vfe>\n");
  fprintf(fd_out,"  <object name=\"DAQ\">\n");
  if(daq.is_VICEPP)
    fprintf(fd_out,"    <property name=\"VICEPP\">%d</property>\n",daq.FEAD_number);
  else
    fprintf(fd_out,"    <property name=\"FEAD\">%d</property>\n",daq.FEAD_number);
  fprintf(fd_out,"    <property name=\"JSONdb\" path=\"%s\"></property>\n",daq.JSONdb);
  fprintf(fd_out,"    <property name=\"resync_phase\">%d</property>\n",daq.resync_phase);
  fprintf(fd_out,"    <property name=\"test_mode\">%d</property>\n",daq.DTU_test_mode);
  fprintf(fd_out,"    <property name=\"I2C_shift_dev_number\">%d</property>\n",daq.I2C_shift_dev_number);
  fprintf(fd_out,"    <property name=\"debug_DAQ\">%d</property>\n",daq.debug_DAQ);
  fprintf(fd_out,"    <property name=\"debug_I2C\">%d</property>\n",daq.debug_I2C);
  fprintf(fd_out,"    <property name=\"debug_GTK\">%d</property>\n",daq.debug_GTK);
  fprintf(fd_out,"    <property name=\"debug_draw\">%d</property>\n",daq.debug_draw);
  fprintf(fd_out,"    <property name=\"debug_ana\">%d</property>\n",daq.debug_ana);
  fprintf(fd_out,"    <property name=\"zoom_draw\">%d</property>\n",daq.zoom_draw);
  sprintf(status,"OFF");
  if(daq.corgain) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"corgain\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.wait_for_ever) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"wait_for_ever\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.I2C_lpGBT_mode) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"I2C_lpGBT\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"HW_delay\">%d</property>\n",daq.hw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"SW_delay\">%d</property>\n",daq.sw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"TP_delay\">%d</property>\n",daq.TP_delay);
  fprintf(fd_out,"    <property name=\"TP_width\">%d</property>\n",daq.TP_width);
  fprintf(fd_out,"    <property name=\"TP_level\">%d</property>\n",daq.TP_level);
  fprintf(fd_out,"    <property name=\"TP_step\">%d</property>\n",daq.TP_step);
  fprintf(fd_out,"    <property name=\"n_TP_step\">%d</property>\n",daq.n_TP_step);
  fprintf(fd_out,"    <property name=\"n_TP_event\">%d</property>\n",daq.n_TP_event);
  sprintf(status,"OFF");
  if(daq.TE_enable) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"TE\" pos=\"%d\" command=\"%x\">%s</property>\n",daq.TE_pos, daq.TE_command,status);
  fprintf(fd_out,"    <property name=\"nevent\">%d</property>\n",daq.nevent);
  fprintf(fd_out,"    <property name=\"nsample\">%d</property>\n",daq.nsample);
  sprintf(status,"OFF");
  if(daq.LVRB_autoscan) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"LVRB_autoscan\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.CATIA_scan_Vref) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"CATIA_scan_Vref\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"CATIA_Vref_target\">%.3f</property>\n",daq.Vref_target);
  fprintf(fd_out,"    <property name=\"LiTEDTU_consumption_V1P2_ref\">%.0f</property>\n",asic.DTU_consumption_ref);
  sprintf(status,"OFF");
  if(daq.ADC_use_ref_calib) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"Use_ref_ADC_calib\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"Rshunt_V1P2\">%.2f</property>\n",daq.Rshunt_V1P2);
  fprintf(fd_out,"    <property name=\"Rshunt_V2P5\">%.2f</property>\n",daq.Rshunt_V2P5);
  fprintf(fd_out,"    <property name=\"Tsensor_divider\">%.3f</property>\n",daq.Tsensor_divider);
  sprintf(status,"OFF");
  if(asic.DTU_override_Vc_bit[0]==1) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"PLL_override_Vc_bit\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"CATIA_reg1_def\">0x%2.2x</property>\n",asic.CATIA_reg_def[0][1]);
  fprintf(fd_out,"    <property name=\"CATIA_reg2_def\">0x%2.2x</property>\n",asic.CATIA_reg_def[0][2]);
  fprintf(fd_out,"    <property name=\"CATIA_reg3_def\">0x%4.4x</property>\n",asic.CATIA_reg_def[0][3]);
  fprintf(fd_out,"    <property name=\"CATIA_reg4_def\">0x%4.4x</property>\n",asic.CATIA_reg_def[0][4]);
  fprintf(fd_out,"    <property name=\"CATIA_reg5_def\">0x%4.4x</property>\n",asic.CATIA_reg_def[0][5]);
  fprintf(fd_out,"    <property name=\"CATIA_reg6_def\">0x%2.2x</property>\n",asic.CATIA_reg_def[0][6]);
  fprintf(fd_out,"<!-- CATIA test : What do we do ? -->\n");
  sprintf(status,"OFF");
  if(daq.test_power) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_power\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_temperature) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_temperature\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_bandgap) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_bandgap\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_DAC1) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_DAC1\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_DAC2) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_DAC2\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_pedestal) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_pedestal\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_noise) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_noise\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_linearity) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_linearity\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_gain_internal) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_gain_int\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.test_gain_AWG) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"test_CATIA_gain_AWG\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"I1P2_min\">%.0f</property>\n",daq.I1P2_min);
  fprintf(fd_out,"    <property name=\"I1P2_max\">%.0f</property>\n",daq.I1P2_max);
  fprintf(fd_out,"    <property name=\"I2P5_min\">%.0f</property>\n",daq.I2P5_min);
  fprintf(fd_out,"    <property name=\"I2P5_max\">%.0f</property>\n",daq.I2P5_max);
  fprintf(fd_out,"    <property name=\"temp_X1_min\">%.2f</property>\n",daq.temp_X1_min);
  fprintf(fd_out,"    <property name=\"temp_X1_max\">%.2f</property>\n",daq.temp_X1_max);
  fprintf(fd_out,"    <property name=\"temp_X5_min\">%.2f</property>\n",daq.temp_X5_min);
  fprintf(fd_out,"    <property name=\"temp_X5_max\">%.2f</property>\n",daq.temp_X5_max);
  fprintf(fd_out,"    <property name=\"bandgap_value_min\">%.2f</property>\n",daq.bandgap_value_min);
  fprintf(fd_out,"    <property name=\"bandgap_value_max\">%.2f</property>\n",daq.bandgap_value_max);
  fprintf(fd_out,"    <property name=\"bandgap_setting_min\">%d</property>\n",daq.bandgap_setting_min);
  fprintf(fd_out,"    <property name=\"bandgap_setting_max\">%d</property>\n",daq.bandgap_setting_max);
  fprintf(fd_out,"    <property name=\"DAC1_offset_min\">%.0f</property>\n",daq.DAC1_offset_min);
  fprintf(fd_out,"    <property name=\"DAC1_offset_max\">%.0f</property>\n",daq.DAC1_offset_max);
  fprintf(fd_out,"    <property name=\"DAC1_slope_min\">%.2e</property>\n",daq.DAC1_slope_min);
  fprintf(fd_out,"    <property name=\"DAC1_slope_max\">%.2e</property>\n",daq.DAC1_slope_max);
  fprintf(fd_out,"    <property name=\"DAC2_offset_min\">%.0f</property>\n",daq.DAC2_offset_min);
  fprintf(fd_out,"    <property name=\"DAC2_offset_max\">%.0f</property>\n",daq.DAC2_offset_max);
  fprintf(fd_out,"    <property name=\"DAC2_slope_min\">%.2e</property>\n",daq.DAC2_slope_min);
  fprintf(fd_out,"    <property name=\"DAC2_slope_max\">%.2e</property>\n",daq.DAC2_slope_max);
  fprintf(fd_out,"    <property name=\"ped_G10_offset_min\">%.0f</property>\n",daq.ped_G10_offset_min);
  fprintf(fd_out,"    <property name=\"ped_G10_offset_max\">%.0f</property>\n",daq.ped_G10_offset_max);
  fprintf(fd_out,"    <property name=\"ped_G10_slope_min\">%.2f</property>\n",daq.ped_G10_slope_min);
  fprintf(fd_out,"    <property name=\"ped_G10_slope_max\">%.2f</property>\n",daq.ped_G10_slope_max);
  fprintf(fd_out,"    <property name=\"ped_G10_best_min\">%d</property>\n",daq.ped_G10_best_min);
  fprintf(fd_out,"    <property name=\"ped_G10_best_max\">%d</property>\n",daq.ped_G10_best_max);
  fprintf(fd_out,"    <property name=\"ped_G1_offset_min\">%.0f</property>\n",daq.ped_G1_offset_min);
  fprintf(fd_out,"    <property name=\"ped_G1_offset_max\">%.0f</property>\n",daq.ped_G1_offset_max);
  fprintf(fd_out,"    <property name=\"ped_G1_slope_min\">%.2f</property>\n",daq.ped_G1_slope_min);
  fprintf(fd_out,"    <property name=\"ped_G1_slope_max\">%.2f</property>\n",daq.ped_G1_slope_max);
  fprintf(fd_out,"    <property name=\"ped_G1_best_min\">%d</property>\n",daq.ped_G1_best_min);
  fprintf(fd_out,"    <property name=\"ped_G1_best_max\">%d</property>\n",daq.ped_G1_best_max);
  fprintf(fd_out,"    <property name=\"noise_R320_LPF50_min\">%.0f</property>\n",daq.noise_R320_LPF50_min);
  fprintf(fd_out,"    <property name=\"noise_R320_LPF50_max\">%.0f</property>\n",daq.noise_R320_LPF35_max);
  fprintf(fd_out,"    <property name=\"noise_R320_LPF35_min\">%.0f</property>\n",daq.noise_R320_LPF50_min);
  fprintf(fd_out,"    <property name=\"noise_R320_LPF35_max\">%.0f</property>\n",daq.noise_R320_LPF35_max);
  fprintf(fd_out,"    <property name=\"noise_R380_LPF50_min\">%.0f</property>\n",daq.noise_R380_LPF50_min);
  fprintf(fd_out,"    <property name=\"noise_R380_LPF50_max\">%.0f</property>\n",daq.noise_R380_LPF35_max);
  fprintf(fd_out,"    <property name=\"noise_R380_LPF35_min\">%.0f</property>\n",daq.noise_R380_LPF50_min);
  fprintf(fd_out,"    <property name=\"noise_R380_LPF35_max\">%.0f</property>\n",daq.noise_R380_LPF35_max);
  fprintf(fd_out,"    <property name=\"noise_R470_LPF50_min\">%.0f</property>\n",daq.noise_R470_LPF50_min);
  fprintf(fd_out,"    <property name=\"noise_R470_LPF50_max\">%.0f</property>\n",daq.noise_R470_LPF35_max);
  fprintf(fd_out,"    <property name=\"noise_R470_LPF35_min\">%.0f</property>\n",daq.noise_R470_LPF50_min);
  fprintf(fd_out,"    <property name=\"noise_R470_LPF35_max\">%.0f</property>\n",daq.noise_R470_LPF35_max);
  fprintf(fd_out,"    <property name=\"noise_G1_min\">%.0f</property>\n",daq.noise_G1_min);
  fprintf(fd_out,"    <property name=\"noise_G1_max\">%.0f</property>\n",daq.noise_G1_max);
  fprintf(fd_out,"    <property name=\"linearity_G10_min\">%.1f</property>\n",daq.linearity_G10_min);
  fprintf(fd_out,"    <property name=\"linearity_G10_max\">%.1f</property>\n",daq.linearity_G10_max);
  fprintf(fd_out,"    <property name=\"linearity_G1_min\">%.1f</property>\n",daq.linearity_G1_min);
  fprintf(fd_out,"    <property name=\"linearity_G1_max\">%.1f</property>\n",daq.linearity_G1_max);
  fprintf(fd_out,"    <property name=\"gain_int_G10_min\">%.0f</property>\n",daq.gain_int_G10_min);
  fprintf(fd_out,"    <property name=\"gain_int_G10_max\">%.0f</property>\n",daq.gain_int_G10_max);
  fprintf(fd_out,"    <property name=\"gain_int_R320_min\">%.0f</property>\n",daq.gain_int_R320_min);
  fprintf(fd_out,"    <property name=\"gain_int_R320_max\">%.0f</property>\n",daq.gain_int_R320_max);
  fprintf(fd_out,"    <property name=\"gain_int_R380_min\">%.0f</property>\n",daq.gain_int_R380_min);
  fprintf(fd_out,"    <property name=\"gain_int_R380_max\">%.0f</property>\n",daq.gain_int_R380_max);
  fprintf(fd_out,"    <property name=\"gain_int_R470_min\">%.0f</property>\n",daq.gain_int_R470_min);
  fprintf(fd_out,"    <property name=\"gain_int_R470_max\">%.0f</property>\n",daq.gain_int_R470_max);
  fprintf(fd_out,"    <property name=\"gain_int_R2471_min\">%.0f</property>\n",daq.gain_int_R2471_min);
  fprintf(fd_out,"    <property name=\"gain_int_R2471_max\">%.0f</property>\n",daq.gain_int_R2471_max);
  fprintf(fd_out,"    <property name=\"gain_int_R272_min\">%.0f</property>\n",daq.gain_int_R272_min);
  fprintf(fd_out,"    <property name=\"gain_int_R272_max\">%.0f</property>\n",daq.gain_int_R272_max);
  fprintf(fd_out,"    <property name=\"gain_AWG_R320_min\">%.0f</property>\n",daq.gain_AWG_R320_min);
  fprintf(fd_out,"    <property name=\"gain_AWG_R320_max\">%.0f</property>\n",daq.gain_AWG_R320_max);
  fprintf(fd_out,"    <property name=\"gain_AWG_R380_min\">%.0f</property>\n",daq.gain_AWG_R380_min);
  fprintf(fd_out,"    <property name=\"gain_AWG_R380_max\">%.0f</property>\n",daq.gain_AWG_R380_max);
  fprintf(fd_out,"    <property name=\"gain_AWG_R470_min\">%.0f</property>\n",daq.gain_AWG_R470_min);
  fprintf(fd_out,"    <property name=\"gain_AWG_R470_max\">%.0f</property>\n",daq.gain_AWG_R470_max);
  fprintf(fd_out,"    <property name=\"gain_AWG_R2471_min\">%.0f</property>\n",daq.gain_AWG_R2471_min);
  fprintf(fd_out,"    <property name=\"gain_AWG_R2471_max\">%.0f</property>\n",daq.gain_AWG_R2471_max);
  fprintf(fd_out,"    <property name=\"gain_AWG_R272_min\">%.0f</property>\n",daq.gain_AWG_R272_min);
  fprintf(fd_out,"    <property name=\"gain_AWG_R272_max\">%.0f</property>\n",daq.gain_AWG_R272_max);
  fprintf(fd_out,"  </object>\n");
  for(Int_t ich=2; ich<3; ich++)
  {
    sprintf(status,"OFF");
    if((daq.eLink_active & (1<<ich))!=0) sprintf(status,"ON");
    if(daq.n_active_channel==1 && ich!=daq.channel_number[0])sprintf(status,"OFF");
    fprintf(fd_out,"  <object name=\"channel\" id=\"%d\" status=\"%s\">\n",ich+1,status);
    fprintf(fd_out,"    <object name=\"CATIA\" id=\"%d\">\n",asic.CATIA_number[ich]);
    fprintf(fd_out,"      <property name=\"version\">%d</property>\n",asic.CATIA_version[ich]);
    fprintf(fd_out,"      <property name=\"ped_G10\">%d</property>\n",asic.CATIA_ped_G10[ich]);
    fprintf(fd_out,"      <property name=\"ped_G1\">%d</property>\n",asic.CATIA_ped_G1[ich]);
    fprintf(fd_out,"      <property name=\"Vref\">%d</property>\n",asic.CATIA_Vref_val[ich]);
    fprintf(fd_out,"      <property name=\"gain\">%d</property>\n",asic.CATIA_gain[ich]);
    fprintf(fd_out,"      <property name=\"LPF\">%d</property>\n",asic.CATIA_LPF35[ich]);
    sprintf(status,"OFF");
    if(asic.CATIA_DAC1_status[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"DAC1\" status=\"%s\">%d</property>\n",status,asic.CATIA_DAC1_val[ich]);
    sprintf(status,"OFF");
    if(asic.CATIA_DAC2_status[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"DAC2\" status=\"%s\">%d</property>\n",status,asic.CATIA_DAC2_val[ich]);
    sprintf(status,"OFF");
    if(asic.CATIA_SEU_corr[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"SEU_corr\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"Temp_offset\">%.3f</property>\n",asic.CATIA_Temp_offset[ich]);
    fprintf(fd_out,"    </object>\n");
    fprintf(fd_out,"    <object name=\"LiTEDTU\" id=\"%d\">\n",asic.DTU_number[ich]);
    fprintf(fd_out,"      <property name=\"version\">%d</property>\n",asic.DTU_version[ich]);
    sprintf(status,"OFF");
    if(asic.DTU_eLink[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"eLink\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"BL_G10\">%d</property>\n",asic.DTU_BL_G10[ich]);
    fprintf(fd_out,"      <property name=\"BL_G1\">%d</property>\n",asic.DTU_BL_G1[ich]);
    sprintf(status,"OFF");
    if(asic.DTU_force_G10[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"force_G10\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_force_G1[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"force_G1\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"PLL\">0x%x</property>\n",asic.DTU_PLL_conf[ich]);
    sprintf(status,"OFF");
    if(asic.DTU_force_PLL[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"force_PLL\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_clk_out[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"PLL_clock_out\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_VCO_rail_mode[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"VCO_rail_mode\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_bias_ctrl_override[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"bias_ctrl_override\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"driver_current\">%d</property>\n",asic.DTU_driver_current[ich]);
    fprintf(fd_out,"      <property name=\"PE_width\">%d</property>\n",asic.DTU_PE_width[ich]);
    fprintf(fd_out,"      <property name=\"PE_strength\">%d</property>\n",asic.DTU_PE_strength[ich]);
    fprintf(fd_out,"    </object>\n");
    fprintf(fd_out,"  </object>\n");
  }
  fprintf(fd_out,"</vfe>\n");
  fclose(fd_out);
}

void allow_limit_tuning(bool allow_min, bool allow_max)
{
  GtkWidget *widget;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_value_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_value_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_setting_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_setting_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_offset_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_offset_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_slope_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_slope_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_best_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_best_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_offset_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_offset_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_slope_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_slope_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_best_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_best_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF50_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF50_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF35_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF35_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF50_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF50_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF35_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF35_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF50_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF50_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF35_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF35_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G10_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G10_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G1_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G1_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_G10_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_G10_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R320_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R320_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R380_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R380_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R470_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R470_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R2471_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R2471_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R272_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R272_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  /*
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R320_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R320_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R380_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R380_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R470_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R470_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R2471_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R2471_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R272_min");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_min);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R272_max");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,allow_max);
  */
}

