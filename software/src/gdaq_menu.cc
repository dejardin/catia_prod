#define EXTERN extern
#include <gtk/gtk.h>
#include <TSystem.h>
#include "gdaq_VFE.h"

void callback_about (GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;

  dialog =  gtk_about_dialog_new ();

  /* Pour l'exemple on va rendre la fenêtre a r/opos" modale par rapport a la fenêtre principale. */
  gtk_window_set_transient_for (GTK_WINDOW(dialog), GTK_WINDOW(gtk_builder_get_object (gtk_data.builder, "MainWindow")));

  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

void delete_event(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal delete !\n");
  end_of_run();
}

void my_exit(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal destroy !\n");
  end_of_run();
}

void button_clicked(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget=NULL;
  char widget_name[80];
  char content[80];
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  if(daq.debug_GTK==1)printf("Button %s clicked !\n",name);
  char button_type[80];
  sscanf(&name[2],"%s",button_type);
  if(strcmp(button_type,"debug_DAQ")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_DAQ==0)daq.debug_DAQ=1;
    }
    else
      daq.debug_DAQ=0;
  }
  else if(strcmp(button_type,"debug_draw")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.debug_draw=1;
      printf("Set debug_draw to 1 : %d\n",daq.debug_draw);
      if(daq.c1==NULL)
      {
        daq.c1=new TCanvas("c1","c1",700,660,1200.,420.);
        daq.c1->Divide(3,2);
        daq.c1->Update();
      }
    }
    else
    {
      printf("destroy draw window !\n");
      daq.debug_draw=0;
      printf("Set debug_draw to 0 : %d\n",daq.debug_draw);
      if(daq.c1!=NULL)
      {
        //delete daq.c1;
        daq.c1->~TCanvas();
        gSystem->ProcessEvents();
        daq.c1=NULL;
      }
    }
  }
  else if(strcmp(button_type,"init_DAQ")==0)
  {
    if(daq.daq_initialized<0)
    {
      init_gDAQ();
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_init_DAQ");
      g_assert (widget);
      gtk_button_set_label((GtkButton*)widget,"DAQ initialized");
      daq.daq_initialized=1;
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_start");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_redo");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_measure_voltages");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
  }
  else if(strcmp(button_type,"power_ON")==0)
  {
    printf("Change power status !\n");
    switch_power(gtk_toggle_button_get_active((GtkToggleButton*)button));
  }
  else if(strcmp(button_type,"measure_voltages")==0)
  {
    get_consumption();
  }
  else if(strcmp(button_type,"min_setting")==0)
  {
    daq.min_setting=gtk_toggle_button_get_active((GtkToggleButton*)button);
    allow_limit_tuning(daq.min_setting, daq.max_setting);
  }
  else if(strcmp(button_type,"max_setting")==0)
  {
    daq.max_setting=gtk_toggle_button_get_active((GtkToggleButton*)button);
    allow_limit_tuning(daq.min_setting, daq.max_setting);
  }
  else if(strcmp(button_type,"start")==0)
  {
    if(daq.daq_initialized<0)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_init_DAQ");
      g_assert (widget);
      gtk_button_clicked((GtkButton*)widget);
    }
    CATIA_calibration(0);
  }
  else if(strcmp(button_type,"redo")==0)
  {
    if(daq.daq_initialized<0)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_init_DAQ");
      g_assert (widget);
      gtk_button_clicked((GtkButton*)widget);
    }
    CATIA_calibration(1);
  }
}

void switch_toggled(GtkWidget *button, gpointer user_data)
{
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  char button_type[80];
  char widget_name[80];
  GtkWidget *widget;
  sscanf(&name[2],"%s",button_type);

  if(daq.debug_GTK==1)printf("CheckButton %s type %s toggled : %d, state %d\n",name, button_type,gtk_toggle_button_get_active((GtkToggleButton*)button));
  gtk_toggle_button_set_active((GtkToggleButton*)button,gtk_toggle_button_get_active((GtkToggleButton*)button));

// (de)activate all widgets relative to this test
  if(strcmp(button_type,"power")==0)
  {
    daq.test_power=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_power);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V1P2_consumption_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_power);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_power);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_V2P5_consumption_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_power);
  }
  else if(strcmp(button_type,"temperature")==0)
  {
    daq.test_temperature=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_temperature);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X1_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_temperature);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_temperature);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_temperature_X5_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_temperature);
  }
  else if(strcmp(button_type,"bandgap")==0)
  {
    daq.test_bandgap=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_value_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_bandgap);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_setting_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_bandgap);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_best_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_bandgap);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_bandgap_best_setting");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_bandgap);
  }
  else if(strcmp(button_type,"TP_DAC1")==0)
  {
    daq.test_DAC1=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_offset_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC1_slope_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC1);
  }
  else if(strcmp(button_type,"TP_DAC2")==0)
  {
    daq.test_DAC2=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC2);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_offset_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC2);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC2);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_DAC2_slope_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_DAC2);
  }
  else if(strcmp(button_type,"pedestal")==0)
  {
    daq.test_pedestal=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_offset_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_slope_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_best_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_offset_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_slope_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G10_best_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_offset_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_slope_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_best_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_offset_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_slope_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_pedestal_G1_best_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_pedestal);
  }
  else if(strcmp(button_type,"noise")==0)
  {
    daq.test_noise=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF50_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF50_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF35_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R320_LPF35_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF50_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF50_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF35_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R380_LPF35_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF50_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF50_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF35_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_R470_LPF35_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_noise_G1_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_noise);
  }
  else if(strcmp(button_type,"linearity")==0)
  {
    daq.test_linearity=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G10_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_linearity);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G10_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_linearity);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G1_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_linearity);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_linearity_G1_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_linearity);
  }
  else if(strcmp(button_type,"gain_internal")==0)
  {
    daq.test_gain_internal=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_G10_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_G10_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R320_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R320_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R380_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R380_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R470_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R470_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R2471_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R2471_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R272_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_internal_R272_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_internal);
  }
  else if(strcmp(button_type,"gain_AWG")==0)
  {
    daq.test_gain_AWG=gtk_toggle_button_get_active((GtkToggleButton*)button);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R320_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R320_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R380_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R380_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R470_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R470_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R2471_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R2471_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R272_label");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gain_AWG_R272_value");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,daq.test_gain_AWG);
  }
}

void Entry_modified(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget;

  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  int device_number, iret;
  char text[80], button_type[80], widget_name[80];
  sscanf(&name[2],"%s",button_type);

  if(daq.debug_GTK==1)printf("Entry %s modified : type %s, new value %s\n",name, button_type,gtk_entry_get_text((GtkEntry*)button));
  if(strcmp(button_type,"FEAD_number")==0)
  {
    Int_t loc_number;
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&loc_number);
    daq.FEAD_number=loc_number;
    daq.VFE_number=0;
    printf("New FEAD board %d\n",daq.FEAD_number);
  }
  else if(strcmp(button_type,"DB_name")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%s",daq.JSONdb);
  }
  else if(strcmp(button_type,"CATIA_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.CATIA_test_number);
  }
  else if(strcmp(button_type,"V1P2_consumption_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.I1P2_min);
  }
  else if(strcmp(button_type,"V1P2_consumption_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.I1P2_max);
  }
  else if(strcmp(button_type,"V2P5_consumption_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.I2P5_min);
  }
  else if(strcmp(button_type,"V2P5_consumption_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.I2P5_max);
  }
  else if(strcmp(button_type,"temperature_X1_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.temp_X1_min);
  }
  else if(strcmp(button_type,"temperature_X1_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.temp_X1_max);
  }
  else if(strcmp(button_type,"temperature_X5_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.temp_X5_min);
  }
  else if(strcmp(button_type,"temperature_X5_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.temp_X5_max);
  }
  else if(strcmp(button_type,"bandgap_value_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.bandgap_value_min);
  }
  else if(strcmp(button_type,"bandgap_value_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.bandgap_value_max);
  }
  else if(strcmp(button_type,"bandgap_setting_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.bandgap_setting_min);
  }
  else if(strcmp(button_type,"bandgap_setting_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.bandgap_setting_max);
  }
  else if(strcmp(button_type,"DAC1_offset_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC1_offset_min);
  }
  else if(strcmp(button_type,"DAC1_offset_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC1_offset_max);
  }
  else if(strcmp(button_type,"DAC1_slope_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC1_slope_min);
  }
  else if(strcmp(button_type,"DAC1_slope_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC1_slope_max);
  }
  else if(strcmp(button_type,"DAC2_offset_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC2_offset_min);
  }
  else if(strcmp(button_type,"DAC2_offset_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC2_offset_max);
  }
  else if(strcmp(button_type,"DAC2_slope_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC2_slope_min);
  }
  else if(strcmp(button_type,"DAC2_slope_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.DAC2_slope_max);
  }
  else if(strcmp(button_type,"pedestal_G10_offset_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G10_offset_min);
  }
  else if(strcmp(button_type,"pedestal_G10_offset_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G10_offset_max);
  }
  else if(strcmp(button_type,"pedestal_G10_slope_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G10_slope_min);
  }
  else if(strcmp(button_type,"pedestal_G10_slope_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G10_slope_max);
  }
  else if(strcmp(button_type,"pedestal_G10_best_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.ped_G10_best_min);
  }
  else if(strcmp(button_type,"pedestal_G10_best_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.ped_G10_best_max);
  }
  else if(strcmp(button_type,"pedestal_G1_offset_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G1_offset_min);
  }
  else if(strcmp(button_type,"pedestal_G1_offset_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G1_offset_max);
  }
  else if(strcmp(button_type,"pedestal_G1_slope_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G1_slope_min);
  }
  else if(strcmp(button_type,"pedestal_G1_slope_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.ped_G1_slope_max);
  }
  else if(strcmp(button_type,"pedestal_G1_best_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.ped_G1_best_min);
  }
  else if(strcmp(button_type,"pedestal_G1_best_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.ped_G1_best_max);
  }
  else if(strcmp(button_type,"noise_R320_LPF50_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R320_LPF50_min);
  }
  else if(strcmp(button_type,"noise_R320_LPF50_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R320_LPF50_max);
  }
  else if(strcmp(button_type,"noise_R320_LPF35_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R320_LPF35_min);
  }
  else if(strcmp(button_type,"noise_R320_LPF35_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R320_LPF35_max);
  }
  else if(strcmp(button_type,"noise_R380_LPF50_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R380_LPF50_min);
  }
  else if(strcmp(button_type,"noise_R380_LPF50_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R380_LPF50_max);
  }
  else if(strcmp(button_type,"noise_R380_LPF35_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R380_LPF35_min);
  }
  else if(strcmp(button_type,"noise_R380_LPF35_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R380_LPF35_max);
  }
  else if(strcmp(button_type,"noise_R470_LPF50_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R470_LPF50_min);
  }
  else if(strcmp(button_type,"noise_R470_LPF50_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R470_LPF50_max);
  }
  else if(strcmp(button_type,"noise_R470_LPF35_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R470_LPF35_min);
  }
  else if(strcmp(button_type,"noise_R470_LPF35_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_R470_LPF35_max);
  }
  else if(strcmp(button_type,"noise_G1_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_G1_min);
  }
  else if(strcmp(button_type,"noise_G1_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.noise_G1_max);
  }
  else if(strcmp(button_type,"linearity_G10_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.linearity_G10_min);
  }
  else if(strcmp(button_type,"linearity_G10_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.linearity_G10_max);
  }
  else if(strcmp(button_type,"linearity_G1_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.linearity_G1_min);
  }
  else if(strcmp(button_type,"linearity_G1_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.linearity_G1_max);
  }
  else if(strcmp(button_type,"gain_internal_G10_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_G10_min);
  }
  else if(strcmp(button_type,"gain_internal_G10_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_G10_max);
  }
  else if(strcmp(button_type,"gain_internal_R320_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R320_min);
  }
  else if(strcmp(button_type,"gain_internal_R320_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R320_max);
  }
  else if(strcmp(button_type,"gain_internal_R380_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R380_min);
  }
  else if(strcmp(button_type,"gain_internal_R380_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R380_max);
  }
  else if(strcmp(button_type,"gain_internal_R470_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R470_min);
  }
  else if(strcmp(button_type,"gain_internal_R470_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R470_max);
  }
  else if(strcmp(button_type,"gain_internal_R2471_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R2471_min);
  }
  else if(strcmp(button_type,"gain_internal_R2471_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R2471_max);
  }
  else if(strcmp(button_type,"gain_internal_R272_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R272_min);
  }
  else if(strcmp(button_type,"gain_internal_R272_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_int_R272_max);
  }
  else if(strcmp(button_type,"gain_AWG_R320_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R320_min);
  }
  else if(strcmp(button_type,"gain_AWG_R320_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R320_max);
  }
  else if(strcmp(button_type,"gain_AWG_R380_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R380_min);
  }
  else if(strcmp(button_type,"gain_AWG_R380_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R380_max);
  }
  else if(strcmp(button_type,"gain_AWG_R470_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R470_min);
  }
  else if(strcmp(button_type,"gain_AWG_R470_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R470_max);
  }
  else if(strcmp(button_type,"gain_AWG_R2471_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R2471_min);
  }
  else if(strcmp(button_type,"gain_AWG_R2471_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R2471_max);
  }
  else if(strcmp(button_type,"gain_AWG_R272_min")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R272_min);
  }
  else if(strcmp(button_type,"gain_AWG_R272_max")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.gain_AWG_R272_max);
  }
}

UInt_t update_LiTEDTU_reg(Int_t ich, Int_t reg)
{
  UInt_t iret=0;
  if(daq.daq_initialized>=0 && ich>=0 && ich<5)
  {
    uhal::HwInterface hw=devices.front();
    UInt_t I2C_data=0;
    Int_t num=asic.DTU_number[ich];
    Int_t device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    if(reg==0)
    {
      // Enable outputs
      I2C_data=0x80;
      if(asic.DTU_clk_out[ich])I2C_data|=0x50;
      if(daq.DTU_test_mode==1)
        I2C_data|=0x0F;
      else
        I2C_data|=0x00;
        //I2C_data|=0x01;
      iret=I2C_RW(hw, device_number, 0, I2C_data, 0, 3, daq.debug_I2C);
      printf("1- ch %d, DTU test mode : %d, Reg0 content : W 0x%2.2x, R 0x%2.2x\n",ich, daq.DTU_test_mode, I2C_data, iret&0xff);
    }
    else if(reg==1)
    {
  // ADCs ON and Force/free gain, G1 window :
      I2C_data=0x03; // ADC H&L ON 
      printf("Update register 1 of DTU %d with 0x%2.2x\n",ich+1,I2C_data);
      if(asic.DTU_force_G1[ich])
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0xc0, 0, 3, daq.debug_I2C);
      }
      else if(asic.DTU_force_G10[ich])
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0x40, 0, 3, daq.debug_I2C);
      }
      else if(asic.DTU_G1_window16[ich]==1)
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0x80, 0, 3, daq.debug_I2C);
      }
      else
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data, 0, 3, daq.debug_I2C);
      }
      printf("Reg 1 content 0x%2.2x\n",iret&0xff);
    }
    else if(reg==5) // G10 baseline
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G10[ich], 0, 3, daq.debug_I2C);
    }
    else if(reg==6) // G10 baseline
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G1[ich], 0, 3, daq.debug_I2C);
    }
    else if(reg==7 && asic.DTU_version[ich]>=20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, I2C_data&0xff, 0, 3, daq.debug_I2C);
    }
    else if(reg==8 && asic.DTU_version[ich]>=20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
    }
    else if(reg==17 && asic.DTU_version[ich]<20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, I2C_data&0xff, 0, 3, daq.debug_I2C);
    }
    else if(reg==18 && asic.DTU_version[ich]<20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
    }
    else if(reg==15 && asic.DTU_version[ich]<20) // CapBank_Override - V1.2
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret=(iret&0xfd) | (asic.DTU_override_Vc_bit[ich]<<1);
      iret=I2C_RW(hw, device_number, reg, iret, 0, 3, daq.debug_I2C);
    }
    else if(reg==15 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, reg, (iret&0xFE)|(((asic.DTU_PLL_conf[ich]&0x100)>>8)&1), 0, 1, 0);
    }
    else if(reg==16 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_PLL_conf[ich]&0xFF, 0, 3, daq.debug_I2C);
    }
    else if(reg==8 && asic.DTU_version[ich]<20)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, reg, (iret&0xFE)|(((asic.DTU_PLL_conf[ich]&0x100)>>8)&1), 0, 1, 0);
    }
    else if(reg==9 && asic.DTU_version[ich]<20)
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_PLL_conf[ich]&0xFF, 0, 3, daq.debug_I2C);
    }
    else if(reg==17 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret=(iret&0xf4) | (asic.DTU_force_PLL[ich]<<3) | (1<<2) | (asic.DTU_VCO_rail_mode[ich]<<1) | (asic.DTU_override_Vc_bit[ich]<<0);
      iret=I2C_RW(hw, device_number, reg, iret, 0, 3, daq.debug_I2C);
    }
    else if(reg==25 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, daq.TP_width, 0, 3, daq.debug_I2C);
    }
  }
  return iret;
}

UInt_t update_CATIA_reg(Int_t ich, Int_t reg)
{
  UInt_t iret=0;
  if(daq.daq_initialized>=0 && ich>=0 && ich<5)
  {
    uhal::HwInterface hw=devices.front();
    UInt_t I2C_data=0, I2C_long=0;
    
    Int_t num=asic.CATIA_number[ich];
    if(num<=0)return -1;
    Int_t device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+((ich+1)<<daq.I2C_shift_dev_number)+0xb;

    Int_t loc_DAC1_status=0, loc_DAC2_status=0;
    if(asic.CATIA_DAC1_status[ich])loc_DAC1_status=1;
    if(asic.CATIA_DAC2_status[ich])loc_DAC2_status=1;

    if(reg==1)
    {
      I2C_data=(asic.CATIA_temp_X5[ich]<<4) | (asic.CATIA_temp_out[ich]<<3) | (asic.CATIA_SEU_corr[ich]<<1);
      I2C_long=0;
    }
    else if(reg==3)
    {
// Set pedestal values, gain and LPF
      if(asic.CATIA_version[ich]>=14)
        I2C_data=asic.CATIA_gain[ich]<<14 | asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      else
        I2C_data=asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<3 | asic.CATIA_gain[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      I2C_long=1;
    }
    else if(reg==4)
    {
      I2C_data=asic.CATIA_DAC_buf_off[ich]<<15 | loc_DAC2_status<<13 | loc_DAC1_status<<12 | asic.CATIA_DAC1_val[ich];
      I2C_long=1;
    }
    else if(reg==5)
    {
      I2C_data=(1-asic.CATIA_Vref_out[ich])<<14;;
      Int_t Vcal_out=0;
      if(daq.CATIA_Vcal_out)Vcal_out=3;
      I2C_data |= Vcal_out<<12 | asic.CATIA_DAC2_val[ich];
      I2C_long=1;
    }
    else if(reg==6)
    {
      I2C_data=asic.CATIA_Vref_val[ich]<<4 | asic.CATIA_Rconv[ich]<<2 | 0xb;
      I2C_long=0;
    }
    if(reg>0 && reg<=6)
    {
      iret=I2C_RW(hw, device_number, reg, I2C_data, I2C_long, 3, daq.debug_I2C);
      asic.CATIA_reg_loc[ich][reg]=I2C_data;
    }
  }
  return iret;
}

void update_trigger(void)
{
  if(daq.daq_initialized>=0)
  {
    printf("Update trigger settings :\n");
    printf("calib pulse            : %d\n",daq.calib_pulse_enabled);
    printf("calib mux              : %d\n",daq.calib_mux_enabled);
    printf("force I2C low          : %d\n",daq.I2C_low);
    printf("self trigger           : %d\n",daq.self_trigger);
    printf("self trigger loop      : %d\n",daq.self_trigger_loop);
    printf("self trigger mode      : %d\n",daq.self_trigger_mode);
    printf("self trigger threshold : %d\n",daq.self_trigger_threshold);
    printf("self trigger mask      : %2.2x\n",daq.self_trigger_mask);
  // Switch to triggered mode + external trigger :
    UInt_t command=
               CALIB_PULSE_ENABLED   *daq.calib_pulse_enabled              |
               CALIB_MUX_ENABLED     *daq.calib_mux_enabled                |
               I2C_LOW               *daq.I2C_low                          |
               CLOCK_SELECT          *daq.clock_select                     |
               SELF_TRIGGER_MODE     *daq.self_trigger_mode                |
              (SELF_TRIGGER_MASK     *(daq.self_trigger_mask&0x1F))        |
              (SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0xFFF))  |
               SELF_TRIGGER          *daq.self_trigger                     |
               SELF_TRIGGER_LOOP     *daq.self_trigger_loop                |
               FIFO_MODE             *(daq.fifo_mode&1)                    |
               RESET                 *0;
    uhal::HwInterface hw=devices.front();
    hw.getNode("FEAD_CTRL").write(command);
    hw.getNode("CLK_SETTING").write(RESYNC_PHASE*daq.resync_phase);
    //printf("resync phase : %d\n",daq.resync_phase);
    hw.dispatch();
  }
}

