#define EXTERN extern
#include "gdaq_VFE.h"

void get_CRC_errors(int reset)
{
  uhal::HwInterface hw=devices.front();
  GtkWidget* widget;
  char widget_text[80], widget_name[80],reg_name[32];

// Reset CRC error counters if requested and only when the pedestals are set correctly.
// When FIFO is full, we have CRC errors since samples are missing :
  if(reset==1)
  {
    hw.getNode("FW_VER").write(CRC_RESET*1);
    hw.dispatch();
  }

// Read FEAD register for CRC errors
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    sprintf(reg_name,"CRC_%d",ich+1);
    ValWord<uint32_t> crc=hw.getNode(reg_name).read();
    hw.dispatch();
    asic.DTU_CRC_errors[ich]=crc.value();
    sprintf(widget_name,"%d_CRC_errors",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"%d",asic.DTU_CRC_errors[ich]);
    gtk_label_set_text((GtkLabel*)widget,widget_text);
  }
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving
}
