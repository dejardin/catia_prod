#define EXTERN extern
#include <gtk/gtk.h>
#include "gdaq_VFE.h"

void switch_power(bool status)
{
  UInt_t device_number, iret;
  GtkWidget* widget;

  uhal::HwInterface hw=devices.front();
  if(status)
  {
    printf("Switch ON VFE power supply\n");
    daq.I2C_low=0;
    daq.clock_select=0;
    update_trigger();

    for(Int_t iLVR=0; iLVR<2; iLVR++)
    {
      if(iLVR==0){device_number=0x67; if(daq.debug_DAQ)printf("V2P5 :\n");}
      else       {device_number=0x6C; if(daq.debug_DAQ)printf("V1P2 :\n");}
    // Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
      iret=I2C_RW(hw, device_number, 0x00, 0x05,0, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New content of control register : 0x%x\n",iret&0xffff);
// Max power limit
      iret=I2C_RW(hw, device_number, 0x0e, 0xff,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x0f, 0xff,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, daq.debug_I2C);
// Min power limit
      iret=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, daq.debug_I2C);
// Max ADin limit
      iret=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, daq.debug_I2C);
// Min ADin limit
      iret=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, daq.debug_I2C);
// Max Vin limit
      iret=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, daq.debug_I2C);
// Min Vin limit
      iret=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, daq.debug_I2C);
// Max current limit
      iret=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New max threshold on current 0x%x\n",iret&0xffff);
// Min current limit
      iret=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New min threshold on current 0x%x\n",iret&0xffff);
// Read and clear Fault register :
      // Alert register
      iret=I2C_RW(hw, device_number, 0x4, 0x00,0, 2, daq.debug_I2C);
      if(daq.debug_DAQ)printf("Alert register content : 0x%x\n",iret&0xffff);
      iret=I2C_RW(hw, device_number, 0x4, 0x00,0, 2, daq.debug_I2C);
      if(daq.debug_DAQ)printf("Alert register content : 0x%x\n",iret&0xffff);
// Set Alert register
      // Alert on Dsense overflow
      iret=I2C_RW(hw, device_number, 0x01, 0x20,0, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New alert bit pattern : 0x%x\n", iret&0xffff);
    }
    if(daq.debug_DAQ)printf("\n");
    usleep(100000);
// Release I2C and ReSync signals :
    daq.DTU_test_mode=1;
    Int_t ich=daq.channel_number[0];
    update_LiTEDTU_reg(ich,0); // Switch off line drivers
    daq.I2C_low=0;
    update_trigger();          // Put I2C signal at 0 and ReSync line in high-Z

    printf("Redo pwup_reset and Start LVRB auto scan mode\n");
    hw.getNode("VFE_CTRL").write(PWUP_RESETB*1);
    hw.getNode("VFE_CTRL").write(0);
    hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
    hw.dispatch();
    usleep(100000);

    get_consumption();

    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
      g_assert (widget);
      gtk_button_set_label((GtkButton*)widget,"Power ON");
    }
  }
  else
  {
    printf("Switch OFF VFE power supply\n");
// First ask to generate alert with overcurrent
    iret=I2C_RW(hw,0x67,0x01,0x20,0,1,daq.debug_I2C);
    iret=I2C_RW(hw,0x6C,0x01,0x20,0,1,daq.debug_I2C);
// Now, generate a current overflow :
    iret=I2C_RW(hw,0x67,0x03,0x20,0,1,daq.debug_I2C);
    iret=I2C_RW(hw,0x6C,0x03,0x20,0,1,daq.debug_I2C);
// Put I2C level at 0 and stop clocks fron FEAD board :
    //daq.DTU_test_mode=0;
    //Int_t ich=daq.channel_number[0];
    //update_LiTEDTU_reg(ich,0); // Switch off line drivers
    daq.I2C_low=1;
    update_trigger();          // Put I2C signal at 0 and ReSync line in high-Z
    hw.getNode("VFE_CTRL").write(0x80000000); // Put pwup_rstb and test_mode signals at 0
    hw.dispatch();
    usleep(500000);

    get_consumption();

    if(!daq.cli)
    {
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_power_ON");
      g_assert (widget);
      gtk_button_set_label((GtkButton*)widget,"Power OFF");
    }

    //daq.clock_select=0;
    //update_trigger();
  }
}

void get_consumption()
{
  GtkWidget* widget;
  char content[80];

  if(daq.daq_initialized<0) return;

  uhal::HwInterface hw=devices.front();
  Int_t iret=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  daq.I2P5_val=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V2P5;
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",iret,daq.I2P5_val);
  iret=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  daq.I1P2_val=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V1P2;
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",iret,daq.I1P2_val);
  iret=I2C_RW(hw, 0x67, 0x28, 0,1, 2, daq.debug_I2C);
  daq.V2P5_val=((iret>>4)&0xfff)/4096.*2.048*2.;
  printf("VFE V2P5 measured : %.3f\n",daq.V2P5_val);
  iret=I2C_RW(hw, 0x6C, 0x28, 0,1, 2, daq.debug_I2C);
  daq.V1P2_val=((iret>>4)&0xfff)/4096.*2.048;
  printf("VFE V1P2 measured : %.3f\n",daq.V1P2_val);

  if(!daq.cli)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_V2P5_value");
    g_assert (widget);
    sprintf(content,"%.3f V",daq.V2P5_val);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_V1P2_value");
    g_assert (widget);
    sprintf(content,"%.3f V",daq.V1P2_val);
    gtk_label_set_text((GtkLabel*)widget,content);
  }
}
